#ifndef CLRTBLWIDGET_H
#define CLRTBLWIDGET_H

#include <QWidget>
#include <QImage>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QDialog>
#include <QStackedWidget>
#include <QSplitter>
#include "fits.h"
#include "guitools.h"

class QFitsMainWindow;
class QFitsSingleBuffer;
class QFitsSimplestButton;

class QFitsToolsWidget : public QStackedWidget {
    Q_OBJECT
public:
    QFitsToolsWidget(QWidget *parent);
    void setSplitter(QSplitter *);
public slots:
    void setCurrentWidget(QWidget *w);
    void setFloating();
protected:
    void resizeEvent(QResizeEvent *event);
private:
    QFitsSimplestButton *hideButton, *floatButton;
    QWidget *myparent;
    QSplitter *mysplitter;
    bool floating;
    Qt::WindowFlags oldflags;
};


class QFits2dFit : public QWidget {
	Q_OBJECT
//----- Functions -----
public:
    QFits2dFit(QFitsMainWindow *parent = NULL);
    ~QFits2dFit() {}

    void centre(int, int, int, int);
    void fitGauss();
protected:
    void paintEvent( QPaintEvent * );
    void resizeEvent(QResizeEvent *);

//----- Slots -----
public slots:
    void refit();
    void zoomChanged(int);
    void fitfunctionChanged(const QString &);
    void createFitFunction();
    void copyFitResult();
    void newBufferFromFitResult();
    void passToGalfit();
//----- Signals -----
signals:
    void newposinfo(const QString &);

//----- Members -----
public:
//    QLabel *resultLabel1, *resultLabel2, *resultLabel3, *resultLabelPM,
    QLabel *resultLabelCuts;
    QSpinBox *fitwindow;
    QDoubleSpinBox *slm;
    QCheckBox *fitslm;
    QComboBox *fitfunction;
    dpFitResult *fitresults[10];
    QLabel *resultlabels[10];
    QPushButton /* *refitButton, *closeButton, */*createButton;
    int zoom;
    double fitresult[20], chisq;
    int cenx, ceny, cencx, cency, savex, savey, savefitwidth, fitx, fity;
    Fits result;
    QFitsMainWindow *myParent;
};

#endif /* CLRTBLWIDGET_H */
