#ifndef QFITSSCROLLER_H
#define QFITSSCROLLER_H

#include <QScrollArea>

#include "QFitsGlobal.h"

class QFitsMainView;
class QFitsWidget2D;
class Fits;

class QFitsScroller : public QScrollArea {
    Q_OBJECT
//----- Functions -----
public:
    QFitsScroller(QWidget *parent);
    ~QFitsScroller() {}

    void calculateScaling(Fits *fits);
    bool isSliderActive()       { return sliderActive; }

protected:
    virtual void viewportMouseMoveEvent( QMouseEvent *m );

//----- Slots -----
public slots:
    void processMouseClick(int, int);
    void sliderActivated();
    void sliderDeactivated();

//----- Signals -----
//----- Members -----
private:
    QWidget *myParent;
    bool sliderActive;
};

#endif /* QFITSSCROLLER_H */
