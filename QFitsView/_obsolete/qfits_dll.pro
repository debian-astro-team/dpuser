#### set ARCH ################
ARCH         = WIN32

#### set TEMPLATE ################
TEMPLATE     = lib

#### set QT ################
QT          += 	qt3support \
				opengl \
				network

#### set CONFIG ################
CONFIG      += qt \
               warn_off \
               dll

#### set VTK_PATH ################
VTK_PATH = vtk/MinGW/qt_shared_shared

#### set INCLUDEPATH ################
INCLUDEPATH +=	../utils \
                ../libfits \
                ../include \
                ../include/vtk \
                ../dpuser \
                ../dpuser/parser

#### set MAKEFILE ################
MAKEFILE     = QFitsView_dll.mk

#### set TARGET ################
CONFIG(release, debug|release) {
   TARGET    = QFitsView
}
CONFIG(debug, debug|release) {
   TARGET    = QFitsViewD
}

#### set DEFINES ################
DEFINES     -= UNICODE
DEFINES     += DPQT \
			   HAS_PGPLOT \
			   HAS_OPENGL \
			   NO_READLINE \
			   $$ARCH

CONFIG(debug, debug|release) {
   DEFINES  += DBG
}
#### set LIBS ################
CONFIG += qf_libs_common qf_libs_shared_shared
include(QFitsView_common.pro)
CONFIG -= qf_libs_common qf_libs_shared_shared

LIBS += -L../lib/win32/vtk/MinGW/qt_shared_shared

#CONFIG += qf_libs_common qf_libs_vtk
#include(QFitsView_common.pro)
#CONFIG -= qf_libs_common qf_libs_vtk

LIBS      += -lws2_32 -lcomdlg32
	 
contains(DEFINES, HAS_PGPLOT) {
	CONFIG += qf_libs_pgplot_win32
	include(QFitsView_common.pro)
	CONFIG -= qf_libs_pgplot_win32
	
	LIBS  += -lg2c -lgdi32
}

#LIBS += -L../lib/WIN32/vtk/MinGW/qt_shared \
#		-lQVTKWidgetPlugin

#### add sources and headers ################
CONFIG += qf_sources dp_sources
include(QFitsView_common.pro)
CONFIG -= qf_sources dp_sources
