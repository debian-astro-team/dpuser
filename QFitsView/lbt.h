#ifndef LBT_H
#define LBT_H

#include <QDialog>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QLineEdit>

class dpLuciMaskAlignDialog : public QDialog {
    Q_OBJECT
public:
    dpLuciMaskAlignDialog(QWidget*);
    QVBoxLayout     *mainLayout;
private:
    QCheckBox *autoFile1, *autoFile2;
    QLineEdit *sky1, *sky2, *obj1, *obj2, *mask1, *mask2;
    QLineEdit *dx1, *dy1, *dphi1, *dx2, *dy2, *dphi2;
    QPushButton *align1, *align2;
    QPushButton *apply1, *apply2, *applyboth;
    bool firsttime;

public slots:
    void align1Clicked(void);
    void align2Clicked(void);
    void apply1Clicked(void);
    void apply2Clicked(void);
    void applybothClicked(void);
    void updateValue(int, double);
    void updateFilename(int, QString);
    void show();
};

#endif /* LBT_H */
