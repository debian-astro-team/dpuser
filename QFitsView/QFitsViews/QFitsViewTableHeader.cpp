#include "QFitsViewTableHeader.h"
#include "QFitsGlobal.h"

QFitsViewTableHeader::QFitsViewTableHeader(Qt::Orientation orientation, QWidget* parent)
    : QHeaderView(orientation, parent)
{qFitsDebug("QFitsViewTableHeader::QFitsViewTableHeader [consider inheriting as well from QFitsBaseView ?]");

}

QRect QFitsViewTableHeader::sectionRect(int logicalIndex) const
{
    return sections.value(logicalIndex);
}

void QFitsViewTableHeader::mouseDoubleClickEvent(QMouseEvent* event)
{
    QHeaderView::mouseDoubleClickEvent(event);

    const int idx = visualIndexAt(pick(event->pos()));
    const QModelIndex persistent = model()->index(idx, 0);
    if (event->button() & Qt::LeftButton)
        edit(persistent, QAbstractItemView::DoubleClicked, event);
}

void QFitsViewTableHeader::paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const
{
    sections.insert(logicalIndex, rect);
    QHeaderView::paintSection(painter, rect, logicalIndex);
}

int QFitsViewTableHeader::pick(const QPoint& pos) const
{
    return orientation() == Qt::Horizontal ? pos.x() : pos.y();
}

void QFitsViewTableHeader::headerDataChanged(Qt::Orientation orientation, int logicalFirst, int logicalLast) {
    emit renamePlotLabels();
}

QFitsHeaderDelegate::QFitsHeaderDelegate(Qt::Orientation orientation, QObject* parent)
    : QItemDelegate(parent), orientation(orientation)
{

}

QWidget* QFitsHeaderDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(option);
    if (!index.isValid())
        return 0;

    QWidget* editor = 0;
    QFitsViewTableHeader* header = qobject_cast<QFitsViewTableHeader*>(QObject::parent());
    if (header)
    {
        const int logicalIndex = header->logicalIndex(index.row());
        const QVariant data = index.model()->headerData(logicalIndex, orientation, Qt::EditRole);
        const QVariant::Type type = static_cast<QVariant::Type>(data.userType());
        editor = editorFactory()->createEditor(type, parent);
    }
    return editor;
}

void QFitsHeaderDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    QFitsViewTableHeader* header = qobject_cast<QFitsViewTableHeader*>(parent());
    if (header)
    {
        const int logicalIndex = header->logicalIndex(index.row());
        const QVariant value = index.model()->headerData(logicalIndex, orientation, Qt::EditRole);
        QByteArray name = editor->metaObject()->userProperty().name();
        if (name.isEmpty())
            name = editorFactory()->valuePropertyName(static_cast<QVariant::Type>(value.userType()));
        if (!name.isEmpty())
            editor->setProperty(name, value);
    }
}

void QFitsHeaderDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    QFitsViewTableHeader* header = qobject_cast<QFitsViewTableHeader*>(parent());
    if (header)
    {
        const int logicalIndex = header->logicalIndex(index.row());
        const QVariant value = index.model()->headerData(logicalIndex, orientation, Qt::EditRole);
        QByteArray name = editor->metaObject()->userProperty().name();
        if (name.isEmpty())
            name = editorFactory()->valuePropertyName(static_cast<QVariant::Type>(value.userType()));
        if (!name.isEmpty())
            model->setHeaderData(logicalIndex, orientation, editor->property(name), Qt::EditRole);
    }
}

void QFitsHeaderDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(option);
    QFitsViewTableHeader* header = qobject_cast<QFitsViewTableHeader*>(parent());
    if (header)
    {
        const int logicalIndex = header->logicalIndex(index.row());
        const QRect rect = header->sectionRect(logicalIndex);
        editor->setGeometry(rect);
    }
}

const QItemEditorFactory* QFitsHeaderDelegate::editorFactory() const
{
    const QItemEditorFactory* factory = itemEditorFactory();
    if (factory == 0)
        factory = QItemEditorFactory::defaultFactory();
    return factory;
}
