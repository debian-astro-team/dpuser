#ifndef QFITSBASEVIEW_H
#define QFITSBASEVIEW_H

#include <QWidget>

class QFitsBaseView : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsBaseView(QWidget *parent);
    virtual ~QFitsBaseView() {}

    virtual void switchBackgroundColor();

protected:
    virtual void setMousePressedPos(int, int);
    virtual void getMousePressedPos(int*, int*);

//----- Slots -----
//----- Signals -----
//----- Members -----
public:
// myParent is not declared because QFitsWidget1D and QFitsCubeSpectrumViewer
// are derived from QFitsView1D (different parents)!
//    QFitsWidget1D *myParent;
private:
    int     mousePressedPosX,
            mousePressedPosY;
};

#endif // QFITSBASEVIEW_H
