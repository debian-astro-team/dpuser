#ifndef GALFIT_H
#define GALFIT_H

#include <QDialog>
#include <QTextEdit>
#include "fits.h"
#include "guitools.h"
#include "ui_galfit_control.h"
#include "ui_galfit_sky.h"
#include "ui_galfit_gaussian.h"
#include "ui_galfit_sersic.h"
#include "ui_galfit_hidden.h"

class dpGalfitComponent : public QWidget {
    Q_OBJECT
public:
    dpGalfitComponent(QWidget *parent = NULL) : QWidget(parent) {};
    virtual QStringList getParameters(bool ro = false) { return QStringList(); };
    virtual QStringList getConstraints(int component) { return QStringList(); };
    virtual void setResult(const int component, const Fits &result) {};
};

class dpGalfitControl : public Ui::Galfit_Control, dpGalfitComponent {
public:
    dpGalfitControl() {};
    virtual QStringList getParameters(bool ro = false);
};

class dpGalfitSky : public Ui::galfit_sky, public dpGalfitComponent {
public:
    dpGalfitSky(QWidget *parent = NULL);
    void setValues(double b, double x = 0.0, double y = 0.0);
    virtual QStringList getParameters(bool ro = false);
    virtual QStringList getConstraints(int component);
    virtual void setResult(const int component, const Fits &result);
};

class dpGalfitHidden : public Ui::galfit_hidden, public dpGalfitComponent {
public:
    dpGalfitHidden(QWidget *parent = NULL) : dpGalfitComponent(parent) { setupUi(this); widget->adjustSize(); };
    void setValues(double c0v, double b1v, double b2v, double b3v);
    virtual QStringList getParameters(bool ro = false);
    virtual QStringList getConstraints(int component);
    virtual void setResult(const int component, const Fits &result);
};

class dpGalfitGaussian : public Ui::galfit_gaussian, public dpGalfitComponent {
public:
    dpGalfitGaussian(QWidget *parent = NULL);
    void setValues(double x, double y, double m, double f, double r, double a);
    virtual QStringList getParameters(bool ro = false);
    virtual QStringList getConstraints(int component);
    virtual void setResult(const int component, const Fits &result);
    dpGalfitHidden *hidden;
};

class dpGalfitSersic : public Ui::galfit_sersic, public dpGalfitComponent {
public:
    dpGalfitSersic(QWidget *parent = NULL);
    void setValues(double x, double y, double m, double e, double i, double r, double a);
    virtual QStringList getParameters(bool ro = false);
    virtual QStringList getConstraints(int component);
    virtual void setResult(const int component, const Fits &result);
    dpGalfitHidden *hidden;
};

class dpGalfitDialog : public QDialog {
    Q_OBJECT
public:
    dpGalfitDialog(QWidget *parent);
    void newBufferFromComponent(int);
//private:
    dpGalfitControl *galfitControl;
    QTabWidget *objects;
    QTextEdit *output;
public slots:
//    void accepted();
    void runGalfit();
    void updateandshow();
    void clearObjects();
    void addSky(double background = 0.0, double dx = 0.0, double dy = 0.0);
    void addGaussian(double x = 1, double y = 1, double m = 1, double f = 1, double r = 1, double a = 1);
    void addSersic(double x = 1, double y = 1, double m = 1, double e = 1, double i = 1, double r = 1, double a = 1, bool sersic_fixed = false);
    void closeObject(int);
    void newInputDataImage(const QString &);
    void newBufferClicked();
    void newBufferComponentClicked();
};

#endif /* GALFIT_H */

