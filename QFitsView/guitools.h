#ifndef GUITOOLS_H
#define GUITOOLS_H

#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QDoubleValidator>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QMenu>
#include "QFitsGlobal.h"

class QFitsSimplestButton : public QLabel {
    Q_OBJECT
public:
    QFitsSimplestButton(QPixmap pm, QWidget *parent);
    QFitsSimplestButton(const QString &te, QWidget *parent);
    ~QFitsSimplestButton() {}

protected:
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseDoubleClickEvent(QMouseEvent *);

signals:
    void clicked();
    void doubleClicked();
};

class dpDoubleEdit : public QLineEdit {
    Q_OBJECT
public:
    dpDoubleEdit(QWidget *parent) : dpDoubleEdit("0.0", parent) { };
    dpDoubleEdit(QString, QWidget *parent);
    void setValue(const double &);
    double value();
public slots:
    void notifyChange(const QString &);

signals:
    void valueChanged(double);
};

class dpFitEstimate : public dpDoubleEdit {
    Q_OBJECT
public:
    dpFitEstimate(QWidget *parent) : dpFitEstimate("0.0", parent) { };
    dpFitEstimate(QString, QWidget *parent);
    bool isFixed() { return fixed->isChecked(); }
    bool isConstrained() { return constrained->isChecked(); }
protected:
    void resizeEvent(QResizeEvent*);
    virtual void contextMenuEvent(QContextMenuEvent *event);
//    virtual void mousePressEvent(QMouseEvent *event);
private:
    QLabel *minConstrain, *maxConstrain, *parameterLock;
    QWidget *parameters;
    QFitsSimplestButton *popupButton;
public:
    QCheckBox *constrained, *fixed;
    dpDoubleEdit *upperBound, *lowerBound;
//    QLabel *result, *error;
public slots:
    void popupParameters();
};

class dpFitResult : public QWidget {
    Q_OBJECT
public:
    dpFitResult(QWidget *parent);
    void setResult(double, double);
    void adjustSize();
    QString text() const;
    QString getValue() const;
    QString getError() const;
protected:
    void resizeEvent(QResizeEvent*);
private:
    QLabel *v, *e, *pm;
    double value, error;
};

#endif /* GUITOOLS_H */
