#ifndef QFITS_PREFERENCES_H
#define QFITS_PREFERENCES_H

#include <QDialog>

class QTabWidget;
class QWidget;
class QSpinBox;
class QLabel;
class QComboBox;
class QLineEdit;
class QPushButton;
class QFontComboBox;
class QDialogButtonBox;

class QFitsPrefDialog : public QDialog {
    Q_OBJECT

public:
    QFitsPrefDialog(QWidget*);
    ~QFitsPrefDialog();

    QString GetInitialZoom();

protected slots:
    void accept();
    void updateFontSample(int);
    void dpuserlibPathButtonClicked();
    void pythonLibraryButtonClicked();
    void galfitButtonClicked();

private:
    QDialogButtonBox    *buttonBox;
    QPushButton         *buttonCancel,
                        *buttonOk;
    QTabWidget          *tabWidget;
    QComboBox           *imageScalingLimits,
                        *initialZoom,
                        *viewingTools,
                        *tools,
                        *dpuserConsole;
    QLineEdit           *dpuserlibPath,
                        *pythonLibraryPath,
                        *pythonPath,
                        *gdlPath,
                        *galfitPath;
    QSpinBox            *wiregridWidth,
                        *wiregridHeight,
                        *textFontSize;
    QFontComboBox       *textFont;
    QLabel              *textFontSample;
    QPushButton         *dpuserlibPathButton,
                        *pythonLibraryButton,
                        *galfitButton;
};

#endif // QFITS_PREFERENCES_H
