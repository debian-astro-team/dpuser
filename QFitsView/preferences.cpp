//#include <qvariant.h>
//#include <qpushbutton.h>
//#include <qtabwidget.h>
//#include <qwidget.h>
//#include <qspinbox.h>
//#include <qlabel.h>
//#include <qcombobox.h>
//#include <qlayout.h>
//#include <qtooltip.h>
//#include <qwhatsthis.h>
//#include <qimage.h>

#include "QFitsGlobal.h"
#include "QFitsPreferences.h"

static const char* const image0_data[] = { 
"16 16 5 1",
"b c #000000",
"# c #0000ff",
". c #009999",
"a c #00ffff",
"c c #cccccc",
"...........##...",
".........##aa#..",
"........#aaaa#..",
".......#aaaaaa#.",
".....##aaaaa##..",
"....#aaaaa##....",
"...#aaaa##......",
".##aaa##b.......",
"#aaa##..b.......",
"#a##...bbb......",
".#.....bbb......",
"......b.b.b.....",
"......b.b.b.....",
".....b..b..b....",
".....b.ccc.b....",
"....ccc...ccc..."};


/*
 *  Constructs a QFitsPreferences as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
QFitsPreferences::QFitsPreferences( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl ),
      image0( (const char **) image0_data )
{
    if ( !name )
    setName( "Preferences" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setIcon( image0 );
    setSizeGripEnabled( FALSE );
    preferencesLayout = new QHBoxLayout( this, 11, 6, "preferencesLayout"); 

    layout13 = new QVBoxLayout( 0, 0, 6, "layout13"); 

    tabWidget2 = new QTabWidget( this, "tabWidget2" );
    tabWidget2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)1, 0, 0, tabWidget2->sizePolicy().hasHeightForWidth() ) );

    tab = new QWidget( tabWidget2, "tab" );
    tabLayout = new QVBoxLayout( tab, 11, 6, "tabLayout"); 

    layout12 = new QGridLayout( 0, 1, 1, 0, 6, "layout12"); 

    layout9 = new QHBoxLayout( 0, 0, 6, "layout9"); 

    cube3dx = new QSpinBox( tab, "cube3dx" );
    cube3dx->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)0, 0, 0, cube3dx->sizePolicy().hasHeightForWidth() ) );
    cube3dx->setMinValue( 2 );
    layout9->addWidget( cube3dx );

    textLabel2_3 = new QLabel( tab, "textLabel2_3" );
    layout9->addWidget( textLabel2_3 );

    cube3dy = new QSpinBox( tab, "cube3dy" );
    cube3dy->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)0, 0, 0, cube3dy->sizePolicy().hasHeightForWidth() ) );
    cube3dy->setMinValue( 2 );
    layout9->addWidget( cube3dy );

    textLabel3 = new QLabel( tab, "textLabel3" );
    layout9->addWidget( textLabel3 );

    cube3dz = new QSpinBox( tab, "cube3dz" );
    cube3dz->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)0, 0, 0, cube3dz->sizePolicy().hasHeightForWidth() ) );
    cube3dz->setMinValue( 2 );
    layout9->addWidget( cube3dz );

    layout12->addLayout( layout9, 3, 1 );

    defaultZoom = new QComboBox( FALSE, tab, "defaultZoom" );
    defaultZoom->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)1, 0, 0, defaultZoom->sizePolicy().hasHeightForWidth() ) );

    layout12->addWidget( defaultZoom, 1, 1 );

    textLabel1 = new QLabel( tab, "textLabel1" );
    textLabel1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel1->sizePolicy().hasHeightForWidth() ) );

    layout12->addWidget( textLabel1, 0, 0 );

    textLabel1_5 = new QLabel( tab, "textLabel1_5" );

    layout12->addWidget( textLabel1_5, 3, 0 );

    layout11 = new QHBoxLayout( 0, 0, 6, "layout11"); 

    wiregridwidth = new QSpinBox( tab, "wiregridwidth" );
    wiregridwidth->setMaxValue( 999 );
    wiregridwidth->setMinValue( 2 );
    layout11->addWidget( wiregridwidth );

    textLabel2_4 = new QLabel( tab, "textLabel2_4" );
    layout11->addWidget( textLabel2_4 );

    wiregridheight = new QSpinBox( tab, "wiregridheight" );
    wiregridheight->setMaxValue( 999 );
    wiregridheight->setMinValue( 2 );
    layout11->addWidget( wiregridheight );
    spacer5_2 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout11->addItem( spacer5_2 );

    layout12->addLayout( layout11, 2, 1 );

    textLabel1_6 = new QLabel( tab, "textLabel1_6" );

    layout12->addWidget( textLabel1_6, 2, 0 );

    textLabel1_4 = new QLabel( tab, "textLabel1_4" );
    textLabel1_4->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel1_4->sizePolicy().hasHeightForWidth() ) );

    layout12->addWidget( textLabel1_4, 1, 0 );

    defaultLimits = new QComboBox( FALSE, tab, "defaultLimits" );
    defaultLimits->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)1, 0, 0, defaultLimits->sizePolicy().hasHeightForWidth() ) );

    layout12->addWidget( defaultLimits, 0, 1 );
    tabLayout->addLayout( layout12 );
    spacer4 = new QSpacerItem( 31, 190, QSizePolicy::Minimum, QSizePolicy::Expanding );
    tabLayout->addItem( spacer4 );
    tabWidget2->insertTab( tab, QString::fromLatin1("") );

    tab_2 = new QWidget( tabWidget2, "tab_2" );
    tabLayout_2 = new QVBoxLayout( tab_2, 11, 6, "tabLayout_2"); 

    layout2 = new QGridLayout( 0, 1, 1, 0, 6, "layout2"); 

    textLabel2 = new QLabel( tab_2, "textLabel2" );
    textLabel2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel2->sizePolicy().hasHeightForWidth() ) );

    layout2->addWidget( textLabel2, 0, 0 );
    spacer3 = new QSpacerItem( 21, 231, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout2->addItem( spacer3, 2, 0 );

    viewingTools = new QComboBox( FALSE, tab_2, "viewingTools" );
    viewingTools->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)1, 0, 0, viewingTools->sizePolicy().hasHeightForWidth() ) );

    layout2->addWidget( viewingTools, 0, 1 );

    textLabel2_2 = new QLabel( tab_2, "textLabel2_2" );
    textLabel2_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel2_2->sizePolicy().hasHeightForWidth() ) );

    layout2->addWidget( textLabel2_2, 1, 0 );

    Dpuser = new QComboBox( FALSE, tab_2, "Dpuser" );
    Dpuser->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)1, 0, 0, Dpuser->sizePolicy().hasHeightForWidth() ) );

    layout2->addWidget( Dpuser, 1, 1 );
    tabLayout_2->addLayout( layout2 );
    tabWidget2->insertTab( tab_2, QString::fromLatin1("") );

    TabPage = new QWidget( tabWidget2, "TabPage" );
    TabPageLayout = new QHBoxLayout( TabPage, 11, 6, "TabPageLayout"); 

    layout9_2 = new QVBoxLayout( 0, 0, 6, "layout9_2"); 

    textLabel1_2 = new QLabel( TabPage, "textLabel1_2" );
    textLabel1_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)1, 0, 0, textLabel1_2->sizePolicy().hasHeightForWidth() ) );
    layout9_2->addWidget( textLabel1_2 );

    layout8 = new QHBoxLayout( 0, 0, 6, "layout8"); 

    docuPath = new QLineEdit( TabPage, "docuPath" );
    docuPath->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)1, 0, 0, docuPath->sizePolicy().hasHeightForWidth() ) );
    docuPath->setMinimumSize( QSize( 300, 0 ) );
    QFont docuPath_font(  docuPath->font() );
    docuPath_font.setPointSize( 10 );
    docuPath->setFont( docuPath_font ); 
    layout8->addWidget( docuPath );

    docuButton = new QPushButton( TabPage, "docuButton" );
    docuButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, docuButton->sizePolicy().hasHeightForWidth() ) );
    QFont docuButton_font(  docuButton->font() );
    docuButton_font.setPointSize( 10 );
    docuButton->setFont( docuButton_font ); 
    layout8->addWidget( docuButton );
    layout9_2->addLayout( layout8 );
    spacer5 = new QSpacerItem( 21, 240, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout9_2->addItem( spacer5 );
    TabPageLayout->addLayout( layout9_2 );
    tabWidget2->insertTab( TabPage, QString::fromLatin1("") );
    layout13->addWidget( tabWidget2 );

    Layout1 = new QHBoxLayout( 0, 0, 6, "Layout1"); 

    buttonHelp = new QPushButton( this, "buttonHelp" );
    buttonHelp->setAutoDefault( TRUE );
    Layout1->addWidget( buttonHelp );
    Horizontal_Spacing2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( Horizontal_Spacing2 );

    buttonOk = new QPushButton( this, "buttonOk" );
    buttonOk->setAutoDefault( TRUE );
    buttonOk->setDefault( TRUE );
    Layout1->addWidget( buttonOk );

    buttonCancel = new QPushButton( this, "buttonCancel" );
    buttonCancel->setAutoDefault( TRUE );
    Layout1->addWidget( buttonCancel );
    layout13->addLayout( Layout1 );

    textLabel1_3 = new QLabel( this, "textLabel1_3" );
    textLabel1_3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, textLabel1_3->sizePolicy().hasHeightForWidth() ) );
    textLabel1_3->setAlignment( int( QLabel::AlignCenter ) );
    layout13->addWidget( textLabel1_3 );
    preferencesLayout->addLayout( layout13 );
    languageChange();
    resize( QSize(512, 463).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( buttonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );
    connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect(docuSearchButton, SIGNAL(clicked()),
            this, SLOT(docuSearchButtonPressed()));

    // tab order
    setTabOrder( buttonHelp, buttonOk );
    setTabOrder( buttonOk, buttonCancel );
    setTabOrder( buttonCancel, defaultLimits );
    setTabOrder( defaultLimits, defaultZoom );
    setTabOrder( defaultZoom, cube3dx );
    setTabOrder( cube3dx, cube3dy );
    setTabOrder( cube3dy, cube3dz );
    setTabOrder( cube3dz, tabWidget2 );
    setTabOrder( tabWidget2, viewingTools );
    setTabOrder( viewingTools, Dpuser );
    setTabOrder( Dpuser, docuPath );
    setTabOrder( docuPath, docuButton );
}

/*
 *  Destroys the object and frees any allocated resources
 */
QFitsPreferences::~QFitsPreferences()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void QFitsPreferences::languageChange()
{
    setCaption( tr( "QFitsView Preferences" ) );
    textLabel2_3->setText( tr( "X" ) );
    textLabel3->setText( tr( "X" ) );
    defaultZoom->clear();
    defaultZoom->insertItem( tr( "25%" ) );
    defaultZoom->insertItem( tr( "50%" ) );
    defaultZoom->insertItem( tr( "100%" ) );
    defaultZoom->insertItem( tr( "200%" ) );
    defaultZoom->insertItem( tr( "400%" ) );
    defaultZoom->insertItem( tr( "Fit window" ) );
    textLabel1->setText( tr( "Image scaling limits" ) );
    textLabel1_5->setText( tr( "Maximum 3D cube size" ) );
    textLabel2_4->setText( tr( "X" ) );
    textLabel1_6->setText( tr( "Wiregrid size" ) );
    textLabel1_4->setText( tr( "Initial zoom" ) );
    defaultLimits->clear();
    defaultLimits->insertItem( tr( "minmax" ) );
    defaultLimits->insertItem( tr( "99.9%" ) );
    defaultLimits->insertItem( tr( "99.5%" ) );
    defaultLimits->insertItem( tr( "99%" ) );
    defaultLimits->insertItem( tr( "98%" ) );
    defaultLimits->insertItem( tr( "95%" ) );
    tabWidget2->changeTab( tab, tr( "Image Display" ) );
    textLabel2->setText( tr( "Viewing Tools" ) );
    viewingTools->clear();
    viewingTools->insertItem( tr( "Hide" ) );
    viewingTools->insertItem( tr( "In Dock" ) );
    viewingTools->insertItem( tr( "Floating" ) );
    textLabel2_2->setText( tr( "Dpuser Console" ) );
    Dpuser->clear();
    Dpuser->insertItem( tr( "Hide" ) );
    Dpuser->insertItem( tr( "In Dock" ) );
    Dpuser->insertItem( tr( "Floating" ) );
    tabWidget2->changeTab( tab_2, tr( "Appearance" ) );
    textLabel1_2->setText( tr( "Documentation location:" ) );
    docuButton->setText( tr( "..." ) );
    tabWidget2->changeTab( TabPage, tr( "Paths" ) );
    buttonHelp->setText( tr( "&Help" ) );
    buttonOk->setCaption( QString::null );
    buttonOk->setText( tr( "&OK" ) );
    buttonCancel->setText( tr( "&Cancel" ) );
    textLabel1_3->setText( tr( "Changes take effect next time QFitsView is started" ) );
}

void QFitsPreferences::docuSearchButtonPressed() {
    docuPath->setText(searchForDocumentation());
}

