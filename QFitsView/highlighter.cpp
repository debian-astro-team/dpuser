/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include <QtGui>

#include "highlighter.h"

Highlighter::Highlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

//    keywordFormat.setForeground(Qt::darkBlue);
    keywordFormat.setFontWeight(QFont::Bold);
    QStringList keywordPatterns;
    keywordPatterns << "\\bfor\\b" << "\\bif\\b" << "\\bwhile\\b"
                    << "\\bdo\\b" << "\\begin\\b" << "\\bfunction\\b"
					<< "\\bprocedure\\b";
    foreach (QString pattern, keywordPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    classFormat.setFontWeight(QFont::Bold);
    classFormat.setForeground(Qt::darkMagenta);
    rule.pattern = QRegularExpression("\\bQ[A-Za-z]+\\b");
    rule.format = classFormat;
    highlightingRules.append(rule);

    singleLineCommentFormat.setForeground(Qt::gray);
    rule.pattern = QRegularExpression("//[^\n]*");
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    multiLineCommentFormat.setForeground(Qt::gray);

    quotationFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression("\".*\"");
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    functionFormat.setFontItalic(true);
    functionFormat.setForeground(Qt::blue);
    rule.pattern = QRegularExpression("\\b[A-Za-z0-9_]+(?=\\()");
    rule.format = functionFormat;
    highlightingRules.append(rule);

    commentStartExpression = QRegularExpression("/\\*");
    commentEndExpression = QRegularExpression("\\*/");
}

void Highlighter::highlightBlock(const QString &text)
{
    QRegularExpressionMatch rmatch;
    foreach (HighlightingRule rule, highlightingRules) {
        QRegularExpression expression(rule.pattern);
        int index = text.indexOf(expression, 0, &rmatch);
        while (index >= 0) {
            int length = rmatch.capturedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length);
        }
    }
    setCurrentBlockState(0);

    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = text.indexOf(commentStartExpression, 0, &rmatch);

    while (startIndex >= 0) {
        int endIndex = text.indexOf(commentEndExpression, startIndex, &rmatch);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex + rmatch.capturedLength();
//                            + commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = text.indexOf(commentStartExpression,
                                                startIndex + commentLength);
    }
}

PreviewHighlighter::PreviewHighlighter(QTextDocument *parent, const QString &comment, const QString &delimiter)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    if (comment.length() > 0) {
        singleLineCommentFormat.setForeground(Qt::gray);
        rule.pattern = QRegularExpression("^[" + comment + "][^\n]*");
        rule.format = singleLineCommentFormat;
        highlightingRules.append(rule);
    }

    if (delimiter.length() > 0) {
        delimiterFormat.setBackground(QBrush(Qt::black, Qt::VerPattern));
        QRegularExpression delimiterSyntax(delimiter);
//        delimiterSyntax.setPatternSyntax(QRegularExpression::FixedString);
        rule.pattern = QRegularExpression(delimiterSyntax);
        if (delimiter == " ") rule.pattern = QRegularExpression("[ \t]");
        rule.format = delimiterFormat;
        highlightingRules.append(rule);
    }
}

void PreviewHighlighter::highlightBlock(const QString &text) {
    foreach (HighlightingRule rule, highlightingRules) {
        QRegularExpressionMatch rmatch;
        QRegularExpression expression(rule.pattern);
        int index = text.indexOf(expression, 0, &rmatch);
        while (index >= 0) {
            int length = rmatch.capturedLength();
            setFormat(index, length, rule.format);
            index = text.indexOf(expression, index + length);
        }
    }
}
