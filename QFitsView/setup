#!/bin/bash

# get absolute directory path to the build location:

# --build-dir, -b

build_destination="`pwd`"

# get absolute directory path to this script:
source_destination="`dirname $0`"
source_destination="`( cd "$source_destination"; pwd )`"


### DEFAULTS

# --install-dir, -i

# get absolute directory path to the install location:
install_destination="`( cd ..; pwd)`/install-QFitsView"

# --shared, --linked, -l | --static, -s

shared_build=true

# --architecture, -a

valid_architectures=("DETECT" "WINDOWS" "LINUX" "OSX")
build_architecture="DETECT"

# --option <OPTION>, -o
valid_options=("LBT=[ON|OFF]" "HAS_VTK=[ON|OFF]" "HAS_GDL=[ON|OFF]" "HAS_PYTHON=[ON|OFF]" "HAS_PLPLOT=[ON|OFF]" "HAS_PGPLOT=[ON|OFF]" "HAS_DPPGPLOT=[ON|OFF]" "HAS_PG2PLPLOT=[ON|OFF]")
custom_options=""

# --qt-path <PATH>, -q
qt_path=""

# --help, -h
function helpConfigure {
    echo "Usage: configure [OPTIONS]..."
    echo "Configures cmake to build the current project."
    echo
    echo "Mandatory arguments to long options are mandatory for short options too."
    printf "  %-32s  %s\n" "-b, --build-dir <BUILD_DIR>" "Set build directory for cmake."
    printf "  %-32s  %s\n" "-i, --install-dir <INSTALL_DIR>" "Set install directory for cmake."
    printf "  %-32s  %s\n" "-l, --linked, --shared" "Use dynamic libraries."
    printf "  %-32s  %s\n" "-s, --static" "Use as much static libraries as possible."
    printf "  %-32s  %s\n" "-a, --architecture <ARCH>" "Select architecture to compile for [UNUSED]."
    printf "  %32s  " ""
    printf "%s, " "${valid_architectures[@]}"
    printf "\n"
    printf "  %-32s  %s\n" "-o, --option <OPTION=[ON|OFF]>" "Enable a compiler option."
    printf "  %32s  " ""
    printf "%s, " "${valid_options[@]}"
    printf "\n"
    printf "  %-32s  %s\n" "-h, --help" "Show this help manual."
    printf "  %-32s  %s\n" "-q, --qt-path <QT_PATH>" "Select the Qt install directory."
}

### PARSING COMMAND LINE ARGUMENTS

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
      -b|--build-dir)
        build_destination="$2"

        shift
        shift
        ;;
      -i|--install-dir)
        install_destination="$build_destination/$2"

        shift
        shift
        ;;
      -l|--linked|--shared)
        shared_build=true

        shift
        ;;
      -s|--static)
        shared_build=false

        shift
        ;;
      -a|--architecture)
        architecture="$2"

        if [[ ! ${valid_architectures[$architecture]} ]]; then
            echo "Invalid architecture: $architecture".
            echo "Available options are: "
            printf '%s, ' "${valid_architectures[@]}"
            echo
            exit 1
        fi

        build_architecture="$architecture"

        shift
        shift
        ;;
      -o|--option)
        custom_option="$2"

        if [[ ! ${valid_options[$option]} ]]; then
            echo "Invalid option: $custom_option".
            echo "Available options are: "
            printf '%s, ' "${valid_option[@]}"
            echo
            exit 1
        fi

        custom_options+="$custom_option "

        shift
        shift
        ;;
      -q|--qt-path)
        qt_path="$2"

        shift
        shift
        ;;
      -h|--help)
        helpConfigure
        exit 0
        ;;
      \?)
        echo "ERROR: Invalid parameter: $key"
        exit 1
        ;;
    esac
done

if [ "$build_architecture" == "DETECT" ]; then
    echo "Autodetecting operating system..."

    os=$(uname)

    case "$os" in
        "Darwin")
        build_architecture="OSX"

        ;;
        "Linux")
        build_architecture="Linux"

        ;;
        CYGWIN*|MINGW*)
        build_architecture="Windows"

        ;;
        *)
        echo "ERROR: unknown OS type $os. Exiting..."
        exit 1

        ;;
    esac

    echo
fi


### OUTPUT

echo "Source location: $source_destination"
echo "Build location: $build_destination"
echo "Install location: $install_destination"

if [ "$shared_build" = true ]; then
    echo "Build type: SHARED"
else
    echo "Build type: STATIC"
fi

echo "Architecture: $build_architecture"
echo "Custom options: $custom_options"


### CONFIG FILE OUTPUT

config_file="$source_destination/config"

echo "source_location=$source_destination" > config_file
echo "build_destination=$build_destination" >> config_file
echo "install_destination=$install_destination" >> config_file

if [ "$shared_build" = true ]; then
    echo "build_type=SHARED" >> config_file
else
    echo "build_type=STATIC" >> config_file
fi

echo "build_architecture=$build_architecture" >> config_file
echo "custom_options=$custom_options" >> config_file

### CMAKE CONFIGURATION

echo
echo

if [ ! -z "$custom_options" ]; then
    options=$(printf '%s %s ' '-D' "${custom_options[@]}")
fi

options+=" -D BUILD_SHARED_LIBS="
if [ "$shared_build" = true ]; then
    options+="ON"
else
    options+="OFF"
fi

if [ "$qt_path" ]; then
    options+=" -D QT_PATH='$qt_path'"
fi

if ! cmake -DCMAKE_INSTALL_PREFIX="$install_destination" -S "$source_destination" -B "$build_destination" $options; then
    echo
    echo
    echo "Failed to configure QFitsView."
    echo
    exit 1
fi
echo
echo

### FINAL INFORMATION

echo "QFitsView was successfully configured."
echo
echo "Run 'cmake --build . --parallel' to build QFitsView."
echo "After that, run 'cmake --install .' to install QFitsView to the installation directory."
echo
