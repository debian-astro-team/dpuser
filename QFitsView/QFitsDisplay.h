#ifndef QFITSDISPLAY_H
#define QFITSDISPLAY_H

#include <qwidget.h>
#include <qstring.h>
#include "QFitsImage.h"

class QFitsDisplay : public QWidget {
	Q_OBJECT
public:
	QFitsDisplay(QWidget *parent = NULL);
	QFitsImage image;
	Fits *data;
	bool LoadFile(QString &fname);
protected:
	void paintEvent( QPaintEvent * );
	void resizeEvent(QResizeEvent *);
};

#endif /* QFITSDISPLAY_H */
