#ifndef QFITSWIDGET3D_H
#define QFITSWIDGET3D_H

#include <QResizeEvent>

#include "QFitsBaseWidget.h"

class QFitsView3D;

//------------------------------------------------------------------------------
//         QFitsWidget3D
//------------------------------------------------------------------------------
// Adapter-pattern
class QFitsWidget3D : public QFitsBaseWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsWidget3D(QFitsBaseBuffer*);
    ~QFitsWidget3D();

    //
    // overloaded abstract base functions
    //
//    void setZoom(double);
//    void updateScaling();
    void newData3D();
    void setupColours();
//    void setImageCenter(double, double);
    QFitsBaseView* getView() { return NULL; }

protected:
    void resizeEvent(QResizeEvent *r);

//----- Slots -----
//----- Signals -----
//----- Members -----
public:  // public for testing purpose
//private:
    QFitsView3D *cubeViewer;
};

#endif /* QFITSWIDGET3D_H */
