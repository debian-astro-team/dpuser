#ifndef CUBE_3D_VTK_H
#define CUBE_3D_VTK_H

#include <QImage>
#include <QResizeEvent>
#include <QGLWidget>

#include "QFitsBaseWidget.h"
#include "QFitsGlobal.h"
#include "fits.h"

class Fits;
class QFitsMainView;
class QFitsView3D;

//------------------------------------------------------------------------------
//         QFitsWidget3D
//------------------------------------------------------------------------------
// Adapter-pattern
class QFitsWidget3D : public QFitsBaseWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsWidget3D(QFitsSingleBuffer *buffer);
    ~QFitsWidget3D();

    void scrollerMoved() {}
    void resizeEvent(QResizeEvent *r);

    void orientationChanged()                       {}
    void setFlipX(bool)                             {}
    void setFlipY(bool)                             {}
    void setRotation(int)                           {}
    void incRotation()                              {}
    void decRotation()                              {}
    void setZoom(int);
    void updateScaling();
    void reset();
    void enableMovie(bool)                          {}
    void setXRange(const double &, const double &)  {}
    void setXRangeC(const int &, const int &)       {}
    void setYRange(const double &, const double &)  {}
    void setData(const Fits*)                       {}
    void newData3D();
    void setupColours();
    void setMovieSpeed(int)                         {}
    void setImageCenter(int, int);
    void setCenter(int, int)                        {}
    void setMouseTrackingView(bool)                 {}
    QFitsBaseView* getView()                        { return NULL; }

//----- Slots -----
//----- Signals -----
//----- Members -----
public:  // public for testing purpose
//private:
    QFitsView3D *cubeViewer;
};

//------------------------------------------------------------------------------
//         QFitsView3D
//------------------------------------------------------------------------------
class QFitsView3D : public QGLWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsView3D(QFitsWidget3D* parent);
    ~QFitsView3D();

    void updateScaling();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL( int w, int h );
    void keyPressEvent( QKeyEvent *e );
    void updateData();
    void calcZoomedRanges(int *x1, int *x2, int *y1, int *y2,
                                  int *z1, int *z2);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void calcZoomedVisibleArea();
    void newColourtable();
    void enterEvent(QEvent*);
    void leaveEvent(QEvent*);

//----- Slots -----
public slots:
    void updateColourtable();
    void setImageCenter(int, int);
    void newData();
    void newData2();
    void updateZoom(int);
    void setZoom(int);
    void paintQuickAndDirty();
    void paintSlowAndNice();
    void ResetCamera();
    void updateZRange3D(double, double);

//----- Signals -----
signals:
    void flipX();
    void flipY();
    void decRot();

//----- Members -----
private:
    int previousBuffer;
    bool initialized;
    double default_cam_viewUp[3];
    double default_cam_position[3];
    Fits subcube;
    bool colourtableDirty;
    bool scalingDirty;
    double zmin;
    double zmax;
    QFitsWidget3D* myParent;
    GLubyte glcolors[256][3];
    GLfloat xRot, yRot, zRot, scale;
    int mousex, mousey;
};

#endif /* CUBE_3D_VTK_H */
