#ifndef QFITSWIDGET2D_H
#define QFITSWIDGET2D_H

#include <QWidget>

#include "QFitsBaseWidget.h"
#include "QFitsView2D.h"
#include "QFitsGlobal.h"

class QFitsMainView;
class QFitsView2D;
class QFitsScroller;
class QFitsMarkers;


class QFitsWidget2D : public QFitsBaseWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsWidget2D(QFitsBaseBuffer*);
    ~QFitsWidget2D();

    void saveImage();
    void printImage();

    //
    // overloaded abstract base functions
    //
    void orientationChanged();
    void setFlipX(bool b);
    void setFlipY(bool b);
    void setRotation(int i);
    void setZoom(double);
    void setupColours();
    void setImageCenter(double, double);
    void setCenter(int, int);
    void setMouseTrackingView(bool);
    QFitsMarkers* getSourceMarkers();
    QFitsBaseView* getView()            { return viewer; }
    void copyImage(int);
    QFitsScroller* getScr() { return scroller; }

protected:
        void resizeEvent(QResizeEvent*);
//----- Slots -----
private slots:
    void scrollerMoved();
    void scrollerValueChanged(int);

//----- Signals -----
//----- Members -----
public:
    QFitsView2D *viewer;

private:
    QFitsScroller *scroller;
    bool          doUpdate;
};

#endif /* QFITSWIDGET2D_H */
