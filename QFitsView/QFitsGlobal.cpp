#include <QMouseEvent>
#include <QFile>
#include <QFileDialog>
#include <QString>
#include <QSettings>

#include "QFitsGlobal.h"

QFitsMainWindow     *fitsMainWindow = NULL;
qtdpuser            *dpuser_widget  = NULL;
int                 zoomIndex       = 10;
QString             appDirPath;
qFitsViewSettings   settings;
QRgb                colourTable[NCOLORS],
                    currentTable[NCOLORS];
QReadWriteLock      buffersLock(QReadWriteLock::Recursive);
bool                resetGUIsettings = false;

void addPythonGDLPath() {
    if (!settings.pythonPath.isEmpty()) {
        char *pythonpath = getenv("PYTHONPATH");
        QString env;
        if (pythonpath == NULL) env = "PYTHONPATH=" + settings.pythonPath;
        else env = "PYTHONPATH=" + settings.pythonPath + ":" + pythonpath;
        putenv(strdup(env.toStdString().c_str()));
    }
    if (!settings.GDLPath.isEmpty()) {
        char *gdlpath = getenv("GDL_PATH");
        QString env;
        if (gdlpath == NULL) env = "GDL_PATH=" + settings.GDLPath;
        else env = "GDL_PATH=" + settings.GDLPath + ":" + gdlpath;
        putenv(strdup(env.toStdString().c_str()));
    }
}

// See if documentation exists in some default locations.
// If nothing is found, return "http://www.mpe.mpg.de/~ott/dpuser".
QString searchForDocumentation() {
    if (QFile::exists(appDirPath.left(appDirPath.lastIndexOf("qfitsview", -1, Qt::CaseInsensitive)) +
                                                            "doc/dpuser.css")) {
        return appDirPath.left(appDirPath.lastIndexOf("qfitsview", -1, Qt::CaseInsensitive)) + "doc/";
    }
    if (QFile::exists("/usr/share/doc/dpuser/dpuser.css")) {
        return "/usr/share/doc/dpuser/";
    }
    if (QFile::exists("/usr/share/doc/dpuser/dpuser.css")) {
        return "/usr/share/doc/dpuser/";
    }

    return "http://www.mpe.mpg.de/~ott/dpuser/";
}

//------------------------------------------------------------------------------
//         qFitsViewSettings
//------------------------------------------------------------------------------
qFitsViewSettings::qFitsViewSettings() {
    defaultLimits = 0;
    defaultZoom = 5;
    wiregridwidth = wiregridheight = 50;
    plotstyle = 0;

    showViewingTools = 1;
    showDpuser = 1;
    showTools = 0;

    dpuserlibPath = "";
    pythonLibraryPath = "";

    galfitPath = "";

    width = 800;
    height = 500;

    textfont = "Courier";
    textsize = 12;

    maximized = false;

    readSettings();
};

qFitsViewSettings::~qFitsViewSettings() {
    writeSettings();
}

void qFitsViewSettings::readSettings() {
    loadExtHeight = loadExtWidth = -1;
    loadExtMaximized = false;
#ifdef Q_OS_MAC
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "mpe.mpg.de", "qfitsview");
#else
    QSettings settings("mpe.mpg.de", "qfitsview");
#endif
    settings.beginGroup("display");
    defaultLimits = settings.value("defaultLimits", defaultLimits).toInt();
    defaultZoom = settings.value("defaultZoom", defaultZoom).toInt();
    wiregridwidth = settings.value("wiregridwidth", wiregridwidth).toInt();
    wiregridheight = settings.value("wiregridheight", wiregridheight).toInt();
    plotstyle = settings.value("plotstyle", plotstyle).toInt();
    settings.endGroup();
    settings.beginGroup("gui");
    showViewingTools = settings.value("viewingtools", showViewingTools).toInt();
    showDpuser = settings.value("dpuser", showDpuser).toInt();
    showTools = settings.value("tools", showTools).toInt();
    dpuserlibPath = settings.value("dpuserlibpath", dpuserlibPath).toString();
    pythonLibraryPath = settings.value("pythonlibrarypath", pythonLibraryPath).toString();
    pythonPath = settings.value("pythonpath", pythonPath).toString();
    GDLPath = settings.value("gdlpath", GDLPath).toString();
    galfitPath = settings.value("galfitpath", galfitPath).toString();
    width = settings.value("width", width).toInt();
    height = settings.value("height", height).toInt();
    textfont = settings.value("textfont", textfont).toString();
    textsize = settings.value("textsize", textsize).toInt();
    maximized = settings.value("maximized", maximized).toBool();
    lastOpenPath = settings.value("openpath", lastOpenPath).toString();
    loadExtHeight = settings.value("loadextheight", loadExtHeight).toInt();
    loadExtWidth = settings.value("loadextwidth", loadExtWidth).toInt();
    loadExtMaximized = settings.value("loadextmaximized", loadExtMaximized).toBool();
    if (lastOpenPath.size() == 0) {
    #ifdef Q_WS_X11
        // Linux
        lastOpenPath = ".";
    #else
        // MacOS
        #include <QDir>
        lastOpenPath = QDir::toNativeSeparators(QDir::homePath());
    #endif /* Q_WS_X11 */
    }
    lastSavePath = settings.value("savepath", lastSavePath).toString();
    if (lastSavePath.size() == 0) {
    #ifdef Q_WS_X11
        // Linux
        lastSavePath = ".";
    #else
        // MacOS
        #include <QDir>
        lastSavePath = QDir::toNativeSeparators(QDir::homePath());
    #endif /* Q_WS_X11 */
    }
    lastDpuserPath = settings.value("dpuserpath", lastDpuserPath).toString();
    if (lastDpuserPath.size() == 0) {
    #ifdef Q_WS_X11
        // Linux
        lastDpuserPath = ".";
    #else
        // MacOS
        #include <QDir>
        lastDpuserPath = QDir::toNativeSeparators(QDir::homePath());
    #endif /* Q_WS_X11 */
    }
    settings.endGroup();

    addPythonGDLPath();
}

void qFitsViewSettings::writeSettings() {
#ifdef Q_OS_MAC
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "mpe.mpg.de", "qfitsview");
#else
    QSettings settings("mpe.mpg.de", "qfitsview");
#endif
    settings.beginGroup("display");
    settings.setValue("defaultLimits", defaultLimits);
    settings.setValue("defaultZoom", defaultZoom);
    settings.setValue("wiregridwidth", wiregridwidth);
    settings.setValue("wiregridheight", wiregridheight);
    settings.setValue("plotstyle", plotstyle);
    settings.endGroup();
    settings.beginGroup("gui");
    settings.setValue("viewingtools", showViewingTools);
    settings.setValue("dpuser", showDpuser);
    settings.setValue("tools", showTools);
    settings.setValue("dpuserlibpath", dpuserlibPath);
    settings.setValue("pythonlibrarypath", pythonLibraryPath);
    settings.setValue("pythonpath", pythonPath);
    settings.setValue("gdlpath", GDLPath);
    settings.setValue("galfitpath", galfitPath);
    settings.setValue("width", width);
    settings.setValue("height", height);
    settings.setValue("textfont", textfont);
    settings.setValue("textsize", textsize);
    settings.setValue("maximized", maximized);
    settings.setValue("openpath", lastOpenPath);
    settings.setValue("savepath", lastSavePath);
    settings.setValue("dpuserpath", lastDpuserPath);
    settings.setValue("loadextheight", loadExtHeight);
    settings.setValue("loadextwidth", loadExtWidth);
    settings.setValue("loadextmaximized", loadExtMaximized);
    settings.endGroup();
}
