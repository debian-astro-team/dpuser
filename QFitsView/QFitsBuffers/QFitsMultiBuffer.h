#ifndef QFITSMULTIBUFFER_H
#define QFITSMULTIBUFFER_H

#include <QWidget>
#include "QFitsBaseBuffer.h"

class QGridLayout;
class QFitsBaseBuffer;
class QFitsSingleBuffer;
class QFitsMainView;
class QFitsScroller;
class QFitsWidgetTable;

// Multi buffer implementation, contains a list of QFitsBaseBuffers
class QFitsMultiBuffer : public QFitsBaseBuffer {
    Q_OBJECT
//----- Functions -----
public:
    QFitsMultiBuffer(QFitsMainView*, dpuserType*, int dbg, QString dbg_upper);
protected:
    QFitsMultiBuffer(QFitsMultiBuffer*, dpuserType*, int dbg, QString dbg_upper);
public:
    ~QFitsMultiBuffer();
    void init(dpuserType*, int dbg, QString dbg_upper);

    void activateBuffer();
    BufferAppearance updateAppearance();
    int  indexOf(QFitsBaseBuffer *bb);
    void resizeEvent(QResizeEvent*);
    int  getPos(QFitsSingleBuffer*);
    void setMouseTrackingView(bool, bool traceUp = false);
    void updateHomogenSB();

    //
    // implementations of abstract base functions
    //
    void ChangeWidgetState(dpViewMode);
    void zoomTextChanged(double, FitZoom);
    void showCubePlot();
    void updateLinemapDialog();
    void orientationChanged();
    void setZoomWidget(double);
    void updateScaling();
    void enableMovie(bool);
    void setXRange();
    void setYRange();
    void setYRange(const double &, const double &);
    void setData();
    void newData3D();
    void setMovieSpeed(int);
    void setImageCenter(double, double);
    void setIgnore(bool, double);
    void copyImage(int);
    void setFocus();
    void setLinemapOptions(bool, bool, int, int, int, int, int, int);
    void updateCubeMode();
    bool showSpectrum();
    void createManualSpectrum();

    QFitsBaseWidget*   getState();
    QFitsBaseWidget*   getTableWidget();
    QFitsSingleBuffer* getSB(int);
    QFitsSingleBuffer* getFirstImageBuffer();

    //
    // get/set functions
    //
    bool getHasChildMB()        { return hasChildMB; }
    bool hasCubeSpecHomogenSB() { return cubeSpecHomogenSB; }

    //
    // get/set functions (overrides for QFitsBaseBuffer)
    //
    void setImageScaleRange(dpScaleMode);
    void setCubeMode(dpCubeMode, bool allSB = false);
    void setColormap(QAction*);
    void setZoomFactor(double);
    void setBrightness(int, bool sbInitialized = false);
    void setContrast(int, bool sbInitialized = false);
    void setBrightnessContrast(int, int, bool sbInitialized = false);
    void setImageScalingMethod(int, bool sbInitialized = false);
    void setRotation(int);
    void setInvertColormap(bool);
    void setFlipX(bool);
    void setFlipY(bool);
    void setCubeSpecOrientation(QFV::Orientation, bool allSB = false);
    void setCubeCenter(double, QFV::Orientation, bool allSB = false);
    void setLineWidth(double, QFV::Orientation, bool allSB = false);

protected:
    void setupColours();
    void checkCubeSpecHomogenSB();
    void ResetSpectrum();

//----- Slots -----
//----- Signals -----
//----- Members -----
private:
    QList<QFitsBaseBuffer*> bufferList;
    QGridLayout             *grid;
    QWidget                 *gridWidget;
    bool                    hasChildMB,
                            cubeSpecHomogenSB;

    // MultiTableView
    QFitsWidgetTable        *mtv_tableMulti;
};

#endif // QFITSMULTIBUFFER_H
