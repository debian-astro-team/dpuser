#ifndef QFITSGLOBAL_H
#define QFITSGLOBAL_H

#include <QObject>
#include <QReadWriteLock>
#include <QRgb>
#ifdef WIN
#include <QSettings>
#endif

#define NCOLORS 256

class QFitsMainWindow;
class qtdpuser;
class qFitsViewSettings;

//------------------------------------------------------------------------------
//         Global variables
//------------------------------------------------------------------------------
extern QFitsMainWindow      *fitsMainWindow;
extern qtdpuser             *dpuser_widget;
extern int                  zoomIndex;
extern QString              appDirPath;
extern QRgb                 colourTable[NCOLORS],
                            currentTable[NCOLORS];
extern QReadWriteLock       buffersLock;
extern qFitsViewSettings    settings;
extern bool                 resetGUIsettings;

//------------------------------------------------------------------------------
//         Global enums
//------------------------------------------------------------------------------
namespace QFV {
    enum Orientation {
        Default     = 0,
        Horizontal  = 1,
        Vertical    = 2,
        Wavelength  = 3
    };
}

typedef enum {
    FitZoomNone,
    FitZoomWindow,
    FitZoomWidth,
    FitZoomHeight
} FitZoom;

typedef enum {
    DisplayCubeSingle,
    DisplayCubeAverage,
    DisplayCubeMedian,
    DisplayCubeLinemap
} dpCubeMode;

typedef enum {
    ScaleMinMax = 0,
    Scale999 = 1,
    Scale995 = 2,
    Scale99 = 3,
    Scale98 = 4,
    Scale95 = 5,
    ScaleManual = 6
} dpScaleMode;

typedef enum {
    ViewImage = 0,
    ViewWiregrid = 1,
    ViewContour = 2,
    ViewTable = 3,
//#ifdef HAS_VTK
    View3D = 4,
//#endif
    ViewUndefined = -1
} dpViewMode;

QString searchForDocumentation();
void addPythonGDLPath();

//------------------------------------------------------------------------------
//         Global struct
//------------------------------------------------------------------------------
typedef enum { Tools_None = 0, Tools_2Dfit = 1, Tools_Markpos = 2, Tools_Deblend = 4 } ToolsVisible;

typedef struct _BufferAppearance BufferAppearance;
struct _BufferAppearance {
    QString windowTitle;
    bool    hideWedge,
            hideViewingtools,
            hideViewingtoolsCutsplot,
            hideSpectrum,
            enableSpectrumControls,
            showToolbarZoom,
            showToolbarOrientation,
            showToolbarScaling,
            showToolbarMovie,
            showToolbarArithmetics,
            showToolbarCube,
            enableImredMenu,
            enableArithmeticButtons,
            enableColourmapMenu,
            enableScaleMenu,
            enableZoomMenu;
    ToolsVisible toolsVisible;
};

class qFitsViewSettings {
//----- Functions -----
public:
        qFitsViewSettings();
        ~qFitsViewSettings();
private:
        void readSettings();
        void writeSettings();

//----- Slots -----
//----- Signals -----
//----- Members -----
public:
        int defaultLimits;
        int defaultZoom;
        int showViewingTools;
        int showDpuser;
        int showTools;
        int width, height;
        int wiregridheight, wiregridwidth;
        bool maximized;
        int plotstyle;
        QString textfont;
        int textsize;
        QString dpuserlibPath;
        QString pythonLibraryPath;
        QString pythonPath;
        QString GDLPath;
        QString galfitPath;
        QString lastOpenPath;
        QString lastSavePath;
        QString lastDpuserPath;
        int loadExtWidth, loadExtHeight;
        bool loadExtMaximized;
};

#endif // QFITSGLOBAL_H
