#ifndef QFITSVIEWINGTOOLS_H
#define QFITSVIEWINGTOOLS_H

//
// QFitsViewingTools: The dock window at the left (usually).
// It includes the pixel coordinate window (with WCS information),
// the magnifier, the total view and the cuts plot.
//

#include <QWidget>
#include <QLabel>

#include "fits.h"

typedef enum {
    cpHorizontal = 0,
    cpVertical = 1,
    cpRadialAverage = 2
} CutsPlotStyle;

class QFitsMainWindow;
class QFitsBaseBuffer;
class QFitsSingleBuffer;
class QFitsMag;
class QFitsTotal;
class QFitsCutsPlot;
class QFitsSimplestButton;

class QFitsViewingTools : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsViewingTools(QFitsMainWindow*, int);
    ~QFitsViewingTools() {}

    void refreshPosInfo(int, int);
    void setDistanceInfo(const double &dx, const double &dy, const int &dix, const int &diy, const bool &inPixels);
    void updateRegionInfo();
    void clear();

protected:
    void enterEvent(QEvent*);

//----- Slots -----
public slots:
    void refreshViews();
//    void setRegionInfo(const QString &);

//----- Signals -----
//----- Members -----
public:
    QFitsMag            *magnifier;
    QFitsTotal          *total;
    QFitsCutsPlot       *cuts_plot;

private:
    QFitsMainWindow     *myParent;
    QLabel              *posinfo,
                        *valinfo,
                        *worldinfo,
                        *extrainfo;
};

class QFitsMag : public QWidget {
        Q_OBJECT
//----- Functions -----
public:
    QFitsMag(QFrame*, QFitsViewingTools*);
    ~QFitsMag() {}

    void setCenter(int, int);
    void clear();

protected:
    virtual void paintEvent(QPaintEvent*);

//----- Slots -----
public slots:
    void setDirectPixmap(QPixmap&);

//----- Signals -----
//----- Members -----
private:
    QFitsViewingTools   *myParent;
    QPixmap             directPixmap;
    int                 cenx,
                        ceny;
    bool                clearWidget;
};

class QFitsTotal : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsTotal(QFrame*, QFitsViewingTools*);
    ~QFitsTotal() {}

    void clear();
    bool isCompletelyVisible();

protected:
    void paintEvent(QPaintEvent*);
    void mouseMoveEvent(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);

private:
    void correctVisibleRect(double*, double*);
    void calcVisibleRect();

//----- Slots -----
//----- Signals -----
//#ifdef HAS_VTK
signals:
    void setQuickAndDirty();
    void setSlowAndNice();
//#endif

//----- Members -----
private:
    QFitsViewingTools   *myParent;
    QRect               visibleAreaRect;
    QImage              totalImage;
    double              totalImageScaleFactor;
    int                 zoom,
                        mousex,
                        mousey;
    bool                clearWidget;
};

class QFitsCutsPlot : public QWidget {
    Q_OBJECT
//----- Functions -----
public:
    QFitsCutsPlot(QFrame*, QFitsViewingTools*);
    ~QFitsCutsPlot() {}

    void setCenter(int, int);
//    void setOrientation(int, bool, bool);
    void setLimits(double, double);
    void clear();

//----- Slots -----
public slots:
    void setCutsStyle(int);
    void setCutsWidth(int);
    void setTakeLimits(bool);

protected slots:
    virtual void paintEvent(QPaintEvent*);

//----- Signals -----
//----- Members -----
private:
    QFitsViewingTools   *myParent;
    CutsPlotStyle        style;
    int                 cenx,
                        ceny,
                        cutswidth,
                        rotation;
    double              minlimit,
                        maxlimit;
    bool                flipX,
                        flipY,
                        takeLimits;
    bool                clearWidget;
};

#endif // QFITSVIEWINGTOOLS_H
