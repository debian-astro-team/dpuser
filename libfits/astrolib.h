#ifndef DP_ASTROLIB_H
#define DP_ASTROLIB_H

#include "fits.h"

double ten(const double dd, const double mm, const double ss);
bool find(Fits &image, Fits &result, double hmin, double fwhm, Fits &roundlim, Fits &sharplim, bool PRINT=TRUE);
void precess(double rah, double ram, double ras, double ded, double dem, double des, const double equinox1, const double equinox2);
bool fxcor(Fits &result, Fits &object, const Fits &templat, int contsub);
bool correlmap(Fits &result, Fits &cube, const Fits &templat, int method);
bool longslit(Fits &result, Fits &cube, int xcenter, int ycenter, double angle, int width, double opening_angle = 0.0);
bool twodcut(Fits &result, Fits &image, int xcenter, int ycenter, double angle, int width);
void abszissaGenerate(Fits &values, const Fits &data, int axis);
Fits &primes(int k, Fits &rv);

#endif /* DP_ASTROLIB_H */
