#include <fits.h>

#define result (rv.i4data)

Fits &primes(int k, Fits &rv) {
	rv.create(k, 1, I4);
	
	if (k == 1) {
		result[0] = 2;
		return rv;
	}
	
	result[0] = 2;
	unsigned long n = 3;
	unsigned long count = 1;
	result[count] = 3;

	count++;

	while(count < k) {
		n = n + 2;

		for (int ip = 1; ip < count; ip++) {
			double q = n / result[ip];
			int r = n % result[ip];
			if (r == 0) {
				ip = count + 1;    // n is not prime.
			} else {
				if (q <= result[ip]) {  // n is prime.
					result[count] = n;
					count++;               // compute next prime.
					ip = count + 1;
				}
			}
		}
	}
	return rv;
}
