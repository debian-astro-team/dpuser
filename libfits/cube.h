#ifndef CUBE_H
#define CUBE_H

#ifdef __cplusplus
extern "C" {
#endif

#define cubePT_IGNORE    -1
#define cubePT_USE        0
#define cubePT_FIND       1

typedef struct xyzw_ {
  double x;
  double y;
  double z;
  double w;
} XYZW;

typedef struct dim_ {
  unsigned short x;
  unsigned short y;
  unsigned short z;
} Dim;

/*#define index(d,X,Y,Z)   (d.y*((X*d.x)+Y))+Z*/
#define index(d,X,Y,Z)   (X+Y*d.x+Z*d.x*d.y)
/*return(x+y*naxis[1]+z*naxis[1]*naxis[2]);*/
#define cubsize(d)       (d.x*d.y*d.z)

int Bezier(XYZW *p,int n,double mu, double munk, XYZW *res);
float interpol(float *mycube,short *actioncube,Dim dim);
void interpol1d(float *mycube,short *actioncube,Dim dim, unsigned short nloops);

#ifdef __cplusplus
}
#endif

#endif /* CUBE_H */
