#ifndef DPUSER_YACCHELPER_H
#define DPUSER_YACCHELPER_H

#include "dpuser.h"

//extern nodeType **userprocs;
extern int nuserprocs;
extern char **userprocnames;
extern char **userprocvars;
extern int *userprocnparams;

void yyerror(const char *s);
//nodeType *con(long value);
//nodeType *dcon(double value);
//nodeType *scon(char *value);
//nodeType *ccon(dpComplex *value);
//nodeType *ffcon(char *value);
//nodeType *id(int i);
//nodeType *iddereference(int oper, int i, nodeType *range);
//nodeType *opr(int oper, int nops, ...);
//nodeType *fnc(int oper, int nops, ...);
//nodeType *addfncarg(nodeType *src, int nops, ...);
//nodeType *addfncopt(nodeType *src, char *option);
//nodeType *pgp(int oper, int nops, ...);
//nodeType *addpgplotarg(nodeType *src, int nops, ...);
//nodeType *addprocopt(nodeType *src, char *option);
//nodeType *rng(int nops, ...);
//nodeType *addrngarg(nodeType *src, int nops, ...);
//nodeType *strdef(int nops, ...);
//nodeType *addstrdef(nodeType *src, int nops, ...);
//nodeType *arg(nodeType *par, nodeType *newelement);
void protect(void *);
void unprotect(void *);
void unprotect();
//void freeNode(nodeType *p);
void freePointerlist();

#endif /* DPUSER_YACCHELPER_H */
