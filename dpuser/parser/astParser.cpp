/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "/mnt/c/ottl/project/dpuser/parser/ast.y"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <dpComplex.h>
#include "dpuser.h"
#include "dpuserType.h"
#include "dpuser.yacchelper.h"
#include "dpuser.procs.h"
#include "procedures.h"
#include "functions.h"
#include "dpuser_utils.h"
#include "svn_revision.h"
#include <signal.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_rng.h>
#include <fits.h>
#include <locale.h>

#ifdef WIN32
#pragma warning (disable: 4996) // disable warning for deprecated POSIX name
#include <direct.h>
#endif /* WIN32 */

#include <iostream>
#include <string>
#include "dpuserAST.h"

/*
int astlex();
void asterror(const char *s);
*/

#ifndef NO_READLINE
#ifdef __cplusplus
extern "C" {
#endif
int read_history(const char *);
int write_history(const char *);
void stifle_history(int);
#ifdef __cplusplus
}
#endif
#define DPHISTORY_SIZE 500
#endif /* !NO_READLINE */

#ifdef DPQT
/* extern Fits *defaultBuffer; */
#endif /* DPQT */

int yyparse();
int yylex();
int parseError;
extern int ScriptInterrupt;
double test;
extern dpStringList script;

#define YYERROR_VERBOSE

#line 133 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "astParser.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_ENTER = 3,                      /* ENTER  */
  YYSYMBOL_IF = 4,                         /* IF  */
  YYSYMBOL_FOR = 5,                        /* FOR  */
  YYSYMBOL_WHILE = 6,                      /* WHILE  */
  YYSYMBOL_EXIT = 7,                       /* EXIT  */
  YYSYMBOL_WHERE = 8,                      /* WHERE  */
  YYSYMBOL_HELP = 9,                       /* HELP  */
  YYSYMBOL_OPTION = 10,                    /* OPTION  */
  YYSYMBOL_INTEGER = 11,                   /* INTEGER  */
  YYSYMBOL_REAL = 12,                      /* REAL  */
  YYSYMBOL_COMPLEX = 13,                   /* COMPLEX  */
  YYSYMBOL_STRING = 14,                    /* STRING  */
  YYSYMBOL_FITSFILE = 15,                  /* FITSFILE  */
  YYSYMBOL_IDENTIFIER = 16,                /* IDENTIFIER  */
  YYSYMBOL_USERPRO = 17,                   /* USERPRO  */
  YYSYMBOL_USERFUNC = 18,                  /* USERFUNC  */
  YYSYMBOL_IFX = 19,                       /* IFX  */
  YYSYMBOL_ELSE = 20,                      /* ELSE  */
  YYSYMBOL_AND = 21,                       /* AND  */
  YYSYMBOL_OR = 22,                        /* OR  */
  YYSYMBOL_EQ = 23,                        /* EQ  */
  YYSYMBOL_NE = 24,                        /* NE  */
  YYSYMBOL_GE = 25,                        /* GE  */
  YYSYMBOL_LE = 26,                        /* LE  */
  YYSYMBOL_27_ = 27,                       /* '>'  */
  YYSYMBOL_28_ = 28,                       /* '<'  */
  YYSYMBOL_29_ = 29,                       /* '='  */
  YYSYMBOL_30_ = 30,                       /* '+'  */
  YYSYMBOL_31_ = 31,                       /* '-'  */
  YYSYMBOL_32_ = 32,                       /* '%'  */
  YYSYMBOL_33_ = 33,                       /* '*'  */
  YYSYMBOL_34_ = 34,                       /* '#'  */
  YYSYMBOL_35_ = 35,                       /* '/'  */
  YYSYMBOL_PlusE = 36,                     /* PlusE  */
  YYSYMBOL_MinusE = 37,                    /* MinusE  */
  YYSYMBOL_MulE = 38,                      /* MulE  */
  YYSYMBOL_DivE = 39,                      /* DivE  */
  YYSYMBOL_PlusP = 40,                     /* PlusP  */
  YYSYMBOL_MinusM = 41,                    /* MinusM  */
  YYSYMBOL_42_ = 42,                       /* '^'  */
  YYSYMBOL_UMINUS = 43,                    /* UMINUS  */
  YYSYMBOL_44_ = 44,                       /* '!'  */
  YYSYMBOL_45_ = 45,                       /* '['  */
  YYSYMBOL_46_ = 46,                       /* ']'  */
  YYSYMBOL_47_ = 47,                       /* '}'  */
  YYSYMBOL_48_ = 48,                       /* ')'  */
  YYSYMBOL_49_ = 49,                       /* ','  */
  YYSYMBOL_50_ = 50,                       /* '{'  */
  YYSYMBOL_51_ = 51,                       /* '('  */
  YYSYMBOL_52_ = 52,                       /* ':'  */
  YYSYMBOL_53_ = 53,                       /* '?'  */
  YYSYMBOL_YYACCEPT = 54,                  /* $accept  */
  YYSYMBOL_function = 55,                  /* function  */
  YYSYMBOL_stmt = 56,                      /* stmt  */
  YYSYMBOL_userprocedure = 57,             /* userprocedure  */
  YYSYMBOL_userpro_args = 58,              /* userpro_args  */
  YYSYMBOL_userfunction = 59,              /* userfunction  */
  YYSYMBOL_userfunc_args = 60,             /* userfunc_args  */
  YYSYMBOL_list = 61,                      /* list  */
  YYSYMBOL_assignment = 62,                /* assignment  */
  YYSYMBOL_varchange = 63,                 /* varchange  */
  YYSYMBOL_fullfunc = 64,                  /* fullfunc  */
  YYSYMBOL_funcarg_list = 65,              /* funcarg_list  */
  YYSYMBOL_rangeargs = 66,                 /* rangeargs  */
  YYSYMBOL_rangearg_list = 67,             /* rangearg_list  */
  YYSYMBOL_procedure = 68,                 /* procedure  */
  YYSYMBOL_expr = 69,                      /* expr  */
  YYSYMBOL_boolean = 70                    /* boolean  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   720

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  54
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  17
/* YYNRULES -- Number of rules.  */
#define YYNRULES  109
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  202

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   288


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    44,     2,    34,     2,    32,     2,     2,
      51,    48,    33,    30,    49,    31,     2,    35,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    52,     2,
      28,    29,    27,    53,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    45,     2,    46,    42,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    50,     2,    47,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    36,    37,    38,    39,    40,    41,    43
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   112,   112,   121,   122,   124,   127,   128,   129,   130,
     131,   132,   133,   135,   137,   140,   141,   142,   144,   145,
     146,   147,   148,   167,   168,   171,   172,   173,   176,   177,
     180,   181,   182,   190,   191,   197,   198,   199,   209,   210,
     211,   212,   213,   214,   215,   216,   217,   218,   219,   220,
     221,   248,   249,   253,   254,   255,   256,   257,   258,   262,
     266,   267,   268,   269,   270,   274,   275,   276,   277,   278,
     279,   281,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303,   304,   305,   308,   309,   310,   311,   312,   313,
     314,   315,   316,   317,   318,   319,   320,   321,   322,   323
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "ENTER", "IF", "FOR",
  "WHILE", "EXIT", "WHERE", "HELP", "OPTION", "INTEGER", "REAL", "COMPLEX",
  "STRING", "FITSFILE", "IDENTIFIER", "USERPRO", "USERFUNC", "IFX", "ELSE",
  "AND", "OR", "EQ", "NE", "GE", "LE", "'>'", "'<'", "'='", "'+'", "'-'",
  "'%'", "'*'", "'#'", "'/'", "PlusE", "MinusE", "MulE", "DivE", "PlusP",
  "MinusM", "'^'", "UMINUS", "'!'", "'['", "']'", "'}'", "')'", "','",
  "'{'", "'('", "':'", "'?'", "$accept", "function", "stmt",
  "userprocedure", "userpro_args", "userfunction", "userfunc_args", "list",
  "assignment", "varchange", "fullfunc", "funcarg_list", "rangeargs",
  "rangearg_list", "procedure", "expr", "boolean", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-80)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -80,   237,   -80,    19,   -80,   543,    15,   543,   -80,    24,
      10,   442,   -48,   -29,   -80,   -80,   -80,    21,   -80,    31,
     261,    47,    48,     5,   -80,   543,   -80,   -80,   -80,   -80,
     -80,    16,   543,   543,   543,   484,   543,   -80,    20,   -80,
     -33,   320,     2,    27,     9,   675,     2,   -80,   543,   -80,
     584,   543,   543,   543,   543,   -80,   -80,   543,   242,   675,
       4,    57,   368,    60,   384,    66,   401,    68,   420,   -80,
     -80,   -80,   -80,   -80,   584,    13,   493,    41,     4,    41,
      34,   -80,   675,   647,    11,   -80,   -80,   584,   -80,   534,
     543,   543,   543,   543,   543,   543,   543,   543,   543,   543,
     543,   543,   543,   543,   543,   543,    69,   -80,   543,   543,
     543,    71,   543,   543,   -80,   675,   -15,   -80,   675,     4,
     675,   675,   675,   675,   675,   543,   543,   543,   543,   543,
     -80,   -80,   -80,   -80,   -80,   -80,    27,   -80,   -80,    27,
     -80,   -80,   -80,   675,     4,   -80,    72,   -80,   675,     4,
     -80,   -80,   -80,   675,     4,   -80,   675,   675,    77,    34,
      77,    34,    77,    77,    77,    77,    77,    77,   131,   131,
     404,     1,     1,     1,    41,   288,    77,    34,    77,    34,
     615,   288,     7,   320,   675,   675,   675,   675,   675,    50,
     -80,   543,   -80,    90,   -80,   -80,   675,   423,    65,   242,
     288,   -80
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       5,     0,     1,     0,     7,     0,     0,     0,    22,     0,
       0,     0,     0,     0,    33,     2,     3,     0,     4,     0,
       0,     0,     0,     6,    21,     0,    72,    73,    74,    75,
      76,    77,     0,     0,     0,     0,     0,    88,     0,    90,
       0,     0,     0,     0,     0,     0,     0,    12,     0,     8,
       0,     0,     0,     0,     0,    38,    39,     0,    90,    65,
      66,     0,     0,     0,     0,     0,     0,     0,     0,    11,
      34,     9,    10,    70,     0,     0,     0,    85,     0,    86,
     109,    61,    60,     0,     0,    56,    52,     0,    59,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    17,    89,     0,     0,
       0,    16,     0,     0,    15,    20,    77,    36,    35,    37,
      40,    41,    42,    43,    67,     0,     0,     0,     0,     0,
      45,    46,    25,    23,    30,    28,    26,    27,    24,    31,
      32,    29,    69,    68,    71,    91,     0,    51,    53,    57,
      87,   100,    55,    54,    58,    64,    63,    62,   102,   104,
     106,   108,    99,    98,    96,    97,    95,    94,    78,    79,
      84,    80,    83,    81,    82,     0,   103,   101,   107,   105,
       0,     0,     0,     0,    44,    47,    48,    49,    50,     0,
      19,     0,    18,     0,    14,    92,    93,     0,     0,     0,
       0,    13
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -80,   -80,    -1,   -80,   -80,   -80,   -80,    25,    -2,   -79,
     -80,   -80,    38,   -80,   -80,    42,   225
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,     1,    70,    16,    17,    18,    19,    20,    21,    22,
      37,    38,    39,    40,    23,    45,    78
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      15,    61,    14,     3,    44,     4,     5,     6,     7,     8,
     193,     9,   112,    88,    50,    73,    89,    10,    11,    90,
      63,    14,    24,   108,   109,   108,   109,    47,   108,   109,
      85,    43,   108,   109,   108,   109,    76,    62,    64,    48,
     106,   111,    66,   105,    68,   114,    35,    41,   117,    58,
      71,    72,    14,    59,    74,   110,    50,   110,   113,   151,
     110,   145,   146,   137,   110,   140,   110,    76,    86,    87,
      65,    14,   142,   132,    77,    79,   134,    82,    83,   107,
      67,    14,   136,   107,   139,   152,    35,   110,   189,   175,
     115,   181,   118,   120,   121,   122,   123,   107,   195,   124,
      93,    94,    95,    96,    97,    98,   197,    99,   100,   101,
     102,   103,   104,   200,   198,   107,   143,   107,   148,   105,
     107,   107,    35,     0,     0,     0,     0,     0,     0,   153,
       0,   156,   157,   158,   160,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,     0,     0,
     176,   178,   180,   107,     0,   183,   107,     0,   107,   107,
     107,   107,   107,   101,   102,   103,   104,   184,   185,   186,
     187,   188,     0,   105,   190,     0,    35,     0,     0,     0,
     192,   107,   194,     0,     0,     0,   107,     0,     0,     0,
       0,   107,     0,     0,   107,   107,   107,     0,   107,   201,
     107,   107,   107,   107,   107,   107,   107,   107,   107,   107,
     107,   107,   107,     0,   107,     0,   107,     0,   107,     0,
       0,   107,   107,   107,   107,   107,   107,     0,     0,     0,
      42,     0,    46,   196,   107,   199,    60,     2,     3,     0,
       4,     5,     6,     7,     8,     0,     9,     0,     0,     0,
      75,     0,    10,    11,    12,    13,     0,     0,     0,    80,
       0,    84,     3,     0,     4,     5,     6,     7,     8,     0,
       9,   125,     0,     0,     0,   119,    10,    11,   126,   127,
     128,   129,   130,   131,     0,     0,     0,    14,     0,     3,
       0,     4,     5,     6,     7,     8,     0,     9,     0,   144,
       0,   149,     0,    10,    11,     0,     0,     0,    69,     0,
       0,    14,   154,     0,     0,     0,   159,   161,     0,     0,
       0,     3,     0,     4,     5,     6,     7,     8,     0,     9,
       0,     0,     0,   177,   179,    10,    11,   182,    14,     0,
       0,    91,    92,    93,    94,    95,    96,    97,    98,     0,
      99,   100,   101,   102,   103,   104,     0,     0,     0,     0,
       0,     0,   105,     0,     0,    35,     0,     0,     0,     3,
      14,     4,     5,     6,     7,     8,     0,     9,     0,     0,
       0,     0,     0,    10,    11,     3,     0,     4,     5,     6,
       7,     8,     0,     9,     0,     0,     0,     0,     0,    10,
      11,     0,     3,     0,     4,     5,     6,     7,     8,     0,
       9,     0,     0,     0,     0,   133,    10,    11,    14,     0,
       0,     3,     0,     4,     5,     6,     7,     8,     0,     9,
       0,   135,     0,     0,    14,    10,    11,   102,   103,   104,
       0,     0,     0,     0,     0,    49,   105,     0,   138,    35,
      25,    14,     0,    26,    27,    28,    29,    30,    31,    51,
      52,    53,    54,    55,    56,     0,     0,   141,    35,     0,
      14,    50,    32,    33,     0,     0,     0,     0,    51,    52,
      53,    54,    55,    56,     0,     0,    34,    35,     0,     0,
       0,    57,    25,    36,     0,    26,    27,    28,    29,    30,
      31,    25,     0,     0,    26,    27,    28,    29,    30,    31,
       0,     0,     0,     0,    32,    33,     0,    81,     0,     0,
       0,     0,     0,    32,    33,     0,     0,     0,    34,    35,
       0,     0,     0,     0,     0,    36,     0,    34,    35,     0,
       0,   147,    25,     0,    36,    26,    27,    28,    29,    30,
      31,    25,     0,     0,    26,    27,    28,    29,    30,    31,
       0,     0,     0,     0,    32,    33,     0,   155,     0,     0,
       0,     0,     0,    32,    33,     0,     0,     0,    34,    35,
       0,     0,     0,     0,     0,    36,     0,    34,    35,     0,
       0,     0,    25,     0,    36,    26,    27,    28,    29,    30,
     116,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    32,    33,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    34,    35,
       0,     0,     0,     0,     0,    36,    91,    92,    93,    94,
      95,    96,    97,    98,     0,    99,   100,   101,   102,   103,
     104,     0,     0,     0,     0,     0,     0,   105,     0,     0,
      35,     0,     0,     0,     0,     0,     0,   191,    91,    92,
      93,    94,    95,    96,    97,    98,     0,    99,   100,   101,
     102,   103,   104,     0,     0,     0,     0,     0,     0,   105,
       0,     0,    35,     0,     0,   150,    91,    92,    93,    94,
      95,    96,    97,    98,     0,    99,   100,   101,   102,   103,
     104,     0,     0,     0,     0,     0,     0,   105,     0,     0,
      35
};

static const yytype_int16 yycheck[] =
{
       1,    49,    50,     1,     6,     3,     4,     5,     6,     7,
       3,     9,     3,    46,    29,    10,    49,    15,    16,    52,
      49,    50,     3,    21,    22,    21,    22,     3,    21,    22,
      10,    16,    21,    22,    21,    22,    51,    12,    13,    29,
      41,    42,    17,    42,    19,    46,    45,     5,    50,    11,
       3,     3,    50,    11,    49,    53,    29,    53,    49,    48,
      53,    48,    49,    65,    53,    67,    53,    51,    48,    49,
      49,    50,    74,    16,    32,    33,    16,    35,    36,    41,
      49,    50,    16,    45,    16,    87,    45,    53,    16,    20,
      48,    20,    50,    51,    52,    53,    54,    59,    48,    57,
      23,    24,    25,    26,    27,    28,    16,    30,    31,    32,
      33,    34,    35,    48,   193,    77,    74,    79,    76,    42,
      82,    83,    45,    -1,    -1,    -1,    -1,    -1,    -1,    87,
      -1,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,    -1,    -1,
     108,   109,   110,   115,    -1,   113,   118,    -1,   120,   121,
     122,   123,   124,    32,    33,    34,    35,   125,   126,   127,
     128,   129,    -1,    42,   175,    -1,    45,    -1,    -1,    -1,
     181,   143,   183,    -1,    -1,    -1,   148,    -1,    -1,    -1,
      -1,   153,    -1,    -1,   156,   157,   158,    -1,   160,   200,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,    -1,   176,    -1,   178,    -1,   180,    -1,
      -1,   183,   184,   185,   186,   187,   188,    -1,    -1,    -1,
       5,    -1,     7,   191,   196,   197,    11,     0,     1,    -1,
       3,     4,     5,     6,     7,    -1,     9,    -1,    -1,    -1,
      25,    -1,    15,    16,    17,    18,    -1,    -1,    -1,    34,
      -1,    36,     1,    -1,     3,     4,     5,     6,     7,    -1,
       9,    29,    -1,    -1,    -1,    50,    15,    16,    36,    37,
      38,    39,    40,    41,    -1,    -1,    -1,    50,    -1,     1,
      -1,     3,     4,     5,     6,     7,    -1,     9,    -1,    74,
      -1,    76,    -1,    15,    16,    -1,    -1,    -1,    47,    -1,
      -1,    50,    87,    -1,    -1,    -1,    91,    92,    -1,    -1,
      -1,     1,    -1,     3,     4,     5,     6,     7,    -1,     9,
      -1,    -1,    -1,   108,   109,    15,    16,   112,    50,    -1,
      -1,    21,    22,    23,    24,    25,    26,    27,    28,    -1,
      30,    31,    32,    33,    34,    35,    -1,    -1,    -1,    -1,
      -1,    -1,    42,    -1,    -1,    45,    -1,    -1,    -1,     1,
      50,     3,     4,     5,     6,     7,    -1,     9,    -1,    -1,
      -1,    -1,    -1,    15,    16,     1,    -1,     3,     4,     5,
       6,     7,    -1,     9,    -1,    -1,    -1,    -1,    -1,    15,
      16,    -1,     1,    -1,     3,     4,     5,     6,     7,    -1,
       9,    -1,    -1,    -1,    -1,    47,    15,    16,    50,    -1,
      -1,     1,    -1,     3,     4,     5,     6,     7,    -1,     9,
      -1,    47,    -1,    -1,    50,    15,    16,    33,    34,    35,
      -1,    -1,    -1,    -1,    -1,     3,    42,    -1,    47,    45,
       8,    50,    -1,    11,    12,    13,    14,    15,    16,    36,
      37,    38,    39,    40,    41,    -1,    -1,    47,    45,    -1,
      50,    29,    30,    31,    -1,    -1,    -1,    -1,    36,    37,
      38,    39,    40,    41,    -1,    -1,    44,    45,    -1,    -1,
      -1,    49,     8,    51,    -1,    11,    12,    13,    14,    15,
      16,     8,    -1,    -1,    11,    12,    13,    14,    15,    16,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    -1,    -1,    44,    45,
      -1,    -1,    -1,    -1,    -1,    51,    -1,    44,    45,    -1,
      -1,    48,     8,    -1,    51,    11,    12,    13,    14,    15,
      16,     8,    -1,    -1,    11,    12,    13,    14,    15,    16,
      -1,    -1,    -1,    -1,    30,    31,    -1,    33,    -1,    -1,
      -1,    -1,    -1,    30,    31,    -1,    -1,    -1,    44,    45,
      -1,    -1,    -1,    -1,    -1,    51,    -1,    44,    45,    -1,
      -1,    -1,     8,    -1,    51,    11,    12,    13,    14,    15,
      16,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    30,    31,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    44,    45,
      -1,    -1,    -1,    -1,    -1,    51,    21,    22,    23,    24,
      25,    26,    27,    28,    -1,    30,    31,    32,    33,    34,
      35,    -1,    -1,    -1,    -1,    -1,    -1,    42,    -1,    -1,
      45,    -1,    -1,    -1,    -1,    -1,    -1,    52,    21,    22,
      23,    24,    25,    26,    27,    28,    -1,    30,    31,    32,
      33,    34,    35,    -1,    -1,    -1,    -1,    -1,    -1,    42,
      -1,    -1,    45,    -1,    -1,    48,    21,    22,    23,    24,
      25,    26,    27,    28,    -1,    30,    31,    32,    33,    34,
      35,    -1,    -1,    -1,    -1,    -1,    -1,    42,    -1,    -1,
      45
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    55,     0,     1,     3,     4,     5,     6,     7,     9,
      15,    16,    17,    18,    50,    56,    57,    58,    59,    60,
      61,    62,    63,    68,     3,     8,    11,    12,    13,    14,
      15,    16,    30,    31,    44,    45,    51,    64,    65,    66,
      67,    69,    70,    16,    62,    69,    70,     3,    29,     3,
      29,    36,    37,    38,    39,    40,    41,    49,    66,    69,
      70,    49,    61,    49,    61,    49,    61,    49,    61,    47,
      56,     3,     3,    10,    49,    70,    51,    69,    70,    69,
      70,    33,    69,    69,    70,    10,    48,    49,    46,    49,
      52,    21,    22,    23,    24,    25,    26,    27,    28,    30,
      31,    32,    33,    34,    35,    42,    56,    66,    21,    22,
      53,    56,     3,    49,    56,    69,    16,    62,    69,    70,
      69,    69,    69,    69,    69,    29,    36,    37,    38,    39,
      40,    41,    16,    47,    16,    47,    16,    62,    47,    16,
      62,    47,    62,    69,    70,    48,    49,    48,    69,    70,
      48,    48,    62,    69,    70,    33,    69,    69,    69,    70,
      69,    70,    69,    69,    69,    69,    69,    69,    69,    69,
      69,    69,    69,    69,    69,    20,    69,    70,    69,    70,
      69,    20,    70,    69,    69,    69,    69,    69,    69,    16,
      56,    52,    56,     3,    56,    48,    69,    16,    63,    66,
      48,    56
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    54,    55,    55,    55,    55,    56,    56,    56,    56,
      56,    56,    56,    56,    56,    56,    56,    56,    56,    56,
      56,    56,    56,    57,    57,    58,    58,    58,    59,    59,
      60,    60,    60,    61,    61,    62,    62,    62,    63,    63,
      63,    63,    63,    63,    63,    63,    63,    63,    63,    63,
      63,    64,    64,    65,    65,    65,    65,    65,    65,    66,
      67,    67,    67,    67,    67,    68,    68,    68,    68,    68,
      68,    68,    69,    69,    69,    69,    69,    69,    69,    69,
      69,    69,    69,    69,    69,    69,    69,    69,    69,    69,
      69,    69,    69,    69,    70,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    70,    70,    70,    70,    70,    70
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     2,     2,     0,     1,     1,     2,     2,
       2,     2,     2,     8,     5,     3,     3,     3,     5,     5,
       3,     2,     1,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     1,     2,     3,     3,     3,     2,     2,
       3,     3,     3,     3,     4,     3,     3,     4,     4,     4,
       4,     3,     2,     3,     3,     3,     2,     3,     3,     2,
       2,     2,     3,     3,     3,     2,     2,     3,     3,     3,
       2,     3,     1,     1,     1,     1,     1,     1,     3,     3,
       3,     3,     3,     3,     3,     2,     2,     3,     1,     2,
       1,     3,     5,     5,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     2
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* function: function stmt  */
#line 112 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { 
		try {
		  FitsInterrupt = 0;
		  (yyvsp[0].astnode)->evaluate(); 
		} catch (dpuserTypeException e) {
      dp_output("%s\n", e.reason());
    }
		delete (yyvsp[0].astnode); /* unprotect(); */ freePointerlist(); looplock = 0; }
#line 1430 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 3: /* function: function userprocedure  */
#line 121 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                             { delete (yyvsp[0].astnode); freePointerlist(); }
#line 1436 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 4: /* function: function userfunction  */
#line 122 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                            { delete (yyvsp[0].astnode); freePointerlist(); }
#line 1442 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 6: /* stmt: procedure  */
#line 127 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = (yyvsp[0].astnode); }
#line 1448 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 7: /* stmt: ENTER  */
#line 128 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new statementNode(NULL); }
#line 1454 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 8: /* stmt: IDENTIFIER ENTER  */
#line 129 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                               { (yyval.astnode) = new procedureNode(*(yyvsp[-1].s)); delete (yyvsp[-1].s); }
#line 1460 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 9: /* stmt: assignment ENTER  */
#line 130 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = (yyvsp[-1].astnode); }
#line 1466 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 10: /* stmt: varchange ENTER  */
#line 131 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = (yyvsp[-1].astnode); }
#line 1472 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 11: /* stmt: list '}'  */
#line 132 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = (yyvsp[-1].astnode); }
#line 1478 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 12: /* stmt: HELP ENTER  */
#line 133 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                { (yyval.astnode) = new helpNode(*(yyvsp[-1].s)); delete (yyvsp[-1].s); }
#line 1484 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 13: /* stmt: FOR assignment ENTER boolean ENTER varchange ')' stmt  */
#line 135 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                                        { (yyval.astnode) = new forloopNode((yyvsp[-6].astnode), (yyvsp[-4].astnode), (yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1490 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 14: /* stmt: FOR assignment ',' expr stmt  */
#line 137 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                               { (yyval.astnode) = new forloopNode((yyvsp[-3].astnode), (yyvsp[-1].astnode), (yyvsp[0].astnode)); }
#line 1496 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 15: /* stmt: WHILE boolean stmt  */
#line 140 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                              { (yyval.astnode) = new whileNode((yyvsp[-1].astnode), (yyvsp[0].astnode)); }
#line 1502 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 16: /* stmt: IF boolean stmt  */
#line 141 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                   { (yyval.astnode) = new ifNode((yyvsp[-1].astnode), (yyvsp[0].astnode)); }
#line 1508 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 17: /* stmt: IF expr stmt  */
#line 142 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                   { (yyval.astnode) = new ifNode((yyvsp[-1].astnode), (yyvsp[0].astnode)); }
#line 1514 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 18: /* stmt: IF boolean stmt ELSE stmt  */
#line 144 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                            { (yyval.astnode) = new ifNode((yyvsp[-3].astnode), (yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1520 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 19: /* stmt: IF expr stmt ELSE stmt  */
#line 145 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                            { (yyval.astnode) = new ifNode((yyvsp[-3].astnode), (yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1526 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 20: /* stmt: FITSFILE '=' expr  */
#line 146 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = new writefitsNode(*(yyvsp[-2].sValue), (yyvsp[0].astnode)); delete (yyvsp[-2].sValue); }
#line 1532 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 21: /* stmt: error ENTER  */
#line 147 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = new statementNode(NULL); }
#line 1538 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 22: /* stmt: EXIT  */
#line 148 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    {
			for (auto el:userprocedures) delete el.second;
			for (auto el:userfunctions) delete el.second;
#ifndef NO_READLINE
			stifle_history(DPHISTORY_SIZE);
			write_history("dpuser.history");
#endif /* !NO_READLINE */
			if (xpa != NULL) {
				XPAClose(xpa);
				xpa = NULL;
			}
#ifdef DPQT
      return 0;
#else
			exit(0);
#endif
		}
#line 1560 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 23: /* userprocedure: USERPRO list '}'  */
#line 167 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                  { userprocedures[*(yyvsp[-2].s)] = (yyvsp[-1].astnode); dp_output("stored procedure %s\n", (yyvsp[-2].s)->c_str()); delete (yyvsp[-2].s); (yyval.astnode) = new statementNode(NULL); }
#line 1566 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 24: /* userprocedure: userpro_args list '}'  */
#line 168 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                              { userprocedures[(yyvsp[-2].sl)->front()] = (yyvsp[-1].astnode); userprocedure_arguments[(yyvsp[-2].sl)->front()] = *(yyvsp[-2].sl); dp_output("stored procedure %s\n", (yyvsp[-2].sl)->front().c_str()); delete (yyvsp[-2].sl); (yyval.astnode) = new statementNode(NULL); }
#line 1572 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 25: /* userpro_args: USERPRO ',' IDENTIFIER  */
#line 171 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                     { delete userprocedures[*(yyvsp[-2].s)]; (yyval.sl) = new std::vector<std::string>; (yyval.sl)->push_back(*(yyvsp[-2].s)); (yyval.sl)->push_back(*(yyvsp[0].s)); delete (yyvsp[-2].s); delete (yyvsp[0].s); }
#line 1578 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 26: /* userpro_args: userpro_args ',' IDENTIFIER  */
#line 172 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                 { (yyval.sl)->push_back(*(yyvsp[0].s)); delete (yyvsp[0].s); }
#line 1584 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 27: /* userpro_args: userpro_args ',' assignment  */
#line 173 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                 { (yyval.sl) = (yyvsp[-2].sl); }
#line 1590 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 28: /* userfunction: USERFUNC list '}'  */
#line 176 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                  { userfunctions[*(yyvsp[-2].s)] = (yyvsp[-1].astnode); dp_output("stored function %s\n", (yyvsp[-2].s)->c_str()); delete (yyvsp[-2].s); (yyval.astnode) = new statementNode(NULL); }
#line 1596 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 29: /* userfunction: userfunc_args list '}'  */
#line 177 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                              { userfunctions[(yyvsp[-2].sl)->front()] = (yyvsp[-1].astnode); userfunction_arguments[(yyvsp[-2].sl)->front()] = *(yyvsp[-2].sl); dp_output("stored function %s\n", (yyvsp[-2].sl)->front().c_str()); delete (yyvsp[-2].sl); (yyval.astnode) = new statementNode(NULL); }
#line 1602 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 30: /* userfunc_args: USERFUNC ',' IDENTIFIER  */
#line 180 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                       { delete userfunctions[*(yyvsp[-2].s)]; (yyval.sl) = new std::vector<std::string>; (yyval.sl)->push_back(*(yyvsp[-2].s)); (yyval.sl)->push_back(*(yyvsp[0].s)); delete (yyvsp[-2].s); delete (yyvsp[0].s); }
#line 1608 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 31: /* userfunc_args: userfunc_args ',' IDENTIFIER  */
#line 181 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                  { (yyval.sl)->push_back(*(yyvsp[0].s)); delete (yyvsp[0].s); }
#line 1614 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 32: /* userfunc_args: userfunc_args ',' assignment  */
#line 182 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                  { (yyval.sl) = (yyvsp[-2].sl); }
#line 1620 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 33: /* list: '{'  */
#line 190 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = new listNode(); }
#line 1626 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 34: /* list: list stmt  */
#line 191 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = (yyvsp[-1].astnode)->append((yyvsp[0].astnode)); }
#line 1632 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 35: /* assignment: IDENTIFIER '=' expr  */
#line 197 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                 { (yyval.astnode) = new assignmentNode(*(yyvsp[-2].s), (yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1638 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 36: /* assignment: IDENTIFIER '=' assignment  */
#line 198 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                            { (yyval.astnode) = new assignmentNode(*(yyvsp[-2].s), (yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1644 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 37: /* assignment: IDENTIFIER '=' boolean  */
#line 199 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                         { (yyval.astnode) = new assignmentNode(*(yyvsp[-2].s), (yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1650 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 38: /* varchange: IDENTIFIER PlusP  */
#line 209 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new incrementNode(*(yyvsp[-1].s)); delete (yyvsp[-1].s); }
#line 1656 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 39: /* varchange: IDENTIFIER MinusM  */
#line 210 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new decrementNode(*(yyvsp[-1].s)); delete (yyvsp[-1].s); }
#line 1662 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 40: /* varchange: IDENTIFIER PlusE expr  */
#line 211 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new plusEqualsNode(*(yyvsp[-2].s), (yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1668 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 41: /* varchange: IDENTIFIER MinusE expr  */
#line 212 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new minusEqualsNode(*(yyvsp[-2].s), (yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1674 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 42: /* varchange: IDENTIFIER MulE expr  */
#line 213 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new multiplyEqualsNode(*(yyvsp[-2].s), (yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1680 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 43: /* varchange: IDENTIFIER DivE expr  */
#line 214 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new divideEqualsNode(*(yyvsp[-2].s), (yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1686 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 44: /* varchange: IDENTIFIER rangeargs '=' expr  */
#line 215 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new operatorRangeEqualsNode(*(yyvsp[-3].s), (yyvsp[-2].astnode), (yyvsp[0].astnode)); delete (yyvsp[-3].s); }
#line 1692 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 45: /* varchange: IDENTIFIER rangeargs PlusP  */
#line 216 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new operatorRangePlusPlusNode(*(yyvsp[-2].s), (yyvsp[-1].astnode)); delete (yyvsp[-2].s); }
#line 1698 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 46: /* varchange: IDENTIFIER rangeargs MinusM  */
#line 217 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new operatorRangeMinusMinusNode(*(yyvsp[-2].s), (yyvsp[-1].astnode)); delete (yyvsp[-2].s); }
#line 1704 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 47: /* varchange: IDENTIFIER rangeargs PlusE expr  */
#line 218 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new operatorRangePlusEqualNode(*(yyvsp[-3].s), (yyvsp[-2].astnode), (yyvsp[0].astnode)); delete (yyvsp[-3].s); }
#line 1710 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 48: /* varchange: IDENTIFIER rangeargs MinusE expr  */
#line 219 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new operatorRangeMinusEqualNode(*(yyvsp[-3].s), (yyvsp[-2].astnode), (yyvsp[0].astnode)); delete (yyvsp[-3].s); }
#line 1716 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 49: /* varchange: IDENTIFIER rangeargs MulE expr  */
#line 220 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new operatorRangeMultiplyEqualNode(*(yyvsp[-3].s), (yyvsp[-2].astnode), (yyvsp[0].astnode)); delete (yyvsp[-3].s); }
#line 1722 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 50: /* varchange: IDENTIFIER rangeargs DivE expr  */
#line 221 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                       { (yyval.astnode) = new operatorRangeDivideEqualNode(*(yyvsp[-3].s), (yyvsp[-2].astnode), (yyvsp[0].astnode)); delete (yyvsp[-3].s); }
#line 1728 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 51: /* fullfunc: IDENTIFIER '(' ')'  */
#line 248 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new functionNode(*(yyvsp[-2].s)); delete (yyvsp[-2].s); }
#line 1734 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 52: /* fullfunc: funcarg_list ')'  */
#line 249 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                   { (yyval.astnode) = (yyvsp[-1].astnode); }
#line 1740 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 53: /* funcarg_list: IDENTIFIER '(' expr  */
#line 253 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                           { (yyval.astnode) = new functionNode(*(yyvsp[-2].s)); delete (yyvsp[-2].s); (yyval.astnode)->append((yyvsp[0].astnode)); }
#line 1746 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 54: /* funcarg_list: funcarg_list ',' expr  */
#line 254 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); }
#line 1752 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 55: /* funcarg_list: funcarg_list ',' assignment  */
#line 255 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); }
#line 1758 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 56: /* funcarg_list: funcarg_list OPTION  */
#line 256 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = (yyvsp[-1].astnode)->append(*(yyvsp[0].s)); delete (yyvsp[0].s); }
#line 1764 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 57: /* funcarg_list: IDENTIFIER '(' boolean  */
#line 257 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                            { (yyval.astnode) = new functionNode(*(yyvsp[-2].s)); delete (yyvsp[-2].s); (yyval.astnode)->append((yyvsp[0].astnode)); }
#line 1770 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 58: /* funcarg_list: funcarg_list ',' boolean  */
#line 258 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                             { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); }
#line 1776 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 59: /* rangeargs: rangearg_list ']'  */
#line 262 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                           { (yyval.astnode) = (yyvsp[-1].astnode); (yyval.astnode)->append(" "); }
#line 1782 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 60: /* rangearg_list: '[' expr  */
#line 266 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = new rangeNode(); (yyval.astnode)->append((yyvsp[0].astnode)); (yyval.astnode)->append(" "); }
#line 1788 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 61: /* rangearg_list: '[' '*'  */
#line 267 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = new rangeNode(); (yyval.astnode)->append(NULL); (yyval.astnode)->append(" "); }
#line 1794 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 62: /* rangearg_list: rangearg_list ':' expr  */
#line 268 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); (yyvsp[-2].astnode)->append(":"); }
#line 1800 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 63: /* rangearg_list: rangearg_list ',' expr  */
#line 269 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); (yyvsp[-2].astnode)->append(","); }
#line 1806 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 64: /* rangearg_list: rangearg_list ',' '*'  */
#line 270 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = (yyvsp[-2].astnode)->append(NULL); (yyvsp[-2].astnode)->append(" "); }
#line 1812 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 65: /* procedure: IDENTIFIER expr  */
#line 274 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                          { (yyval.astnode) = new procedureNode(*(yyvsp[-1].s)); (yyval.astnode)->append((yyvsp[0].astnode)); delete (yyvsp[-1].s); }
#line 1818 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 66: /* procedure: IDENTIFIER boolean  */
#line 275 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                          { (yyval.astnode) = new procedureNode(*(yyvsp[-1].s)); (yyval.astnode)->append((yyvsp[0].astnode)); delete (yyvsp[-1].s); }
#line 1824 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 67: /* procedure: IDENTIFIER ',' expr  */
#line 276 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                          { (yyval.astnode) = new procedureNode(*(yyvsp[-2].s)); (yyval.astnode)->append((yyvsp[0].astnode)); delete (yyvsp[-2].s); }
#line 1830 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 68: /* procedure: procedure ',' expr  */
#line 277 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                          { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); }
#line 1836 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 69: /* procedure: procedure ',' assignment  */
#line 278 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                           { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); }
#line 1842 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 70: /* procedure: procedure OPTION  */
#line 279 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                          { (yyval.astnode) = (yyvsp[-1].astnode)->append(*(yyvsp[0].s)); delete (yyvsp[0].s); }
#line 1848 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 71: /* procedure: procedure ',' boolean  */
#line 281 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                          { (yyval.astnode) = (yyvsp[-2].astnode)->append((yyvsp[0].astnode)); }
#line 1854 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 72: /* expr: INTEGER  */
#line 284 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new numberNode((yyvsp[0].iValue)); }
#line 1860 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 73: /* expr: REAL  */
#line 285 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new numberNode((yyvsp[0].dValue)); }
#line 1866 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 74: /* expr: COMPLEX  */
#line 286 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new numberNode(0.0, (yyvsp[0].cValue)); }
#line 1872 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 75: /* expr: STRING  */
#line 287 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new numberNode(*(yyvsp[0].sValue)); delete (yyvsp[0].sValue); }
#line 1878 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 76: /* expr: FITSFILE  */
#line 288 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new numberNode(*(yyvsp[0].sValue), false); delete (yyvsp[0].sValue); }
#line 1884 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 77: /* expr: IDENTIFIER  */
#line 289 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new variableNode(*(yyvsp[0].s)); delete (yyvsp[0].s); }
#line 1890 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 78: /* expr: expr '+' expr  */
#line 290 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new plusNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1896 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 79: /* expr: expr '-' expr  */
#line 291 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new minusNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1902 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 80: /* expr: expr '*' expr  */
#line 292 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new multiplyNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1908 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 81: /* expr: expr '/' expr  */
#line 293 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new divideNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1914 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 82: /* expr: expr '^' expr  */
#line 294 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new powerNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1920 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 83: /* expr: expr '#' expr  */
#line 295 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new matrixmulNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1926 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 84: /* expr: expr '%' expr  */
#line 296 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                        { (yyval.astnode) = new moduloNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1932 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 85: /* expr: '+' expr  */
#line 297 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                { (yyval.astnode) =  (yyvsp[0].astnode); }
#line 1938 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 86: /* expr: '-' expr  */
#line 298 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                { (yyval.astnode) = new unaryMinusNode((yyvsp[0].astnode)); }
#line 1944 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 87: /* expr: '(' expr ')'  */
#line 299 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                { (yyval.astnode) =  (yyvsp[-1].astnode); }
#line 1950 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 88: /* expr: fullfunc  */
#line 300 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = (yyvsp[0].astnode); }
#line 1956 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 89: /* expr: expr rangeargs  */
#line 301 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = new extractrangeNode((yyvsp[-1].astnode), (yyvsp[0].astnode)); }
#line 1962 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 90: /* expr: rangeargs  */
#line 302 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = new createrangeNode((yyvsp[0].astnode)); }
#line 1968 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 91: /* expr: WHERE boolean ')'  */
#line 303 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                    { (yyval.astnode) = new whereNode((yyvsp[-1].astnode)); }
#line 1974 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 92: /* expr: WHERE boolean ',' IDENTIFIER ')'  */
#line 304 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                   { (yyval.astnode) = new whereNode((yyvsp[-3].astnode), *(yyvsp[-1].s)); delete (yyvsp[-1].s); }
#line 1980 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 93: /* expr: boolean '?' expr ':' expr  */
#line 305 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                   { (yyval.astnode) = new ifNode((yyvsp[-4].astnode), (yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1986 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 94: /* boolean: expr '<' expr  */
#line 308 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                            { (yyval.astnode) = new lessThanNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1992 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 95: /* boolean: expr '>' expr  */
#line 309 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                         { (yyval.astnode) = new greaterThanNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 1998 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 96: /* boolean: expr GE expr  */
#line 310 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                         { (yyval.astnode) = new greaterEqualNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2004 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 97: /* boolean: expr LE expr  */
#line 311 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                         { (yyval.astnode) = new lessEqualNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2010 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 98: /* boolean: expr NE expr  */
#line 312 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                         { (yyval.astnode) = new notEqualNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2016 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 99: /* boolean: expr EQ expr  */
#line 313 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                         { (yyval.astnode) = new equalNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2022 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 100: /* boolean: '(' boolean ')'  */
#line 314 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = (yyvsp[-1].astnode); }
#line 2028 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 101: /* boolean: boolean AND boolean  */
#line 315 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = new andNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2034 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 102: /* boolean: expr AND expr  */
#line 316 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = new andNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2040 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 103: /* boolean: boolean AND expr  */
#line 317 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                        { (yyval.astnode) = new andNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2046 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 104: /* boolean: expr AND boolean  */
#line 318 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                        { (yyval.astnode) = new andNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2052 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 105: /* boolean: boolean OR boolean  */
#line 319 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = new orNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2058 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 106: /* boolean: expr OR expr  */
#line 320 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = new orNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2064 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 107: /* boolean: boolean OR expr  */
#line 321 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                        { (yyval.astnode) = new orNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2070 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 108: /* boolean: expr OR boolean  */
#line 322 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                        { (yyval.astnode) = new orNode((yyvsp[-2].astnode), (yyvsp[0].astnode)); }
#line 2076 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;

  case 109: /* boolean: '!' boolean  */
#line 323 "/mnt/c/ottl/project/dpuser/parser/ast.y"
                                                     { (yyval.astnode) = new notNode((yyvsp[0].astnode)); }
#line 2082 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"
    break;


#line 2086 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 326 "/mnt/c/ottl/project/dpuser/parser/ast.y"

const char *info_startup[] = {
DPUSERVERSION2 DP_VERSION,
GetRevString(),
#ifdef DBG
"DEBUG Version",
#endif
"Written since 1999 by Thomas Ott",
"Inspired by the original MPE dp_user speckle data reduction software",
" ",
"For basic information, type \"help\" at the DPUSER> prompt.",
"Online documentation available at: http://www.mpe.mpg.de/~ott/dpuser",
""
};

void controlcsignal(int signum) {
	dp_output("Interrupt encountered.\n");
	FitsInterrupt = 1;
	ScriptInterrupt = 1;
}

/* install new error handler */
void dpuser_gsl_error_handler(const char *reason, const char *file, int line, int gsl_errno) {
//	dp_output("GSL error: %s - %s\n", reason, gsl_strerror(gsl_errno));
}

#ifdef DPQT
int init_dpuser() {
#else
int main(int argc, char *argv[]) {
#endif /* DPQT */
	int i;

	setlocale(LC_NUMERIC, "C");
	init_functions();
	init_procedures();
	CreateHelpMaps();
	gsl_set_error_handler(&dpuser_gsl_error_handler);
	i = 0;
	while (strlen(info_startup[i]) > 0) {
		dp_output("%s\n", info_startup[i++]);
	}
	
	createGlobalVariables();

	nprotected = 100;
	protectedpntrs = (void **)malloc(nprotected * sizeof(void *));
	for (i = 0; i < nprotected; i++) protectedpntrs[i] = NULL;

	nglobals = 8;

#ifndef DPQT
/* command line arguments, if applicable */
	if (argc > 1) {
		dpuser_vars["argv"].type = typeStrarr;
		dpuser_vars["argv"].arrvalue = CreateStringArray();
		for (i = 1; i < argc; i++) *dpuser_vars["argv"].arrvalue += argv[i];
	}

/* PLPLOT init */
	init_dpplplot();
#endif /* DPQT */

/* add QFitsView default variable */
#ifdef DPQT
/*	svariables[8] = "buffer1";
	variables[8].fvalue = defaultBuffer;
	variables[8].type = typeFits; */
#endif /* DPQT */

/* set number of functions */
//	nfunctions = 0;
//	nfunctions = funcs.size();
//	while (strlen(funcss[nfunctions].name) > 0) nfunctions++;
	dp_output("%i functions registered.\n", funcs.size());
	
/* set number of pgplot routines */
	npg = 0;
	npg = procs.size();
//	while (strlen(pgs[npg].name) > 0) npg++;
	dp_output("%i procedures registered.\n", npg - 126);
#ifdef HAS_PGPLOT
	dp_output("126 pgplot procedures registered.\n");
#else
  dp_output("This version was compiled without PGPLOT support.\n");
#endif /* HAS_PLPLOT */

#ifndef DPQT
	signal(SIGINT, &controlcsignal);
#endif /* !DPQT */

#ifndef NO_READLINE
	read_history("dpuser.history");
#endif /* !NO_READLINE */

#ifdef WIN32
/* On Windows, set up directory for PGPLOT */
/* TO: Commented out 02072014: Not needed anymore with our own modified pgplot
	if (getenv("PGPLOT_DIR") == NULL) {
		char *env, *cwd;
		
		cwd = (char *)malloc(256 * sizeof(char));
		if (cwd != NULL) {
			getcwd(cwd, 255);
			env = (char *)malloc((strlen(cwd) + 12) * sizeof(char));
			if (env != NULL) {
				sprintf(env, "PGPLOT_DIR=%s", cwd);
				putenv(env);
				dp_output("Environment variable %s set.\n", env);
			}
			free(cwd);
		}
	}
*/
#endif /* WIN32 */

/* Set up random number generator */
	const gsl_rng_type * T;

	gsl_rng_env_setup();

	gsl_rng_default_seed = time(NULL);
	T = gsl_rng_default;
	gsl_rng_r = gsl_rng_alloc (T);
	
//	idum = -time(NULL);
//	ran1(&idum);

/* run startup script, if available */
	silence = 1;

	script.clear();
	script += "{startup_=getenv(\"DPUSER\");if startup_==\"\" startup_=getenv(\"HOME\");if(startup_!=\"\"){if startup_[strlen(startup_)]!=\"/\" startup_+=\"/\";}startup_+=\"startup.dpuser\";if fileexists(startup_)==1 run startup_}";
	script += "echo 1";
	
	yyparse();

	if (xpa != NULL) {
		XPAClose(xpa);
		xpa = NULL;
	}

#ifdef DPQT
	return 0;
#else
	exit(0);
#endif
}

/*
extern int astparse();

int main() { initialize_dpuser_functions(); astparse(); }

void asterror(const char *s) { std::cout << s << std::endl; }
*/
