/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_MNT_C_OTTL_PROJECT_QFITSVIEW_BUILD_LINUX_ASTPARSER_H_INCLUDED
# define YY_YY_MNT_C_OTTL_PROJECT_QFITSVIEW_BUILD_LINUX_ASTPARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    ENTER = 258,                   /* ENTER  */
    IF = 259,                      /* IF  */
    FOR = 260,                     /* FOR  */
    WHILE = 261,                   /* WHILE  */
    EXIT = 262,                    /* EXIT  */
    WHERE = 263,                   /* WHERE  */
    HELP = 264,                    /* HELP  */
    OPTION = 265,                  /* OPTION  */
    INTEGER = 266,                 /* INTEGER  */
    REAL = 267,                    /* REAL  */
    COMPLEX = 268,                 /* COMPLEX  */
    STRING = 269,                  /* STRING  */
    FITSFILE = 270,                /* FITSFILE  */
    IDENTIFIER = 271,              /* IDENTIFIER  */
    USERPRO = 272,                 /* USERPRO  */
    USERFUNC = 273,                /* USERFUNC  */
    IFX = 274,                     /* IFX  */
    ELSE = 275,                    /* ELSE  */
    AND = 276,                     /* AND  */
    OR = 277,                      /* OR  */
    EQ = 278,                      /* EQ  */
    NE = 279,                      /* NE  */
    GE = 280,                      /* GE  */
    LE = 281,                      /* LE  */
    PlusE = 282,                   /* PlusE  */
    MinusE = 283,                  /* MinusE  */
    MulE = 284,                    /* MulE  */
    DivE = 285,                    /* DivE  */
    PlusP = 286,                   /* PlusP  */
    MinusM = 287,                  /* MinusM  */
    UMINUS = 288                   /* UMINUS  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 63 "/mnt/c/ottl/project/dpuser/parser/ast.y"

	long iValue;
	double dValue;
	double cValue;
	std::string *s;
	int f;
	ASTNode *astnode;
	std::string *sValue;
	std::vector<std::string> *sl;

#line 108 "/mnt/c/ottl/project/QFitsView/build-linux/astParser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_MNT_C_OTTL_PROJECT_QFITSVIEW_BUILD_LINUX_ASTPARSER_H_INCLUDED  */
