/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with MPFIT_ or mpfit_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with mpfit or MPFIT, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define MPFITBISON 30802

/* Bison version string.  */
#define MPFITBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define MPFITSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define MPFITPURE 0

/* Push parsers.  */
#define MPFITPUSH 0

/* Pull parsers.  */
#define MPFITPULL 1




/* First part of user prologue.  */
#line 1 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"

#include <iostream>
#include <string>
#include <math.h>
#include "mpfitAST.h"

int mpfitlex();
void mpfiterror(const char *s);

#line 81 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"

# ifndef MPFIT_CAST
#  ifdef __cplusplus
#   define MPFIT_CAST(Type, Val) static_cast<Type> (Val)
#   define MPFIT_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define MPFIT_CAST(Type, Val) ((Type) (Val))
#   define MPFIT_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef MPFIT_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define MPFIT_NULLPTR nullptr
#   else
#    define MPFIT_NULLPTR 0
#   endif
#  else
#   define MPFIT_NULLPTR ((void*)0)
#  endif
# endif

#include "mpfitParser.h"
/* Symbol kind.  */
enum mpfitsymbol_kind_t
{
  MPFITSYMBOL_MPFITEMPTY = -2,
  MPFITSYMBOL_MPFITEOF = 0,                      /* "end of file"  */
  MPFITSYMBOL_MPFITerror = 1,                    /* error  */
  MPFITSYMBOL_MPFITUNDEF = 2,                    /* "invalid token"  */
  MPFITSYMBOL_BATATA = 3,                     /* BATATA  */
  MPFITSYMBOL_ENTER = 4,                      /* ENTER  */
  MPFITSYMBOL_REAL = 5,                       /* REAL  */
  MPFITSYMBOL_FUNC = 6,                       /* FUNC  */
  MPFITSYMBOL_VAR = 7,                        /* VAR  */
  MPFITSYMBOL_8_ = 8,                         /* '='  */
  MPFITSYMBOL_9_ = 9,                         /* '+'  */
  MPFITSYMBOL_10_ = 10,                       /* '-'  */
  MPFITSYMBOL_11_ = 11,                       /* '*'  */
  MPFITSYMBOL_12_ = 12,                       /* '/'  */
  MPFITSYMBOL_13_ = 13,                       /* '^'  */
  MPFITSYMBOL_14_ = 14,                       /* ';'  */
  MPFITSYMBOL_15_ = 15,                       /* '('  */
  MPFITSYMBOL_16_ = 16,                       /* ')'  */
  MPFITSYMBOL_17_ = 17,                       /* ','  */
  MPFITSYMBOL_MPFITACCEPT = 18,                  /* $accept  */
  MPFITSYMBOL_list = 19,                      /* list  */
  MPFITSYMBOL_stmt = 20,                      /* stmt  */
  MPFITSYMBOL_expr = 21                       /* expr  */
};
typedef enum mpfitsymbol_kind_t mpfitsymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define MPFIT_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ mpfittype_int8;
#elif defined MPFIT_STDINT_H
typedef int_least8_t mpfittype_int8;
#else
typedef signed char mpfittype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ mpfittype_int16;
#elif defined MPFIT_STDINT_H
typedef int_least16_t mpfittype_int16;
#else
typedef short mpfittype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ mpfittype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined MPFIT_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t mpfittype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char mpfittype_uint8;
#else
typedef short mpfittype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ mpfittype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined MPFIT_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t mpfittype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short mpfittype_uint16;
#else
typedef int mpfittype_uint16;
#endif

#ifndef MPFITPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define MPFITPTRDIFF_T __PTRDIFF_TYPE__
#  define MPFITPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define MPFITPTRDIFF_T ptrdiff_t
#  define MPFITPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define MPFITPTRDIFF_T long
#  define MPFITPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef MPFITSIZE_T
# ifdef __SIZE_TYPE__
#  define MPFITSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define MPFITSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define MPFITSIZE_T size_t
# else
#  define MPFITSIZE_T unsigned
# endif
#endif

#define MPFITSIZE_MAXIMUM                                  \
  MPFIT_CAST (MPFITPTRDIFF_T,                                 \
           (MPFITPTRDIFF_MAXIMUM < MPFIT_CAST (MPFITSIZE_T, -1)  \
            ? MPFITPTRDIFF_MAXIMUM                         \
            : MPFIT_CAST (MPFITSIZE_T, -1)))

#define MPFITSIZEOF(X) MPFIT_CAST (MPFITPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef mpfittype_int8 mpfit_state_t;

/* State numbers in computations.  */
typedef int mpfit_state_fast_t;

#ifndef MPFIT_
# if defined MPFITENABLE_NLS && MPFITENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define MPFIT_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef MPFIT_
#  define MPFIT_(Msgid) Msgid
# endif
#endif


#ifndef MPFIT_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define MPFIT_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define MPFIT_ATTRIBUTE_PURE
# endif
#endif

#ifndef MPFIT_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define MPFIT_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define MPFIT_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define MPFIT_USE(E) ((void) (E))
#else
# define MPFIT_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about mpfitlval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define MPFIT_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define MPFIT_INITIAL_VALUE(Value) Value
#endif
#ifndef MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define MPFIT_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef MPFIT_INITIAL_VALUE
# define MPFIT_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define MPFIT_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define MPFIT_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef MPFIT_IGNORE_USELESS_CAST_BEGIN
# define MPFIT_IGNORE_USELESS_CAST_BEGIN
# define MPFIT_IGNORE_USELESS_CAST_END
#endif


#define MPFIT_ASSERT(E) ((void) (0 && (E)))

#if !defined mpfitoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef MPFITSTACK_USE_ALLOCA
#  if MPFITSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define MPFITSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define MPFITSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define MPFITSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef MPFITSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define MPFITSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef MPFITSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define MPFITSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define MPFITSTACK_ALLOC MPFITMALLOC
#  define MPFITSTACK_FREE MPFITFREE
#  ifndef MPFITSTACK_ALLOC_MAXIMUM
#   define MPFITSTACK_ALLOC_MAXIMUM MPFITSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined MPFITMALLOC || defined malloc) \
             && (defined MPFITFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef MPFITMALLOC
#   define MPFITMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (MPFITSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef MPFITFREE
#   define MPFITFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined mpfitoverflow */

#if (! defined mpfitoverflow \
     && (! defined __cplusplus \
         || (defined MPFITSTYPE_IS_TRIVIAL && MPFITSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union mpfitalloc
{
  mpfit_state_t mpfitss_alloc;
  MPFITSTYPE mpfitvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define MPFITSTACK_GAP_MAXIMUM (MPFITSIZEOF (union mpfitalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define MPFITSTACK_BYTES(N) \
     ((N) * (MPFITSIZEOF (mpfit_state_t) + MPFITSIZEOF (MPFITSTYPE)) \
      + MPFITSTACK_GAP_MAXIMUM)

# define MPFITCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables MPFITSIZE and MPFITSTACKSIZE give the old and new number of
   elements in the stack, and MPFITPTR gives the new location of the
   stack.  Advance MPFITPTR to a properly aligned location for the next
   stack.  */
# define MPFITSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        MPFITPTRDIFF_T mpfitnewbytes;                                         \
        MPFITCOPY (&mpfitptr->Stack_alloc, Stack, mpfitsize);                    \
        Stack = &mpfitptr->Stack_alloc;                                    \
        mpfitnewbytes = mpfitstacksize * MPFITSIZEOF (*Stack) + MPFITSTACK_GAP_MAXIMUM; \
        mpfitptr += mpfitnewbytes / MPFITSIZEOF (*mpfitptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined MPFITCOPY_NEEDED && MPFITCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef MPFITCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define MPFITCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, MPFIT_CAST (MPFITSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define MPFITCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          MPFITPTRDIFF_T mpfiti;                      \
          for (mpfiti = 0; mpfiti < (Count); mpfiti++)   \
            (Dst)[mpfiti] = (Src)[mpfiti];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !MPFITCOPY_NEEDED */

/* MPFITFINAL -- State number of the termination state.  */
#define MPFITFINAL  17
/* MPFITLAST -- Last index in MPFITTABLE.  */
#define MPFITLAST   95

/* MPFITNTOKENS -- Number of terminals.  */
#define MPFITNTOKENS  18
/* MPFITNNTS -- Number of nonterminals.  */
#define MPFITNNTS  4
/* MPFITNRULES -- Number of rules.  */
#define MPFITNRULES  20
/* MPFITNSTATES -- Number of states.  */
#define MPFITNSTATES  40

/* MPFITMAXUTOK -- Last valid token kind.  */
#define MPFITMAXUTOK   262


/* MPFITTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by mpfitlex, with out-of-bounds checking.  */
#define MPFITTRANSLATE(MPFITX)                                \
  (0 <= (MPFITX) && (MPFITX) <= MPFITMAXUTOK                     \
   ? MPFIT_CAST (mpfitsymbol_kind_t, mpfittranslate[MPFITX])        \
   : MPFITSYMBOL_MPFITUNDEF)

/* MPFITTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by mpfitlex.  */
static const mpfittype_int8 mpfittranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      15,    16,    11,     9,    17,    10,     2,    12,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    14,
       2,     8,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,    13,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7
};

#if MPFITDEBUG
/* MPFITRLINE[MPFITN] -- Source line where rule number MPFITN was defined.  */
static const mpfittype_int8 mpfitrline[] =
{
       0,    28,    28,    29,    32,    33,    34,    35,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50
};
#endif

/** Accessing symbol of state STATE.  */
#define MPFIT_ACCESSING_SYMBOL(State) MPFIT_CAST (mpfitsymbol_kind_t, mpfitstos[State])

#if MPFITDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   MPFITSYMBOL.  No bounds checking.  */
static const char *mpfitsymbol_name (mpfitsymbol_kind_t mpfitsymbol) MPFIT_ATTRIBUTE_UNUSED;

/* MPFITTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at MPFITNTOKENS, nonterminals.  */
static const char *const mpfittname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "BATATA", "ENTER",
  "REAL", "FUNC", "VAR", "'='", "'+'", "'-'", "'*'", "'/'", "'^'", "';'",
  "'('", "')'", "','", "$accept", "list", "stmt", "expr", MPFIT_NULLPTR
};

static const char *
mpfitsymbol_name (mpfitsymbol_kind_t mpfitsymbol)
{
  return mpfittname[mpfitsymbol];
}
#endif

#define MPFITPACT_NINF (-9)

#define mpfitpact_value_is_default(Yyn) \
  ((Yyn) == MPFITPACT_NINF)

#define MPFITTABLE_NINF (-1)

#define mpfittable_value_is_error(Yyn) \
  0

/* MPFITPACT[STATE-NUM] -- Index in MPFITTABLE of the portion describing
   STATE-NUM.  */
static const mpfittype_int8 mpfitpact[] =
{
      34,    -9,    -9,    41,    -7,    41,    41,    41,    22,    -9,
       2,    -9,    48,    41,    -9,    -9,    66,    -9,    -9,    -9,
      41,    41,    41,    41,    41,    -9,    -9,    41,    82,    -9,
      -4,    -4,    -8,    -8,    -9,    57,    -9,    41,    74,    -9
};

/* MPFITDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when MPFITTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const mpfittype_int8 mpfitdefact[] =
{
       0,     6,     8,     0,     9,     0,     0,     0,     0,     2,
       0,     9,     0,     0,    15,    16,     0,     1,     3,     5,
       0,     0,     0,     0,     0,     4,    18,     0,     7,    17,
      10,    11,    12,    13,    14,     0,    19,     0,     0,    20
};

/* MPFITPGOTO[NTERM-NUM].  */
static const mpfittype_int8 mpfitpgoto[] =
{
      -9,    -9,    15,    -3
};

/* MPFITDEFGOTO[NTERM-NUM].  */
static const mpfittype_int8 mpfitdefgoto[] =
{
       0,     8,     9,    10
};

/* MPFITTABLE[MPFITPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If MPFITTABLE_NINF, syntax error.  */
static const mpfittype_int8 mpfittable[] =
{
      12,    13,    14,    15,    16,    24,    19,    22,    23,    24,
      28,    20,    21,    22,    23,    24,    25,    30,    31,    32,
      33,    34,    17,    18,    35,     0,     1,     2,     3,     4,
       0,     5,     6,     0,    38,     0,     0,     7,     1,     2,
       3,     4,     0,     5,     6,     0,     2,     3,    11,     7,
       5,     6,     0,     0,     0,     0,     7,    20,    21,    22,
      23,    24,     0,     0,    26,    27,    20,    21,    22,    23,
      24,     0,     0,    36,    37,    20,    21,    22,    23,    24,
       0,     0,    29,    20,    21,    22,    23,    24,     0,     0,
      39,    20,    21,    22,    23,    24
};

static const mpfittype_int8 mpfitcheck[] =
{
       3,     8,     5,     6,     7,    13,     4,    11,    12,    13,
      13,     9,    10,    11,    12,    13,    14,    20,    21,    22,
      23,    24,     0,     8,    27,    -1,     4,     5,     6,     7,
      -1,     9,    10,    -1,    37,    -1,    -1,    15,     4,     5,
       6,     7,    -1,     9,    10,    -1,     5,     6,     7,    15,
       9,    10,    -1,    -1,    -1,    -1,    15,     9,    10,    11,
      12,    13,    -1,    -1,    16,    17,     9,    10,    11,    12,
      13,    -1,    -1,    16,    17,     9,    10,    11,    12,    13,
      -1,    -1,    16,     9,    10,    11,    12,    13,    -1,    -1,
      16,     9,    10,    11,    12,    13
};

/* MPFITSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const mpfittype_int8 mpfitstos[] =
{
       0,     4,     5,     6,     7,     9,    10,    15,    19,    20,
      21,     7,    21,     8,    21,    21,    21,     0,    20,     4,
       9,    10,    11,    12,    13,    14,    16,    17,    21,    16,
      21,    21,    21,    21,    21,    21,    16,    17,    21,    16
};

/* MPFITR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const mpfittype_int8 mpfitr1[] =
{
       0,    18,    19,    19,    20,    20,    20,    20,    21,    21,
      21,    21,    21,    21,    21,    21,    21,    21,    21,    21,
      21
};

/* MPFITR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const mpfittype_int8 mpfitr2[] =
{
       0,     2,     1,     2,     2,     2,     1,     3,     1,     1,
       3,     3,     3,     3,     3,     2,     2,     3,     3,     5,
       7
};


enum { MPFITENOMEM = -2 };

#define mpfiterrok         (mpfiterrstatus = 0)
#define mpfitclearin       (mpfitchar = MPFITEMPTY)

#define MPFITACCEPT        goto mpfitacceptlab
#define MPFITABORT         goto mpfitabortlab
#define MPFITERROR         goto mpfiterrorlab
#define MPFITNOMEM         goto mpfitexhaustedlab


#define MPFITRECOVERING()  (!!mpfiterrstatus)

#define MPFITBACKUP(Token, Value)                                    \
  do                                                              \
    if (mpfitchar == MPFITEMPTY)                                        \
      {                                                           \
        mpfitchar = (Token);                                         \
        mpfitlval = (Value);                                         \
        MPFITPOPSTACK (mpfitlen);                                       \
        mpfitstate = *mpfitssp;                                         \
        goto mpfitbackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        mpfiterror (MPFIT_("syntax error: cannot back up")); \
        MPFITERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use MPFITerror or MPFITUNDEF. */
#define MPFITERRCODE MPFITUNDEF


/* Enable debugging if requested.  */
#if MPFITDEBUG

# ifndef MPFITFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define MPFITFPRINTF fprintf
# endif

# define MPFITDPRINTF(Args)                        \
do {                                            \
  if (mpfitdebug)                                  \
    MPFITFPRINTF Args;                             \
} while (0)




# define MPFIT_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (mpfitdebug)                                                            \
    {                                                                     \
      MPFITFPRINTF (stderr, "%s ", Title);                                   \
      mpfit_symbol_print (stderr,                                            \
                  Kind, Value); \
      MPFITFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on MPFITO.  |
`-----------------------------------*/

static void
mpfit_symbol_value_print (FILE *mpfito,
                       mpfitsymbol_kind_t mpfitkind, MPFITSTYPE const * const mpfitvaluep)
{
  FILE *mpfitoutput = mpfito;
  MPFIT_USE (mpfitoutput);
  if (!mpfitvaluep)
    return;
  MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  MPFIT_USE (mpfitkind);
  MPFIT_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on MPFITO.  |
`---------------------------*/

static void
mpfit_symbol_print (FILE *mpfito,
                 mpfitsymbol_kind_t mpfitkind, MPFITSTYPE const * const mpfitvaluep)
{
  MPFITFPRINTF (mpfito, "%s %s (",
             mpfitkind < MPFITNTOKENS ? "token" : "nterm", mpfitsymbol_name (mpfitkind));

  mpfit_symbol_value_print (mpfito, mpfitkind, mpfitvaluep);
  MPFITFPRINTF (mpfito, ")");
}

/*------------------------------------------------------------------.
| mpfit_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
mpfit_stack_print (mpfit_state_t *mpfitbottom, mpfit_state_t *mpfittop)
{
  MPFITFPRINTF (stderr, "Stack now");
  for (; mpfitbottom <= mpfittop; mpfitbottom++)
    {
      int mpfitbot = *mpfitbottom;
      MPFITFPRINTF (stderr, " %d", mpfitbot);
    }
  MPFITFPRINTF (stderr, "\n");
}

# define MPFIT_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (mpfitdebug)                                                  \
    mpfit_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the MPFITRULE is going to be reduced.  |
`------------------------------------------------*/

static void
mpfit_reduce_print (mpfit_state_t *mpfitssp, MPFITSTYPE *mpfitvsp,
                 int mpfitrule)
{
  int mpfitlno = mpfitrline[mpfitrule];
  int mpfitnrhs = mpfitr2[mpfitrule];
  int mpfiti;
  MPFITFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             mpfitrule - 1, mpfitlno);
  /* The symbols being reduced.  */
  for (mpfiti = 0; mpfiti < mpfitnrhs; mpfiti++)
    {
      MPFITFPRINTF (stderr, "   $%d = ", mpfiti + 1);
      mpfit_symbol_print (stderr,
                       MPFIT_ACCESSING_SYMBOL (+mpfitssp[mpfiti + 1 - mpfitnrhs]),
                       &mpfitvsp[(mpfiti + 1) - (mpfitnrhs)]);
      MPFITFPRINTF (stderr, "\n");
    }
}

# define MPFIT_REDUCE_PRINT(Rule)          \
do {                                    \
  if (mpfitdebug)                          \
    mpfit_reduce_print (mpfitssp, mpfitvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int mpfitdebug;
#else /* !MPFITDEBUG */
# define MPFITDPRINTF(Args) ((void) 0)
# define MPFIT_SYMBOL_PRINT(Title, Kind, Value, Location)
# define MPFIT_STACK_PRINT(Bottom, Top)
# define MPFIT_REDUCE_PRINT(Rule)
#endif /* !MPFITDEBUG */


/* MPFITINITDEPTH -- initial size of the parser's stacks.  */
#ifndef MPFITINITDEPTH
# define MPFITINITDEPTH 200
#endif

/* MPFITMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   MPFITSTACK_ALLOC_MAXIMUM < MPFITSTACK_BYTES (MPFITMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef MPFITMAXDEPTH
# define MPFITMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
mpfitdestruct (const char *mpfitmsg,
            mpfitsymbol_kind_t mpfitkind, MPFITSTYPE *mpfitvaluep)
{
  MPFIT_USE (mpfitvaluep);
  if (!mpfitmsg)
    mpfitmsg = "Deleting";
  MPFIT_SYMBOL_PRINT (mpfitmsg, mpfitkind, mpfitvaluep, mpfitlocationp);

  MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  MPFIT_USE (mpfitkind);
  MPFIT_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int mpfitchar;

/* The semantic value of the lookahead symbol.  */
MPFITSTYPE mpfitlval;
/* Number of syntax errors so far.  */
int mpfitnerrs;




/*----------.
| mpfitparse.  |
`----------*/

int
mpfitparse (void)
{
    mpfit_state_fast_t mpfitstate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int mpfiterrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow mpfitoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    MPFITPTRDIFF_T mpfitstacksize = MPFITINITDEPTH;

    /* The state stack: array, bottom, top.  */
    mpfit_state_t mpfitssa[MPFITINITDEPTH];
    mpfit_state_t *mpfitss = mpfitssa;
    mpfit_state_t *mpfitssp = mpfitss;

    /* The semantic value stack: array, bottom, top.  */
    MPFITSTYPE mpfitvsa[MPFITINITDEPTH];
    MPFITSTYPE *mpfitvs = mpfitvsa;
    MPFITSTYPE *mpfitvsp = mpfitvs;

  int mpfitn;
  /* The return value of mpfitparse.  */
  int mpfitresult;
  /* Lookahead symbol kind.  */
  mpfitsymbol_kind_t mpfittoken = MPFITSYMBOL_MPFITEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  MPFITSTYPE mpfitval;



#define MPFITPOPSTACK(N)   (mpfitvsp -= (N), mpfitssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int mpfitlen = 0;

  MPFITDPRINTF ((stderr, "Starting parse\n"));

  mpfitchar = MPFITEMPTY; /* Cause a token to be read.  */

  goto mpfitsetstate;


/*------------------------------------------------------------.
| mpfitnewstate -- push a new state, which is found in mpfitstate.  |
`------------------------------------------------------------*/
mpfitnewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  mpfitssp++;


/*--------------------------------------------------------------------.
| mpfitsetstate -- set current state (the top of the stack) to mpfitstate.  |
`--------------------------------------------------------------------*/
mpfitsetstate:
  MPFITDPRINTF ((stderr, "Entering state %d\n", mpfitstate));
  MPFIT_ASSERT (0 <= mpfitstate && mpfitstate < MPFITNSTATES);
  MPFIT_IGNORE_USELESS_CAST_BEGIN
  *mpfitssp = MPFIT_CAST (mpfit_state_t, mpfitstate);
  MPFIT_IGNORE_USELESS_CAST_END
  MPFIT_STACK_PRINT (mpfitss, mpfitssp);

  if (mpfitss + mpfitstacksize - 1 <= mpfitssp)
#if !defined mpfitoverflow && !defined MPFITSTACK_RELOCATE
    MPFITNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      MPFITPTRDIFF_T mpfitsize = mpfitssp - mpfitss + 1;

# if defined mpfitoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        mpfit_state_t *mpfitss1 = mpfitss;
        MPFITSTYPE *mpfitvs1 = mpfitvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if mpfitoverflow is a macro.  */
        mpfitoverflow (MPFIT_("memory exhausted"),
                    &mpfitss1, mpfitsize * MPFITSIZEOF (*mpfitssp),
                    &mpfitvs1, mpfitsize * MPFITSIZEOF (*mpfitvsp),
                    &mpfitstacksize);
        mpfitss = mpfitss1;
        mpfitvs = mpfitvs1;
      }
# else /* defined MPFITSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (MPFITMAXDEPTH <= mpfitstacksize)
        MPFITNOMEM;
      mpfitstacksize *= 2;
      if (MPFITMAXDEPTH < mpfitstacksize)
        mpfitstacksize = MPFITMAXDEPTH;

      {
        mpfit_state_t *mpfitss1 = mpfitss;
        union mpfitalloc *mpfitptr =
          MPFIT_CAST (union mpfitalloc *,
                   MPFITSTACK_ALLOC (MPFIT_CAST (MPFITSIZE_T, MPFITSTACK_BYTES (mpfitstacksize))));
        if (! mpfitptr)
          MPFITNOMEM;
        MPFITSTACK_RELOCATE (mpfitss_alloc, mpfitss);
        MPFITSTACK_RELOCATE (mpfitvs_alloc, mpfitvs);
#  undef MPFITSTACK_RELOCATE
        if (mpfitss1 != mpfitssa)
          MPFITSTACK_FREE (mpfitss1);
      }
# endif

      mpfitssp = mpfitss + mpfitsize - 1;
      mpfitvsp = mpfitvs + mpfitsize - 1;

      MPFIT_IGNORE_USELESS_CAST_BEGIN
      MPFITDPRINTF ((stderr, "Stack size increased to %ld\n",
                  MPFIT_CAST (long, mpfitstacksize)));
      MPFIT_IGNORE_USELESS_CAST_END

      if (mpfitss + mpfitstacksize - 1 <= mpfitssp)
        MPFITABORT;
    }
#endif /* !defined mpfitoverflow && !defined MPFITSTACK_RELOCATE */


  if (mpfitstate == MPFITFINAL)
    MPFITACCEPT;

  goto mpfitbackup;


/*-----------.
| mpfitbackup.  |
`-----------*/
mpfitbackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  mpfitn = mpfitpact[mpfitstate];
  if (mpfitpact_value_is_default (mpfitn))
    goto mpfitdefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* MPFITCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (mpfitchar == MPFITEMPTY)
    {
      MPFITDPRINTF ((stderr, "Reading a token\n"));
      mpfitchar = mpfitlex ();
    }

  if (mpfitchar <= MPFITEOF)
    {
      mpfitchar = MPFITEOF;
      mpfittoken = MPFITSYMBOL_MPFITEOF;
      MPFITDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (mpfitchar == MPFITerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      mpfitchar = MPFITUNDEF;
      mpfittoken = MPFITSYMBOL_MPFITerror;
      goto mpfiterrlab1;
    }
  else
    {
      mpfittoken = MPFITTRANSLATE (mpfitchar);
      MPFIT_SYMBOL_PRINT ("Next token is", mpfittoken, &mpfitlval, &mpfitlloc);
    }

  /* If the proper action on seeing token MPFITTOKEN is to reduce or to
     detect an error, take that action.  */
  mpfitn += mpfittoken;
  if (mpfitn < 0 || MPFITLAST < mpfitn || mpfitcheck[mpfitn] != mpfittoken)
    goto mpfitdefault;
  mpfitn = mpfittable[mpfitn];
  if (mpfitn <= 0)
    {
      if (mpfittable_value_is_error (mpfitn))
        goto mpfiterrlab;
      mpfitn = -mpfitn;
      goto mpfitreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (mpfiterrstatus)
    mpfiterrstatus--;

  /* Shift the lookahead token.  */
  MPFIT_SYMBOL_PRINT ("Shifting", mpfittoken, &mpfitlval, &mpfitlloc);
  mpfitstate = mpfitn;
  MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++mpfitvsp = mpfitlval;
  MPFIT_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  mpfitchar = MPFITEMPTY;
  goto mpfitnewstate;


/*-----------------------------------------------------------.
| mpfitdefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
mpfitdefault:
  mpfitn = mpfitdefact[mpfitstate];
  if (mpfitn == 0)
    goto mpfiterrlab;
  goto mpfitreduce;


/*-----------------------------.
| mpfitreduce -- do a reduction.  |
`-----------------------------*/
mpfitreduce:
  /* mpfitn is the number of a rule to reduce with.  */
  mpfitlen = mpfitr2[mpfitn];

  /* If MPFITLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets MPFITVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to MPFITVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that MPFITVAL may be used uninitialized.  */
  mpfitval = mpfitvsp[1-mpfitlen];


  MPFIT_REDUCE_PRINT (mpfitn);
  switch (mpfitn)
    {
  case 4: /* stmt: expr ';'  */
#line 32 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { mpfitAST = (mpfitvsp[-1].astnode); }
#line 1110 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 5: /* stmt: expr ENTER  */
#line 33 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { std::cout << (mpfitvsp[-1].astnode)->evaluate() << std::endl; delete (mpfitvsp[-1].astnode); }
#line 1116 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 6: /* stmt: ENTER  */
#line 34 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { std::cout << std::endl; }
#line 1122 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 7: /* stmt: VAR '=' expr  */
#line 35 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                                    { mpfitassignmentNode *a = new mpfitassignmentNode(*(mpfitvsp[-2].s), (mpfitvsp[0].astnode)); a->evaluate(); delete (mpfitvsp[-2].s); delete a; }
#line 1128 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 8: /* expr: REAL  */
#line 38 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { (mpfitval.astnode) = new mpfitnumberNode((mpfitvsp[0].i)); }
#line 1134 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 9: /* expr: VAR  */
#line 39 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { (mpfitval.astnode) = new mpfitvariableNode(*(mpfitvsp[0].s));      delete (mpfitvsp[0].s); }
#line 1140 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 10: /* expr: expr '+' expr  */
#line 40 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { (mpfitval.astnode) = new mpfitplusNode((mpfitvsp[-2].astnode), (mpfitvsp[0].astnode)); }
#line 1146 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 11: /* expr: expr '-' expr  */
#line 41 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { (mpfitval.astnode) = new mpfitminusNode((mpfitvsp[-2].astnode), (mpfitvsp[0].astnode)); }
#line 1152 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 12: /* expr: expr '*' expr  */
#line 42 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { (mpfitval.astnode) = new mpfitmultiplyNode((mpfitvsp[-2].astnode), (mpfitvsp[0].astnode)); }
#line 1158 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 13: /* expr: expr '/' expr  */
#line 43 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { (mpfitval.astnode) = new mpfitdivideNode((mpfitvsp[-2].astnode), (mpfitvsp[0].astnode)); }
#line 1164 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 14: /* expr: expr '^' expr  */
#line 44 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                        { (mpfitval.astnode) = new mpfitpowerNode((mpfitvsp[-2].astnode), (mpfitvsp[0].astnode)); }
#line 1170 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 15: /* expr: '+' expr  */
#line 45 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                                { (mpfitval.astnode) =  (mpfitvsp[0].astnode); }
#line 1176 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 16: /* expr: '-' expr  */
#line 46 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                                { (mpfitval.astnode) = new mpfitunaryMinusNode((mpfitvsp[0].astnode)); }
#line 1182 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 17: /* expr: '(' expr ')'  */
#line 47 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                                { (mpfitval.astnode) =  (mpfitvsp[-1].astnode); }
#line 1188 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 18: /* expr: FUNC expr ')'  */
#line 48 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                                    { (mpfitval.astnode) = new mpfitfunctionNode(*(mpfitvsp[-2].s), (mpfitvsp[-1].astnode)); delete (mpfitvsp[-2].s); }
#line 1194 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 19: /* expr: FUNC expr ',' expr ')'  */
#line 49 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                                             { (mpfitval.astnode) = new mpfitfunctionNode2(*(mpfitvsp[-4].s), (mpfitvsp[-3].astnode), (mpfitvsp[-1].astnode)); delete (mpfitvsp[-4].s); }
#line 1200 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;

  case 20: /* expr: FUNC expr ',' expr ',' expr ')'  */
#line 50 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
                                                      { (mpfitval.astnode) = new mpfitfunctionNode3(*(mpfitvsp[-6].s), (mpfitvsp[-5].astnode), (mpfitvsp[-3].astnode), (mpfitvsp[-1].astnode)); delete (mpfitvsp[-6].s); }
#line 1206 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"
    break;


#line 1210 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter mpfitchar, and that requires
     that mpfittoken be updated with the new translation.  We take the
     approach of translating immediately before every use of mpfittoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     MPFITABORT, MPFITACCEPT, or MPFITERROR immediately after altering mpfitchar or
     if it invokes MPFITBACKUP.  In the case of MPFITABORT or MPFITACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of MPFITERROR or MPFITBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  MPFIT_SYMBOL_PRINT ("-> $$ =", MPFIT_CAST (mpfitsymbol_kind_t, mpfitr1[mpfitn]), &mpfitval, &mpfitloc);

  MPFITPOPSTACK (mpfitlen);
  mpfitlen = 0;

  *++mpfitvsp = mpfitval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int mpfitlhs = mpfitr1[mpfitn] - MPFITNTOKENS;
    const int mpfiti = mpfitpgoto[mpfitlhs] + *mpfitssp;
    mpfitstate = (0 <= mpfiti && mpfiti <= MPFITLAST && mpfitcheck[mpfiti] == *mpfitssp
               ? mpfittable[mpfiti]
               : mpfitdefgoto[mpfitlhs]);
  }

  goto mpfitnewstate;


/*--------------------------------------.
| mpfiterrlab -- here on detecting error.  |
`--------------------------------------*/
mpfiterrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  mpfittoken = mpfitchar == MPFITEMPTY ? MPFITSYMBOL_MPFITEMPTY : MPFITTRANSLATE (mpfitchar);
  /* If not already recovering from an error, report this error.  */
  if (!mpfiterrstatus)
    {
      ++mpfitnerrs;
      mpfiterror (MPFIT_("syntax error"));
    }

  if (mpfiterrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (mpfitchar <= MPFITEOF)
        {
          /* Return failure if at end of input.  */
          if (mpfitchar == MPFITEOF)
            MPFITABORT;
        }
      else
        {
          mpfitdestruct ("Error: discarding",
                      mpfittoken, &mpfitlval);
          mpfitchar = MPFITEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto mpfiterrlab1;


/*---------------------------------------------------.
| mpfiterrorlab -- error raised explicitly by MPFITERROR.  |
`---------------------------------------------------*/
mpfiterrorlab:
  /* Pacify compilers when the user code never invokes MPFITERROR and the
     label mpfiterrorlab therefore never appears in user code.  */
  if (0)
    MPFITERROR;
  ++mpfitnerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this MPFITERROR.  */
  MPFITPOPSTACK (mpfitlen);
  mpfitlen = 0;
  MPFIT_STACK_PRINT (mpfitss, mpfitssp);
  mpfitstate = *mpfitssp;
  goto mpfiterrlab1;


/*-------------------------------------------------------------.
| mpfiterrlab1 -- common code for both syntax error and MPFITERROR.  |
`-------------------------------------------------------------*/
mpfiterrlab1:
  mpfiterrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      mpfitn = mpfitpact[mpfitstate];
      if (!mpfitpact_value_is_default (mpfitn))
        {
          mpfitn += MPFITSYMBOL_MPFITerror;
          if (0 <= mpfitn && mpfitn <= MPFITLAST && mpfitcheck[mpfitn] == MPFITSYMBOL_MPFITerror)
            {
              mpfitn = mpfittable[mpfitn];
              if (0 < mpfitn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (mpfitssp == mpfitss)
        MPFITABORT;


      mpfitdestruct ("Error: popping",
                  MPFIT_ACCESSING_SYMBOL (mpfitstate), mpfitvsp);
      MPFITPOPSTACK (1);
      mpfitstate = *mpfitssp;
      MPFIT_STACK_PRINT (mpfitss, mpfitssp);
    }

  MPFIT_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++mpfitvsp = mpfitlval;
  MPFIT_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  MPFIT_SYMBOL_PRINT ("Shifting", MPFIT_ACCESSING_SYMBOL (mpfitn), mpfitvsp, mpfitlsp);

  mpfitstate = mpfitn;
  goto mpfitnewstate;


/*-------------------------------------.
| mpfitacceptlab -- MPFITACCEPT comes here.  |
`-------------------------------------*/
mpfitacceptlab:
  mpfitresult = 0;
  goto mpfitreturnlab;


/*-----------------------------------.
| mpfitabortlab -- MPFITABORT comes here.  |
`-----------------------------------*/
mpfitabortlab:
  mpfitresult = 1;
  goto mpfitreturnlab;


/*-----------------------------------------------------------.
| mpfitexhaustedlab -- MPFITNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
mpfitexhaustedlab:
  mpfiterror (MPFIT_("memory exhausted"));
  mpfitresult = 2;
  goto mpfitreturnlab;


/*----------------------------------------------------------.
| mpfitreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
mpfitreturnlab:
  if (mpfitchar != MPFITEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      mpfittoken = MPFITTRANSLATE (mpfitchar);
      mpfitdestruct ("Cleanup: discarding lookahead",
                  mpfittoken, &mpfitlval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this MPFITABORT or MPFITACCEPT.  */
  MPFITPOPSTACK (mpfitlen);
  MPFIT_STACK_PRINT (mpfitss, mpfitssp);
  while (mpfitssp != mpfitss)
    {
      mpfitdestruct ("Cleanup: popping",
                  MPFIT_ACCESSING_SYMBOL (+*mpfitssp), mpfitvsp);
      MPFITPOPSTACK (1);
    }
#ifndef mpfitoverflow
  if (mpfitss != mpfitssa)
    MPFITSTACK_FREE (mpfitss);
#endif

  return mpfitresult;
}

#line 53 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"

