/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with MPFIT_ or mpfit_.  They are
   private implementation details that can be changed or removed.  */

#ifndef MPFIT_MPFIT_MNT_C_OTTL_PROJECT_QFITSVIEW_BUILD_LINUX_MPFITPARSER_H_INCLUDED
# define MPFIT_MPFIT_MNT_C_OTTL_PROJECT_QFITSVIEW_BUILD_LINUX_MPFITPARSER_H_INCLUDED
/* Debug traces.  */
#ifndef MPFITDEBUG
# define MPFITDEBUG 0
#endif
#if MPFITDEBUG
extern int mpfitdebug;
#endif

/* Token kinds.  */
#ifndef MPFITTOKENTYPE
# define MPFITTOKENTYPE
  enum mpfittokentype
  {
    MPFITEMPTY = -2,
    MPFITEOF = 0,                     /* "end of file"  */
    MPFITerror = 256,                 /* error  */
    MPFITUNDEF = 257,                 /* "invalid token"  */
    BATATA = 258,                  /* BATATA  */
    ENTER = 259,                   /* ENTER  */
    REAL = 260,                    /* REAL  */
    FUNC = 261,                    /* FUNC  */
    VAR = 262                      /* VAR  */
  };
  typedef enum mpfittokentype mpfittoken_kind_t;
#endif

/* Value type.  */
#if ! defined MPFITSTYPE && ! defined MPFITSTYPE_IS_DECLARED
union MPFITSTYPE
{
#line 11 "/mnt/c/ottl/project/dpuser/mpfit/mpfit.y"
 double i; std::string *s; int f; mpfitASTNode *astnode; 

#line 74 "/mnt/c/ottl/project/QFitsView/build-linux/mpfitParser.h"

};
typedef union MPFITSTYPE MPFITSTYPE;
# define MPFITSTYPE_IS_TRIVIAL 1
# define MPFITSTYPE_IS_DECLARED 1
#endif


extern MPFITSTYPE mpfitlval;


int mpfitparse (void);


#endif /* !MPFIT_MPFIT_MNT_C_OTTL_PROJECT_QFITSVIEW_BUILD_LINUX_MPFITPARSER_H_INCLUDED  */
