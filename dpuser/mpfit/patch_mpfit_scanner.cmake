message(STATUS "Patching mpfitScanner.cpp")
file(READ "${CMAKE_CURRENT_SOURCE_DIR}/mpfitScanner.cpp" file_contents)
string(REPLACE "yy" "mpfit" file_contents "${file_contents}")
string(REPLACE "YY" "MPFIT" file_contents "${file_contents}")
file(WRITE "${CMAKE_CURRENT_SOURCE_DIR}/mpfitScanner.cpp" "${file_contents}")
