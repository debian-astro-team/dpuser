#ifndef DPUSER_FUNCS_H
#define DPUSER_FUNCS_H

#include <vector>

#include <dpstring.h>
#include <dpstringlist.h>
#include "dpuserType.h"
#include "dpuserAST.h"

typedef struct _function_declarations {
    const char *name;
	int minargs, maxargs;
	long args[20];
	int noptions;
    const char *options[20];
} function_declarations;

extern function_declarations funcss[];

class FunctionDeclaration {
public:
    FunctionDeclaration(const char *n, int mi, int ma, long a[20], int no, const char *o[]);
	dpString name;
	int minargs, maxargs;
	long args[20];
	int noptions;
    std::map<std::string, int> options;
};

int stringFunc(char *name);
int stringarrFunc(char *name);

//dpuserType resolveFunction(int id, std::vector<ASTNode *>args, std::vector<std::string>options) {

void init_functions();

#endif /* DPUSER_FUNCS_H */
