procedure moonpos, jd, ra, dec, dis, geolong, geolat {
 npts = nelements(jd)
 dtor = pi()/180
// form time in Julian centuries from 1900.0

 T = (jd - 2451545.0)/36525.0

 dlng = [0,2,2,0,0,0,2,2,2,2,0,1,0,2,0,0,4,0,4,2,2,1,1,2,2,4,2,0,2,2,1,2,0,0,2,2,2,4,0,3,2,4,0,2,2,2,4,0,4,1,2,0,1,3,4,2,0,1,2,2]

 mlng = [0,0,0,0,1,0,0,-1,0,-1,1,0,1,0,0,0,0,0,0,1,1,0,1,-1,0,0,0,1,0,-1,0,-2,1,2,-2,0,0,-1,0,0,1,-1,2,2,1,-1,0,0,-1,0,1,0,1,0,0,-1,2,1,0,0]

 mplng = [1,-1,0,2,0,0,-2,-1,1,0,-1,0,1,0,1,1,-1,3,-2,-1,0,-1,0,1,2,0,-3,-2,-1,-2,1,0,2,0,-1,1,0,-1,2,-1,1,-2,-1,-1,-2,0,1,4,0,-2,0,2,1,-2,-3,2,1,-1,3,-1]

 flng = [0,0,0,0,0,2,0,0,0,0,0,0,0,-2,2,-2,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,-2,2,0,2,0,0,0,0,0,0,-2,0,0,0,0,-2,-2,0,0,0,0,0,0,0,-2]

 sinlng = [6288774,1274027,658314,213618,-185116,-114332,58793,57066,53322,45758,-40923,-34720,-30383,15327,-12528,10980,10675,10034,8548,-7888,-6766,-5163,4987,4036,3994,3861,3665,-2689,-2602,2390,-2348,2236,-2120,-2069,2048,-1773,-1595,1215,-1110,-892,-810,759,-713,-700,691,596,549,537,520,-487,-399,-381,351,-340,330,327,-323,299,294,0.0]

 coslng = [-20905355,-3699111,-2955968,-569925,48888,-3149,246158,-152138,-170733,-204586,-129620,108743,104755,10321,0,79661,-34782,-23210,-21636,24208,30824,-8379,-16675,-12831,-10445,-11650,14403,-7003,0,10056,6322,-9884,5751,0,-4950,4130,0,-3958,0,3258,2616,-1897,-2117,2354,0,0,-1423,-1117,-1571,-1739,0,-4421,0,0,0,0,1165,0,0,8752.0]

 dlat = [0,0,0,2,2,2,2,0,2,0,2,2,2,2,2,2,2,0,4,0,0,0,1,0,0,0,1,0,4,4,0,4,2,2,2,2,0,2,2,2,2,4,2,2,0,2,1,1,0,2,1,2,0,4,4,1,4,1,4,2]

 mlat = [0,0,0,0,0,0,0,0,0,0,-1,0,0,1,-1,-1,-1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0,0,0,-1,0,0,0,0,1,1,0,-1,-2,0,1,1,1,1,1,0,-1,1,0,-1,0,0,0,-1,-2]

 mplat = [0,1,1,0,-1,-1,0,2,1,2,0,-2,1,0,-1,0,-1,-1,-1,0,0,-1,0,1,1,0,0,3,0,-1,1, -2,0,2,1,-2,3,2,-3,-1,0,0,1,0,1,1,0,0,-2,-1,1,-2,2,-2,-1,1,1,-1,0,0]

 flat =[ 1,1,-1,-1,1,-1,1,1,-1,-1,-1,-1,1,-1,1,1,-1,-1,-1,1,3,1,1,1,-1,-1,-1,1,-1,1,-3,1,-3,-1,-1,1,-1,1,-1,1,1,1,1,-1,3,-1,-1,1,-1,-1,1,-1,1,-1,-1,-1,-1,-1,-1,1]

 sinlat = [5128122,280602,277693,173237,55413,46271,32573,17198,9266,8822,8216,4324,4200,-3359,2463,2211,2065,-1870,1828,-1794,-1749,-1565,-1491,-1475,-1410,-1344,-1335,1107,1021,833,777,671,607,596,491,-451,439,422,421,-366,-351,331,315,302,-283,-229,223,223,-220,-220,-185,181,-177,176,166,-164,132,-119,115,107.0]

// Mean longitude of the moon refered to mean equinox of the date

 coeff0 = [218.3164477, 481267.88123421, -0.0015786, 1.0/538841.0,-1.0/6.5194d7 ]
 lprimed = poly(T, coeff0)
 cirrange lprimed
 lprime = lprimed*dtor

// Mean elongation of the Moon

  coeff1 = [297.8501921, 445267.1114034, -0.0018819, 1.0/545868.0, -1.0/1.13065d8 ]
  d = poly(T, coeff1)

  cirrange d

  d = d*dtor
// Sun's mean anomaly

   coeff2 = [357.5291092, 35999.0502909, -0.0001536, 1.0/2.449d7 ]
   M = poly(T,coeff2) 
   cirrange M 
   M = M*dtor

// Moon's mean anomaly

   coeff3 = [134.9633964, 477198.8675055, 0.0087414, 1.0/6.9699d4, -1.0/1.4712d7 ]
   Mprime = poly(T, coeff3) 
   cirrange Mprime
   Mprime = Mprime*dtor

// Moon's argument of latitude

    coeff4 = [93.2720950, 483202.0175233, -0.0036539, -1.0/3.526d7, 1.0/8.6331d8 ]
    F = poly(T, coeff4 ) 
    cirrange F
    F = F*dtor

// Eccentricity of Earth's orbit around the Sun

    E = 1 - 0.002516*T - 7.4d-6*T^2
    E2 = E^2

    ecorr1 = where(abs(mlng) == 1)
    ecorr2 = where(abs(mlat) == 1)
    ecorr3 = where(abs(mlng) == 2)
    ecorr4 = where(abs(mlat) == 2)

// Additional arguments

    A1 = (119.75 + 131.849*T) * dtor
    A2 = (53.09 + 479264.290*T) * dtor
    A3 = (313.45 + 481266.484*T) * dtor
    sumladd = 3958*sin(A1) + 1962*sin(lprime - F) + 318*sin(A2)
    sumbadd =  -2235*sin(lprime) + 382*sin(A3) + 175*sin(A1-F) + 175*sin(A1 + F) + 127*sin(lprime - Mprime) - 115*sin(lprime + Mprime)

// Sum the periodic terms 
 geolong = geolat = dis = doublearray(npts)

   for i=1, npts {
   sinlng1 = sinlng
   coslng1 = coslng
   sinlat1 = sinlat

   for j=1, nelements(ecorr1) {
   sinlng1[j] = E[i]*sinlng1[j]
   coslng1[j] = E[i]*coslng1[j]
   }
   for j=1, nelements(ecorr2) {
   sinlat1[j] = E[i]*sinlat1[j]
   }
   for j=1, nelements(ecorr3) {
   sinlng1[j] = E2[i]*sinlng1[j]
   coslng1[j] = E2[i]*coslng1[j]
   }
   for j=1, nelements(ecorr4) {
   sinlat1[j] = E2[i]*sinlat1[j]
   }
   arg = dlng*d[i] + mlng*M[i] +mplng*Mprime[i] + flng*F[i]

   geolong[i] = lprimed[i] + ( total(sinlng1*sin(arg)) + sumladd[i] )/1.0d6

   dis[i] = 385000.56 + total(coslng1*cos(arg))/1.0d3

   arg = dlat*d[i] + mlat*M[i] +mplat*Mprime[i] + flat*F[i]
   geolat[i] = (total(sinlat1*sin(arg)) + sumbadd[i])/1.0d6    
   }
 if (npts == 1) {
  geolong = geolong[1]
  geolat = geolat[1]
  dis = dis[1]
 }
 nlong = elong = 0
 nutate jd, nlong, elong 
 geolong= geolong + nlong/3.6d3
 cirrange geolong
 lambda = geolong*dtor
 beta = geolat*dtor
// Find mean obliquity and convert lambda,beta to RA, Dec 
 cc = [21.448,-4680.93,-1.55,1999.25,-51.38,-249.67,-39.05,7.12,27.87,5.79,2.45]
 tt = T / 1d2
epsilon = ten(23,26,0) + poly(tt,cc)/3600.
 eps = (epsilon + elong/3600. )*dtor          //True obliquity in radians
 ra = atan( sin(lambda)*cos(eps) - tan(beta)* sin(eps), cos(lambda) )

 cirrange ra,/rad
 dec = asin( sin(beta)*cos(eps) + cos(beta)*sin(eps)*sin(lambda) )

 geolong = lambda
 geolat = beta
}
