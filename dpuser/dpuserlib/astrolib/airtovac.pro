procedure airtovac,wave {                  
/*;+
; NAME:
;       AIRTOVAC
; PURPOSE:
;       Convert air wavelengths to vacuum wavelengths 
; EXPLANATION:
;       Wavelengths are corrected for the index of refraction of air under 
;       standard conditions.  Wavelength values below 2000 A will not be 
;       altered.  Uses the IAU standard for conversion given in Morton 
;       (1991 Ap.J. Suppl. 77, 119)
;
; CALLING SEQUENCE:
;       AIRTOVAC, WAVE
;
; INPUT/OUTPUT:
;       WAVE - Wavelength in Angstroms, scalar or vector
;               WAVE should be input as air wavelength(s), it will be
;               returned as vacuum wavelength(s).  WAVE is always converted to
;               double precision upon return.
;
; EXAMPLE:
;       If the air wavelength is  W = 6056.125 (a Krypton line), then 
;       AIRTOVAC, W yields an vacuum wavelength of W = 6057.8019
;
; METHOD:
;       See Morton (Ap. J. Suppl. 77, 119) for the formula used
;
; REVISION HISTORY
;       Written W. Landsman                November 1991
;       Converted to IDL V5.0   W. Landsman   September 1997
;-*/

  if (nparams == 0) {
      print "Syntax - AIRTOVAC, WAVE"
      print "WAVE (Input) is the air wavelength in Angstroms"
      print "On output WAVE contains the vacuum wavelength in Angstroms"
      break
  }
  
  if (wave >= 2000) {

  sigma2 = (1e4/double(wave) )^2.              //Convert to wavenumber squared

// Compute conversion factor

  fact = 1. + 6.4328e-5 + 2.94981e-2/(146.e0 - sigma2) + 2.5540e-4/( 41.e0 - sigma2)
    
  wave = wave*fact              //Convert Wavelength
  }
}
