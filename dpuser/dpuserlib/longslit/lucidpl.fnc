// dead pixel removal
// image : data to remove dead pixels off
// dark  : dark frame to detect bad pixels from
function lucidpl, image, dark {
    h = dpixcreate(dark, 50, 1, 1)
    lucidpl = dpixapply(image, h)

    print "FINISHED REMOVING DEAD PIXELS"
}
