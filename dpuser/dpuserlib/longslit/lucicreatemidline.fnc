// image:        image to analize
// midgausssize: size of sub-longslit to median
// orientation:  0: horizontal (should be default)
//               1: vertical
function lucicreatemidline, image, midgausssize, orientation  {
    imagewidth = naxis1(image)
    imageheight = naxis2(image)

    length = 0
    if (orientation == 0) {
        length = imagewidth
    } else {
        length = imageheight
    }
    
    lucicreatemidline = [1:length]

    // find max positions on sub-longslit in the middle of the image
    // extract sub-longslit and median it
    for i=1,length {
        if (orientation == 0) {
            lucicreatemidline[i] = median(image[i, imageheight/2-midgausssize/2:imageheight/2+midgausssize/2])
        } else {
             lucicreatemidline[i] = median(image[imagewidth/2-midgausssize/2:imagewidth/2+midgausssize/2, i])
        }

    }
    setbitpix lucicreatemidline, -32
}
