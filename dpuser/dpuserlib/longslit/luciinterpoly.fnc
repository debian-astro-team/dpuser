// image:         image to interpolate in spatial direction
// calimage:      spatial calibration image
// yinterpolpars: [1]: new height of interpolated longslit
//                [2]: 0 (default), increase this value to 3 or 5 in order to see the border of the longslit
//                     although this is not recommended since fitting the lines will fail in this region
function luciinterpoly, image, calimage, yinterpolpars {
    newheight = int(yinterpolpars[1])
    border = int(yinterpolpars[2])

    imagewidth = naxis1(calimage)
    imageheight = naxis2(calimage)

    yline = [1:newheight + 2*border+1]
    luciinterpoly = fits(imagewidth, nelements(yline))

    for x = 1,imagewidth {
        newspec = interpol(image[x,*], calimage[x,*], yline-border-1)
        luciinterpoly[x,*] = newspec
    }

    print "FINISHED INTERPOLATING IN Y"
}
