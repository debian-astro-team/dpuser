// image:         image to interpolate in wavelength direction
// calimage:      spectral calibration image
// xinterpolpars: [1]:  new dispersion
//                [2]:  new start wavelength
//                [3]:  new width of interpolated longslit
function luciinterpolx, image, calimage, xinterpolpars {
    disp = xinterpolpars[1]
    startlambda = xinterpolpars[2]
    newwidth = int(xinterpolpars[3])

    if (newwidth <= naxis1(calimage)) {
        print "WARNING: newwidth is smaller than width of original image!"
    }
    imagewidth = newwidth
    imageheight = naxis2(calimage)
    luciinterpolx = fits(imagewidth, imageheight)

    cdelt1 = disp
    crval1 = startlambda
    crpix1 = 1
    crval2 = 1
    cdelt2 = 1
    crpix2 = 1

    xnew = [1:imagewidth] * cdelt1 + crval1

    for y = 1,imageheight {
        xs = int((calimage[1,y] - crval1) / cdelt1)
        xe = int((calimage[naxis1(calimage),y] - crval1) / cdelt1)

        newspec = interpol(image[*,y], calimage[*,y], xnew)

        if (xs >= 1) {
            newspec[1:xs] = 0
        }

        if (xe <= imagewidth) {
            newspec[xe:imagewidth] = 0
        }

        luciinterpolx[*,y] = newspec
    }

    copyheader luciinterpolx, calimage

    // calculate wcs
    setwcs luciinterpolx, crpix1, crpix2, crval1, crval2, cdelt1, cdelt2

    print "FINISHED INTERPOLATING IN X"
}
