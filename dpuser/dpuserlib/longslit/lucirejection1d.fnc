// data:  vector to apply rejection to
// sigma: sigma-factor for rejection
// iter:  number of iterations
// window: size of moving window over data, will be median'd (must be greater than 0)
function lucirejection1d, data, sigma, iter, window {
    size = naxis1(data)
    xdata = [1:size]
    window = 50

    d=data
    x=xdata

    for iteri=1,iter {
        tmp = where(d > 0)
        d2=d[tmp]
        x2=x[tmp]
        size=naxis1(d2)

        n=int(size/window)
        t=fits(size)

        for i=1,n {
            subdata = d2[(i-1)*window+1:i*window]

            t[(i-1)*window+1:i*window] = median(subdata) + sigma*meddev(subdata)
        }
        if (size > n*window+1) {
            subdata = d2[n*window+1:size]
            t[n*window+1:size] = median(subdata) + sigma*meddev(subdata)
        }

        for i=1,size {
            if (d2[i]>t[i]) d2[i]=-1/0.
        }

        which =where(d2<0)
        pos=x2[which]
        d[pos] = -1/0.
    }
    lucirejection1d = d
}
