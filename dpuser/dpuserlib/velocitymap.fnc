function cubesum, cube {
  cubesum = cubeavg(cube) * naxis3(cube)
}

function velocitymap, cube, center, width {
  result = fits(naxis1(cube), naxis2(cube), 3)
 
// flux
  result[*,*,1] = cubesum(cube)

// centroid and width
/*  for (x = 1; x <= naxis1(result); x++) {
  print x
    for (y = 1; y <= naxis2(result); y++) {
      result[x, y, 2] = xcen(cube[x, y, center-width:center+width])
    }
  }*/
  velocitymap = result
}
