/* Gravitational constant */
function G {
  G = 6.673e-11
}

/* Mass of the sun in kg */
function Msun {
  Msun = 1.989e30
}

/* 1 Parsec in meters */

function Pc {
  Pc = 3.086e16
}

/* Arcsecs to meters for galactic center, assuming a distance of 8.5kPc */
function GCr, arcsecs {
  GCr = 8000 * Pc() * tan(arcsecs / 3600, /deg)
}
