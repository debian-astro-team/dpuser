function ten,dd,mm,ss {
/*
;+
; NAME:
;	TEN()
; PURPOSE:
;	Converts a sexigesimal number to decimal.
; EXPLANATION:
;	Inverse of the SIXTY() function.
;
; CALLING SEQUENCES:
;	X = TEN( [ HOUR_OR_DEG, MIN, SEC ] )
;	X = TEN( HOUR_OR_DEG, MIN, SEC )
;	X = TEN( [ HOUR_OR_DEG, MIN ] )
;	X = TEN( HOUR_OR_DEG, MIN )
;	X = TEN( [ HOUR_OR_DEG ] )      <--  Trivial cases
;	X = TEN( HOUR_OR_DEG )        <--
;
; INPUTS:
;	HOUR_OR_DEG,MIN,SEC -- Scalars giving sexigesimal quantity in 
;		in order from largest to smallest.    
;
; OUTPUTS:
;	Function value returned = double real scalar, decimal equivalent of
;	input sexigesimal quantity.  A minus sign on any element
;	of the input vector causes all the elements to be taken as
;	< 0.
;
; PROCEDURE:
;	Mostly involves checking arguments and setting the sign.
;
;	The procedure TENV can be used when dealing with a vector of 
;	sexigesimal quantities.
;
; MODIFICATION HISTORY:
;	Written by R. S. Hill, STX, 21 April 87       
;	Modified to allow non-vector arguments.  RSH, STX, 19-OCT-87
;	Converted to IDL V5.0   W. Landsman   September 1997
;-
*/
         vector=doublearray(3)
         vector[1]=dd
         vector[2]=mm
      vector[3]=ss
      facs=[1,60,3600]
//      nel = nelements(vector)
      sign = 1
			for i=1,3 if (vector[i] < 0) sign = -1
      vector = abs(vector)
      decim = 0
      for i=1, 3 decim += vector[i] / facs[i]
			
			ten = decim*sign
}
