function mandelbrot, x1, x2, y1, y2, iter {
  mandelbrot = bytearray(256,256)
  eps = abs(x2 - x1) / 256
  x0 = 1
  for (x=x1; x < x2; x+=eps) {
    print x0
    y0 = 1
    if (x0 < 257) {
      for (y = y1; y < y2; y += eps) {
        if (y0 < 257) {
          i = 0
          Z = complex(0,0)
          C = complex(x,y)
          while (abs(Z) < 2 && i < iter) {
            Z = Z*Z + C
            i++
          }
          mandelbrot[x0,y0] = i
          y0++
        }
      }
      x0++
    }
  }
  setwcs mandelbrot, 1, 1, x1, y1, eps
}

      
