/* This function returns the reverse indices of a histogram */
/* !!!!!WARNING: The indices are returned in C-Notation!!!! */
/* So the result is exactly what IDL's histogram REVERSE_INDICES returns */

function histrev, array {
  hist = histogram(array)
  
  histrev = longarray(nelements(hist) + nelements(array) + 1)
  
  for i = 1, nelements(hist) {
    if (i == 1) {
       histrev[i] = nelements(hist) + 1
    } else {
       histrev[i] = histrev[i-1] + hist[i-1]
    }
  }
  histrev[nelements(hist) + 1] = nelements(histrev)
  
  pos = nelements(hist) + 2
  
  min = min(array)
  for histpos = 1, nelements(hist) {
    for i=1, nelements(array) {
      if (array[i] >= min && array[i] < min+1) {
        histrev[pos] = i - 1
        pos++
      }
    }
    min++
  }
}
