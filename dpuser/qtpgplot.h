#ifndef QTPGPLOT_H
#define QTPGPLOT_H

#include <qwidget.h>
#include <qpixmap.h>
#include <qpainter.h>
#include <qthread.h>

// extern variables to communicate with PGPLOT QT driver

extern "C" {
extern int pg_width, pg_height, WINDOW_ID, pg_nbuf, pg_cmd, pg_mousex, pg_mousey;
extern float *pg_rbuf;
extern char pg_mousechar;
}

extern "C" void QtPgParse(void);

class QtPgplot : public QWidget {
	Q_OBJECT
public:
	QtPgplot(QWidget *parent = NULL, const char *name = 0, WFlags f = 0);
	void setDefaultColors(void);

	QPixmap *pixmap;
	QPainter p;
	QColor colors[256];
	int curcol;
	bool colortableChanged;
protected:
	void paintEvent(QPaintEvent *);
	void resizeEvent(QResizeEvent *);
	void closeEvent(QCloseEvent *);
	void mousePressEvent(QMouseEvent *);
	void mouseMoveEvent(QMouseEvent *);
	void keyPressEvent(QKeyEvent *);
};

extern QtPgplot *pgplotWindows[10];
extern QSemaphore *pg_sem;

#endif /* QTPGPLOT_H */
