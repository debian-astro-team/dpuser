#ifndef DPUSER_PGPLOT_H
#define DPUSER_PGPLOT_H

#include "dpuser.h"

typedef struct _pgplot_declarations {
    const char *name;
	int minargs, maxargs;
	long args[20];
	int noptions;
    const char *options[10];
} pgplot_declarations;

//void dopgplot(nodeType *);
void init_procedures();

#endif /* DPUSER_PGPLOT_H */
