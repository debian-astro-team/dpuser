/******************************************************************
 * DPUSER - The Next Generation
 *
 * File:     qtstubs.cpp
 * Purpose:  Stub functions for QFitsView calls which are
 *           not needed for the dpuser command line program
 * Author:   Thomas Ott
 *
 ******************************************************************/
