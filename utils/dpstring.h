#ifndef DPSTRING_H
#define DPSTRING_H

#include <ctype.h>
#include <string>
#include <string.h>

#include "defines.h"

class dpRegExp;

// BEGIN PARSE FOR DPUSER2C

class dpString : public std::string {
public:
	dpString();
	dpString(const char *);
	dpString(const std::string &);
	dpString(const dpString &);
    ~dpString();

	void operator+=(const dpString &);
	void operator+=(const char*);
	void operator+=(const char);

    const char *to_c_str() const;
    dpString mid(dpint64 begin, dpint64 length) const;
	dpString &remove(dpint64, dpint64);

	static dpString number(const long number);
    dpString right(const dpint64 length) const;
    dpString left(const dpint64 length) const;

	dpString upper();
	dpString lower();

	void setNum(const double);
	void setNum(const long);
	void truncate(dpint64 length);

	void sprintf(const char *, ...);
	dpString &replace(dpint64, dpint64, const char *);
	dpString &replace(const dpString &, const dpString &);
	dpString &replace(const dpRegExp &before, const dpString &after);
	
	float toFloat(bool *ok = NULL);
	double toDouble();

	int contains(const char);
	int contains(const char *);
	long strpos(dpString &, const bool);

	dpString simplifyWhiteSpace();
	dpString stripWhiteSpace();
	dpString & strtrim(int = 0);
   
    bool isEmpty() const;
};

// END PARSE FOR DPUSER2C

class dpRegExp : public dpString {
public:
	dpRegExp(const dpString &s) : dpString(s) { }
};

class dpChar {
public:
    dpChar() { rep = 0; }
    dpChar(const char c) { rep = c; }
	bool isDigit() { return isdigit(rep) != 0; }
	bool operator==(const char c) { return rep == c; }
protected:
	char rep;
};

#endif /* DPSTRING_H */
