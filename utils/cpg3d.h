#ifndef CPG3D_H
#define CPG3D_H

#ifdef __cplusplus
extern "C" {
#endif

void cfreddy(const float *a, int kx, int ny, float size, float angle);
void csbfint(const float *rgb, int ic, int ibmode, int ibuf, int *maxbuf);
void csbfbkg(int ic1, int ic2, int ishade);
void csbfsav(int ibuf);
void csbfcls(int ibuf);
void ccolint(const float *rgb, int ic1, int ic2, float diffuse, float shine, float polish);
void ccoltab(const float *rgb, int ncol, float alfa, int ic1, int ic2);
void ccolsrf(const float *rgb, int ncol, float alfa, int ic1, int ic2, int ncband, float difuse, float shine, float polish);
void csbball(const float *eye, const float *centre, float radius, int ic1, int ic2, const float *light, int lshine, float *x0, float *y0, float *r0);
void csbtbal(const float *eye, const float *centre, float radius, int ic1, int ic2, const float *light, int lshine, float *x0, float *y0, float *r0, int itrans);
void csbplan(const float *eye, float nv, const float *vert, int ic1, int ic2, const float *light);
void csbplnt(const float *eye, float nv, const float *vert, int ic1, int ic2, const float *light, int itrans);
void csbrod(const float *eye, const float *end1, const float *end2, float radius, int ic1, int ic2, const float *light, int nsides, int lend);
void csbcone(const float *eye, const float *base, const float *apex, float radius, int ic1, int ic2, const float *light, int nsides);
void csbelip(const float *eye, const float *centre, const float *paxes, int ic1, int ic2, const float *light, int lshine, int icline, float anglin, float *x0, float *y0, float *r0);
void csbline(const float *eye, const float *end1, const float *end2, int icol, int ldash);
void csbtext(const float *eye, const char *text, int icol, const float *pivot, float fjust, const float *orient, float size);
void csbsurf(const float *eye, const float *latice, float *dens, int n1, int n2, int n3, float dsurf, int ic1, int ic2, const float *light, int lshine);
void csbtsur(const float *eye, const float *latice, float *dens, int n1, int n2, int n3, float dsurf, int ic1, int ic2, const float *light, int lshine, int itrans);
void csbslic(const float *eye, const float *latice, float *dens, int n1, int n2, int n3, float dlow, float dhigh, int ic1, int ic2, const float *slnorm, const float *aponit, int icedge);
void csbcpln(const float *eye, const float *latice, int ic1, int ic2, const float *light, const float *slnorm, const float *aponit, int icedge, int itrans);
void csb2srf(const float *eye, const float *latice, float *dens, int n1, int n2, float dlow, float dhigh, float dvert, int ic1, int ic2, int ncband, const float *light, int lshine);
void cxtal(const float *x, int nx, const float *y, int ny, float *z, int n1, int n2, float w, float size, int iwidth, const char *xlbl, const char *ylbl, const char *titl);

#ifdef __cplusplus
}
#endif

#endif /* CPG3D_H */

