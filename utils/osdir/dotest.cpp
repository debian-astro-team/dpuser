#include "osdir.h"

#include <iostream>

int main()
{
	std::cout << "Current directory" << std::endl;
	oslink::directory cwd(".");
	while (cwd)
		std::cout << " " << cwd.next() << std::endl;

	std::cout << "Regular file" << std::endl;
	oslink::directory reg("test.cc");
	while (reg)
		std::cout << " " << reg.next() << std::endl;

	std::cout << "Non existant file" << std::endl;
	oslink::directory non("nonexistant");
	while (non)
		std::cout << " " << non.next() << std::endl;

	std::cout << "C:\\WINDOWS" << std::endl;
	oslink::directory etc("C:\\WINDOWS");
	while (etc)
		std::cout << " " << etc.next() << std::endl;

	std::cout << "That's it" << std::endl;
	return 0;
}
