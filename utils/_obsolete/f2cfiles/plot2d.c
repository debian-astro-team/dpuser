/* .\plot2d.f -- translated by f2c (version 20000704).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#ifdef WIN
#pragma warning (disable: 4244) // disable warning for conversion from double to float, possible loss of data
#pragma warning (disable: 4715) // disable warning for not all control paths return a value
#pragma warning (disable: 4716) // disable warning for must return a value
#endif /* WIN */

#include "f2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__7 = 7;
static real c_b5 = (float)0.;
static real c_b6 = (float)1.;
static real c_b11 = (float)-.87;
static real c_b12 = (float).92;
static real c_b14 = (float).87;
static integer c__0 = 0;
static integer c__1 = 1;
static integer c__16 = 16;
static integer c__17 = 17;
static integer c__256 = 256;
static integer c__255 = 255;
static real c_b53 = (float)-1.2;
static real c_b54 = (float).5;
static real c_b67 = (float)-.5;
static integer c__4 = 4;
static logical c_false = FALSE_;
static real c_b127 = (float)10.;
static integer c__9 = 9;
static integer c__132 = 132;
static integer c__32 = 32;
static real c_b234 = (float).86;
static real c_b235 = (float).9;
static real c_b236 = (float).2;
static real c_b239 = (float)2.;
static integer c__2 = 2;
static real c_b272 = (float).12;
static real c_b273 = (float).82;
static integer c__45 = 45;

/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */
/* ***<plot>************************************************************** */

/* Subroutine */ int xtal_(x, nx, y, ny, z__, n1, n2, w, size, iwidth, xlbl, 
	ylbl, titl, xlbl_len, ylbl_len, titl_len)
real *x;
integer *nx;
real *y;
integer *ny;
real *z__;
integer *n1, *n2;
real *w, *size;
integer *iwidth;
char *xlbl, *ylbl, *titl;
ftnlen xlbl_len;
ftnlen ylbl_len;
ftnlen titl_len;
{
    /* Initialized data */

    static real bgyrw[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float)0.,(float)0.,(float).0195,(float)0.,(float)0.,(
	    float).0391,(float)0.,(float)0.,(float).0586,(float)0.,(float)0.,(
	    float).0781,(float)0.,(float)0.,(float).0977,(float)0.,(float)0.,(
	    float).1172,(float)0.,(float)0.,(float).1367,(float)0.,(float)0.,(
	    float).1563,(float)0.,(float)0.,(float).1758,(float)0.,(float)0.,(
	    float).1992,(float)0.,(float)0.,(float).2188,(float)0.,(float)0.,(
	    float).2383,(float)0.,(float)0.,(float).2578,(float)0.,(float)0.,(
	    float).2773,(float)0.,(float)0.,(float).2969,(float)0.,(float)0.,(
	    float).3164,(float)0.,(float)0.,(float).3359,(float)0.,(float)0.,(
	    float).3555,(float)0.,(float)0.,(float).375,(float)0.,(float)0.,(
	    float).3984,(float)0.,(float)0.,(float).418,(float)0.,(float)0.,(
	    float).4375,(float)0.,(float)0.,(float).457,(float)0.,(float)0.,(
	    float).4766,(float)0.,(float)0.,(float).4961,(float)0.,(float)0.,(
	    float).5156,(float)0.,(float)0.,(float).5352,(float)0.,(float)0.,(
	    float).5547,(float)0.,(float)0.,(float).5742,(float)0.,(float)0.,(
	    float).5977,(float)0.,(float)0.,(float).6172,(float)0.,(float)0.,(
	    float).6367,(float)0.,(float)0.,(float).6563,(float)0.,(float)0.,(
	    float).6758,(float)0.,(float)0.,(float).6953,(float)0.,(float)0.,(
	    float).7148,(float)0.,(float)0.,(float).7344,(float)0.,(float)0.,(
	    float).7539,(float)0.,(float)0.,(float).7734,(float)0.,(float)0.,(
	    float).7969,(float)0.,(float)0.,(float).8164,(float)0.,(float)0.,(
	    float).8359,(float)0.,(float)0.,(float).8555,(float)0.,(float)0.,(
	    float).875,(float)0.,(float)0.,(float).8945,(float)0.,(float)0.,(
	    float).9141,(float)0.,(float)0.,(float).9336,(float)0.,(float)0.,(
	    float).9531,(float)0.,(float)0.,(float).9727,(float)0.,(float)0.,(
	    float).9961,(float)0.,(float).0156,(float).9961,(float)0.,(float)
	    .0469,(float).9961,(float)0.,(float).0781,(float).9961,(float)0.,(
	    float).0938,(float).9961,(float)0.,(float).125,(float).9961,(
	    float)0.,(float).1563,(float).9961,(float)0.,(float).1719,(float)
	    .9961,(float)0.,(float).2031,(float).9961,(float)0.,(float).2344,(
	    float).9961,(float)0.,(float).2656,(float).9961,(float)0.,(float)
	    .2813,(float).9961,(float)0.,(float).3125,(float).9961,(float)0.,(
	    float).3438,(float).9961,(float)0.,(float).3594,(float).9961,(
	    float)0.,(float).3906,(float).9961,(float)0.,(float).4219,(float)
	    .9961,(float)0.,(float).4375,(float).9961,(float)0.,(float).4688,(
	    float).9961,(float)0.,(float).5,(float).9961,(float)0.,(float)
	    .5313,(float).9961,(float)0.,(float).5469,(float).9961,(float)0.,(
	    float).5781,(float).9961,(float)0.,(float).6094,(float).9961,(
	    float)0.,(float).625,(float).9961,(float)0.,(float).6563,(float)
	    .9961,(float)0.,(float).6875,(float).9961,(float)0.,(float).7031,(
	    float).9961,(float)0.,(float).7344,(float).9961,(float)0.,(float)
	    .7656,(float).9961,(float)0.,(float).7969,(float).9961,(float)0.,(
	    float).8125,(float).9961,(float)0.,(float).8438,(float).9961,(
	    float)0.,(float).875,(float).9961,(float)0.,(float).8906,(float)
	    .9961,(float)0.,(float).9219,(float).9961,(float)0.,(float).9531,(
	    float).9961,(float)0.,(float).9688,(float).9961,(float)0.,(float)
	    .9961,(float).9844,(float)0.,(float).9961,(float).9531,(float)0.,(
	    float).9961,(float).9219,(float)0.,(float).9961,(float).9063,(
	    float)0.,(float).9961,(float).875,(float)0.,(float).9961,(float)
	    .8438,(float)0.,(float).9961,(float).8281,(float)0.,(float).9961,(
	    float).7969,(float)0.,(float).9961,(float).7656,(float)0.,(float)
	    .9961,(float).75,(float)0.,(float).9961,(float).7188,(float)0.,(
	    float).9961,(float).6875,(float)0.,(float).9961,(float).6563,(
	    float)0.,(float).9961,(float).6406,(float)0.,(float).9961,(float)
	    .6094,(float)0.,(float).9961,(float).5781,(float)0.,(float).9961,(
	    float).5625,(float)0.,(float).9961,(float).5313,(float)0.,(float)
	    .9961,(float).5,(float)0.,(float).9961,(float).4844,(float)0.,(
	    float).9961,(float).4531,(float)0.,(float).9961,(float).4219,(
	    float)0.,(float).9961,(float).3906,(float)0.,(float).9961,(float)
	    .375,(float)0.,(float).9961,(float).3438,(float)0.,(float).9961,(
	    float).3125,(float)0.,(float).9961,(float).2969,(float)0.,(float)
	    .9961,(float).2656,(float)0.,(float).9961,(float).2344,(float)0.,(
	    float).9961,(float).2188,(float)0.,(float).9961,(float).1875,(
	    float)0.,(float).9961,(float).1563,(float)0.,(float).9961,(float)
	    .125,(float)0.,(float).9961,(float).1094,(float)0.,(float).9961,(
	    float).0781,(float)0.,(float).9961,(float).0469,(float)0.,(float)
	    .9961,(float).0313,(float)0.,(float).9961,(float)0.,(float).0156,(
	    float).9961,(float)0.,(float).0313,(float).9961,(float)0.,(float)
	    .0625,(float).9961,(float)0.,(float).0938,(float).9961,(float)0.,(
	    float).125,(float).9961,(float)0.,(float).1406,(float).9961,(
	    float)0.,(float).1719,(float).9961,(float)0.,(float).2031,(float)
	    .9961,(float)0.,(float).2188,(float).9961,(float)0.,(float).25,(
	    float).9961,(float)0.,(float).2813,(float).9961,(float)0.,(float)
	    .2969,(float).9961,(float)0.,(float).3281,(float).9961,(float)0.,(
	    float).3594,(float).9961,(float)0.,(float).3906,(float).9961,(
	    float)0.,(float).4063,(float).9961,(float)0.,(float).4375,(float)
	    .9961,(float)0.,(float).4688,(float).9961,(float)0.,(float).4844,(
	    float).9961,(float)0.,(float).5156,(float).9961,(float)0.,(float)
	    .5469,(float).9961,(float)0.,(float).5625,(float).9961,(float)0.,(
	    float).5938,(float).9961,(float)0.,(float).625,(float).9961,(
	    float)0.,(float).6563,(float).9961,(float)0.,(float).6719,(float)
	    .9961,(float)0.,(float).7031,(float).9961,(float)0.,(float).7344,(
	    float).9961,(float)0.,(float).75,(float).9961,(float)0.,(float)
	    .7813,(float).9961,(float)0.,(float).8125,(float).9961,(float)0.,(
	    float).8281,(float).9961,(float)0.,(float).8594,(float).9961,(
	    float)0.,(float).8906,(float).9961,(float)0.,(float).9219,(float)
	    .9961,(float)0.,(float).9375,(float).9961,(float)0.,(float).9688,(
	    float).9961,(float)0.,(float).9961,(float).9844,(float)0.,(float)
	    .9961,(float).9688,(float)0.,(float).9961,(float).9375,(float)0.,(
	    float).9961,(float).9063,(float)0.,(float).9961,(float).8906,(
	    float)0.,(float).9961,(float).8594,(float)0.,(float).9961,(float)
	    .8281,(float)0.,(float).9961,(float).7969,(float)0.,(float).9961,(
	    float).7813,(float)0.,(float).9961,(float).75,(float)0.,(float)
	    .9961,(float).7188,(float)0.,(float).9961,(float).7031,(float)0.,(
	    float).9961,(float).6719,(float)0.,(float).9961,(float).6406,(
	    float)0.,(float).9961,(float).625,(float)0.,(float).9961,(float)
	    .5938,(float)0.,(float).9961,(float).5625,(float)0.,(float).9961,(
	    float).5313,(float)0.,(float).9961,(float).5156,(float)0.,(float)
	    .9961,(float).4844,(float)0.,(float).9961,(float).4531,(float)0.,(
	    float).9961,(float).4375,(float)0.,(float).9961,(float).4063,(
	    float)0.,(float).9961,(float).375,(float)0.,(float).9961,(float)
	    .3594,(float)0.,(float).9961,(float).3281,(float)0.,(float).9961,(
	    float).2969,(float)0.,(float).9961,(float).2656,(float)0.,(float)
	    .9961,(float).25,(float)0.,(float).9961,(float).2188,(float)0.,(
	    float).9961,(float).1875,(float)0.,(float).9961,(float).1719,(
	    float)0.,(float).9961,(float).1406,(float)0.,(float).9961,(float)
	    .1094,(float)0.,(float).9961,(float).0938,(float)0.,(float).9961,(
	    float).0625,(float)0.,(float).9961,(float).0313,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float).0273,(float).0273,(
	    float).9961,(float).0547,(float).0547,(float).9961,(float).082,(
	    float).082,(float).9961,(float).1133,(float).1133,(float).9961,(
	    float).1406,(float).1406,(float).9961,(float).168,(float).168,(
	    float).9961,(float).1992,(float).1992,(float).9961,(float).2266,(
	    float).2266,(float).9961,(float).2539,(float).2539,(float).9961,(
	    float).2813,(float).2813,(float).9961,(float).3125,(float).3125,(
	    float).9961,(float).3398,(float).3398,(float).9961,(float).3672,(
	    float).3672,(float).9961,(float).3984,(float).3984,(float).9961,(
	    float).4258,(float).4258,(float).9961,(float).4531,(float).4531,(
	    float).9961,(float).4805,(float).4805,(float).9961,(float).5117,(
	    float).5117,(float).9961,(float).5391,(float).5391,(float).9961,(
	    float).5664,(float).5664,(float).9961,(float).5977,(float).5977,(
	    float).9961,(float).625,(float).625,(float).9961,(float).6523,(
	    float).6523,(float).9961,(float).6797,(float).6797,(float).9961,(
	    float).7109,(float).7109,(float).9961,(float).7383,(float).7383,(
	    float).9961,(float).7656,(float).7656,(float).9961,(float).7969,(
	    float).7969,(float).9961,(float).8242,(float).8242,(float).9961,(
	    float).8516,(float).8516,(float).9961,(float).8789,(float).8789,(
	    float).9961,(float).9102,(float).9102,(float).9961,(float).9375,(
	    float).9375,(float).9961,(float).9648,(float).9648,(float).9961,(
	    float).9961,(float).9961 };
    static real heat[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)
	    0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(
	    float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)
	    0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(
	    float)0.,(float)0.,(float).0195,(float)0.,(float).0195,(float)
	    .0391,(float)0.,(float).0391,(float).0586,(float)0.,(float).0586,(
	    float).0781,(float)0.,(float).0781,(float).1016,(float)0.,(float)
	    .1016,(float).1211,(float)0.,(float).1211,(float).1406,(float)0.,(
	    float).1406,(float).1602,(float)0.,(float).1602,(float).1797,(
	    float)0.,(float).1797,(float).1992,(float)0.,(float).1992,(float)
	    .2188,(float)0.,(float).2188,(float).2383,(float)0.,(float).2383,(
	    float).2578,(float)0.,(float).2578,(float).2773,(float)0.,(float)
	    .2773,(float).3008,(float)0.,(float).3008,(float).3203,(float)0.,(
	    float).3203,(float).3398,(float)0.,(float).3398,(float).3594,(
	    float)0.,(float).3594,(float).3789,(float)0.,(float).3789,(float)
	    .3984,(float)0.,(float).3984,(float).418,(float)0.,(float).418,(
	    float).4375,(float)0.,(float).4375,(float).457,(float)0.,(float)
	    .457,(float).4766,(float)0.,(float).4766,(float).5,(float)0.,(
	    float).5,(float).5195,(float)0.,(float).5195,(float).5391,(float)
	    0.,(float).5391,(float).5586,(float)0.,(float).5586,(float).5781,(
	    float)0.,(float).5781,(float).5977,(float)0.,(float).5977,(float)
	    .6172,(float)0.,(float).6172,(float).6367,(float)0.,(float).6367,(
	    float).6563,(float)0.,(float).6563,(float).6758,(float)0.,(float)
	    .6758,(float).6992,(float)0.,(float).6992,(float).7188,(float)0.,(
	    float).7188,(float).7383,(float)0.,(float).7383,(float).7578,(
	    float)0.,(float).7578,(float).7773,(float)0.,(float).7773,(float)
	    .7969,(float)0.,(float).7969,(float).8164,(float)0.,(float).8164,(
	    float).8359,(float)0.,(float).8359,(float).8555,(float)0.,(float)
	    .8555,(float).875,(float)0.,(float).875,(float).8984,(float)0.,(
	    float).8984,(float).9375,(float)0.,(float).918,(float).9766,(
	    float)0.,(float).9375,(float).9961,(float)0.,(float).957,(float)
	    .9961,(float)0.,(float).9766,(float).9961,(float)0.,(float).9961,(
	    float).9961,(float)0.,(float).9766,(float).9961,(float)0.,(float)
	    .957,(float).9961,(float)0.,(float).9375,(float).9961,(float)0.,(
	    float).918,(float).9961,(float)0.,(float).8984,(float).9961,(
	    float)0.,(float).875,(float).9961,(float)0.,(float).8555,(float)
	    .9961,(float)0.,(float).8359,(float).9961,(float)0.,(float).8164,(
	    float).9961,(float)0.,(float).7969,(float).9961,(float)0.,(float)
	    .7773,(float).9961,(float)0.,(float).7578,(float).9961,(float)0.,(
	    float).7383,(float).9961,(float)0.,(float).7188,(float).9961,(
	    float)0.,(float).6992,(float).9961,(float)0.,(float).6758,(float)
	    .9961,(float)0.,(float).6563,(float).9961,(float)0.,(float).6367,(
	    float).9961,(float)0.,(float).6172,(float).9961,(float)0.,(float)
	    .5977,(float).9961,(float)0.,(float).5781,(float).9961,(float)0.,(
	    float).5586,(float).9961,(float)0.,(float).5391,(float).9961,(
	    float)0.,(float).5195,(float).9961,(float)0.,(float).5,(float)
	    .9961,(float)0.,(float).4766,(float).9961,(float)0.,(float).457,(
	    float).9961,(float)0.,(float).4375,(float).9961,(float)0.,(float)
	    .418,(float).9961,(float)0.,(float).3984,(float).9961,(float)0.,(
	    float).3789,(float).9961,(float)0.,(float).3594,(float).9961,(
	    float)0.,(float).3398,(float).9961,(float)0.,(float).3203,(float)
	    .9961,(float)0.,(float).3008,(float).9961,(float)0.,(float).2773,(
	    float).9961,(float)0.,(float).2578,(float).9961,(float)0.,(float)
	    .2383,(float).9961,(float)0.,(float).2188,(float).9961,(float)0.,(
	    float).1992,(float).9961,(float)0.,(float).1797,(float).9961,(
	    float).0117,(float).1602,(float).9961,(float).0195,(float).1406,(
	    float).9961,(float).0313,(float).1211,(float).9961,(float).0391,(
	    float)0.,(float).9961,(float).0508,(float).0781,(float).9961,(
	    float).0586,(float).0586,(float).9961,(float).0703,(float).0391,(
	    float).9961,(float).0781,(float).0195,(float).9961,(float).0898,(
	    float)0.,(float).9961,(float).1016,(float)0.,(float).9961,(float)
	    .1094,(float)0.,(float).9961,(float).1211,(float)0.,(float).9961,(
	    float).1289,(float)0.,(float).9961,(float).1406,(float)0.,(float)
	    .9961,(float).1484,(float)0.,(float).9961,(float).1602,(float)0.,(
	    float).9961,(float).1719,(float)0.,(float).9961,(float).1797,(
	    float)0.,(float).9961,(float).1914,(float)0.,(float).9961,(float)
	    .1992,(float)0.,(float).9961,(float).2109,(float)0.,(float).9961,(
	    float).2188,(float)0.,(float).9961,(float).2305,(float)0.,(float)
	    .9961,(float).2383,(float)0.,(float).9961,(float).25,(float)0.,(
	    float).9961,(float).2617,(float)0.,(float).9961,(float).2695,(
	    float)0.,(float).9961,(float).2813,(float)0.,(float).9961,(float)
	    .2891,(float)0.,(float).9961,(float).3008,(float)0.,(float).9961,(
	    float).3086,(float)0.,(float).9961,(float).3203,(float)0.,(float)
	    .9961,(float).3281,(float)0.,(float).9961,(float).3398,(float)0.,(
	    float).9961,(float).3516,(float)0.,(float).9961,(float).3594,(
	    float)0.,(float).9961,(float).3711,(float)0.,(float).9961,(float)
	    .3789,(float)0.,(float).9961,(float).3906,(float)0.,(float).9961,(
	    float).3984,(float)0.,(float).9961,(float).4102,(float)0.,(float)
	    .9961,(float).4219,(float)0.,(float).9961,(float).4297,(float)0.,(
	    float).9961,(float).4414,(float)0.,(float).9961,(float).4492,(
	    float)0.,(float).9961,(float).4609,(float)0.,(float).9961,(float)
	    .4688,(float)0.,(float).9961,(float).4805,(float)0.,(float).9961,(
	    float).4883,(float)0.,(float).9961,(float).5195,(float)0.,(float)
	    .9961,(float).5273,(float)0.,(float).9961,(float).5391,(float)0.,(
	    float).9961,(float).5469,(float)0.,(float).9961,(float).5625,(
	    float)0.,(float).9961,(float).5703,(float)0.,(float).9961,(float)
	    .582,(float)0.,(float).9961,(float).5898,(float)0.,(float).9961,(
	    float).6016,(float)0.,(float).9961,(float).6094,(float)0.,(float)
	    .9961,(float).6172,(float)0.,(float).9961,(float).6289,(float)0.,(
	    float).9961,(float).6367,(float)0.,(float).9961,(float).6484,(
	    float)0.,(float).9961,(float).6602,(float)0.,(float).9961,(float)
	    .6719,(float)0.,(float).9961,(float).6797,(float)0.,(float).9961,(
	    float).6875,(float)0.,(float).9961,(float).6992,(float)0.,(float)
	    .9961,(float).707,(float)0.,(float).9961,(float).7188,(float)0.,(
	    float).9961,(float).7266,(float)0.,(float).9961,(float).7383,(
	    float)0.,(float).9961,(float).7461,(float)0.,(float).9961,(float)
	    .7617,(float)0.,(float).9961,(float).7695,(float)0.,(float).9961,(
	    float).7773,(float)0.,(float).9961,(float).7891,(float)0.,(float)
	    .9961,(float).7969,(float)0.,(float).9961,(float).8086,(float)0.,(
	    float).9961,(float).8164,(float)0.,(float).9961,(float).8281,(
	    float)0.,(float).9961,(float).8359,(float)0.,(float).9961,(float)
	    .8477,(float)0.,(float).9961,(float).8594,(float)0.,(float).9961,(
	    float).8672,(float)0.,(float).9961,(float).8789,(float)0.,(float)
	    .9961,(float).8867,(float)0.,(float).9961,(float).8984,(float)0.,(
	    float).9961,(float).9063,(float)0.,(float).9961,(float).918,(
	    float)0.,(float).9961,(float).9258,(float)0.,(float).9961,(float)
	    .9688,(float).0156,(float).9961,(float).9961,(float).0352,(float)
	    .9961,(float).9961,(float).0508,(float).9961,(float).9961,(float)
	    .0664,(float).9961,(float).9961,(float).082,(float).9961,(float)
	    .9961,(float).1016,(float).9961,(float).9961,(float).1172,(float)
	    .9961,(float).9961,(float).1328,(float).9961,(float).9961,(float)
	    .1484,(float).9961,(float).9961,(float).168,(float).9961,(float)
	    .9961,(float).1836,(float).9961,(float).9961,(float).1992,(float)
	    .9961,(float).9961,(float).2148,(float).9961,(float).9961,(float)
	    .2344,(float).9961,(float).9961,(float).25,(float).9961,(float)
	    .9961,(float).2656,(float).9961,(float).9961,(float).2852,(float)
	    .9961,(float).9961,(float).3008,(float).9961,(float).9961,(float)
	    .3164,(float).9961,(float).9961,(float).332,(float).9961,(float)
	    .9961,(float).3516,(float).9961,(float).9961,(float).3672,(float)
	    .9961,(float).9961,(float).3828,(float).9961,(float).9961,(float)
	    .3984,(float).9961,(float).9961,(float).418,(float).9961,(float)
	    .9961,(float).4336,(float).9961,(float).9961,(float).4492,(float)
	    .9961,(float).9961,(float).4648,(float).9961,(float).9961,(float)
	    .4844,(float).9961,(float).9961,(float).5,(float).9961,(float)
	    .9961,(float).4844,(float).9961,(float).9961,(float).4648,(float)
	    .9961,(float).9961,(float).4492,(float).9961,(float).9961,(float)
	    .4336,(float).9961,(float).9961,(float).4531,(float).9961,(float)
	    .9961,(float).4648,(float).9961,(float).9961,(float).4844,(float)
	    .9961,(float).9961,(float).5,(float).9961,(float).9961,(float)
	    .5195,(float).9961,(float).9961,(float).5313,(float).9961,(float)
	    .9961,(float).5508,(float).9961,(float).9961,(float).5664,(float)
	    .9961,(float).9961,(float).5859,(float).9961,(float).9961,(float)
	    .5977,(float).9961,(float).9961,(float).6172,(float).9961,(float)
	    .9961,(float).6328,(float).9961,(float).9961,(float).6484,(float)
	    .9961,(float).9961,(float).6641,(float).9961,(float).9961,(float)
	    .6836,(float).9961,(float).9961,(float).6992,(float).9961,(float)
	    .9961,(float).7148,(float).9844,(float).9844,(float).7305,(float)
	    .9688,(float).9688,(float).75,(float).9414,(float).9414,(float)
	    .7656,(float).9258,(float).9258,(float).7813,(float).9023,(float)
	    .9023,(float).7969,(float).8867,(float).8867,(float).8164,(float)
	    .8633,(float).8633,(float).832,(float).8672,(float).8672,(float)
	    .8477,(float).8633,(float).8633,(float).8633,(float).8984,(float)
	    .8984,(float).8984,(float).9297,(float).9297,(float).9297,(float)
	    .9648,(float).9648,(float).9648,(float).9961,(float).9961,(float)
	    .9961 };
    static real spectrum[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float).016,(float).169,(float).91,(float).012,(float)
	    .184,(float).922,(float).0118,(float).1961,(float).9373,(float)
	    .0078,(float).2078,(float).949,(float).0039,(float).2235,(float)
	    .9608,(float)0.,(float).2392,(float).9765,(float)0.,(float).251,(
	    float).9882,(float)0.,(float).2627,(float)1.,(float)0.,(float)
	    .2627,(float)1.,(float)0.,(float).2745,(float).9882,(float)0.,(
	    float).2863,(float).9804,(float)0.,(float).298,(float).9686,(
	    float)0.,(float).3098,(float).9569,(float)0.,(float).3255,(float)
	    .949,(float)0.,(float).3373,(float).9373,(float)0.,(float).349,(
	    float).9294,(float)0.,(float).3647,(float).9176,(float)0.,(float)
	    .3804,(float).9059,(float)0.,(float).3922,(float).8941,(float)0.,(
	    float).4039,(float).8863,(float)0.,(float).4157,(float).8745,(
	    float)0.,(float).4275,(float).8667,(float)0.,(float).4431,(float)
	    .8549,(float)0.,(float).4549,(float).8431,(float)0.,(float).4667,(
	    float).8353,(float)0.,(float).4784,(float).8235,(float)0.,(float)
	    .4941,(float).8118,(float)0.,(float).5098,(float).8039,(float)0.,(
	    float).5216,(float).7922,(float)0.,(float).5333,(float).7804,(
	    float)0.,(float).5451,(float).7725,(float)0.,(float).5608,(float)
	    .7647,(float)0.,(float).5725,(float).7529,(float)0.,(float).5843,(
	    float).7412,(float)0.,(float).5961,(float).7333,(float)0.,(float)
	    .6118,(float).7216,(float)0.,(float).6235,(float).7098,(float)0.,(
	    float).6353,(float).702,(float)0.,(float).651,(float).6902,(float)
	    0.,(float).6627,(float).6784,(float)0.,(float).6745,(float).6706,(
	    float)0.,(float).6902,(float).6588,(float)0.,(float).702,(float)
	    .6471,(float)0.,(float).7137,(float).6392,(float)0.,(float).7294,(
	    float).6314,(float)0.,(float).7412,(float).6196,(float)0.,(float)
	    .7529,(float).6078,(float)0.,(float).7647,(float).6,(float)0.,(
	    float).7804,(float).5882,(float)0.,(float).7843,(float).5882,(
	    float)0.,(float).7843,(float).5804,(float)0.,(float).7882,(float)
	    .5647,(float)0.,(float).7961,(float).549,(float)0.,(float).8,(
	    float).5333,(float)0.,(float).8078,(float).5216,(float)0.,(float)
	    .8118,(float).5059,(float)0.,(float).8157,(float).4902,(float)0.,(
	    float).8235,(float).4745,(float)0.,(float).8275,(float).4627,(
	    float)0.,(float).8353,(float).4471,(float)0.,(float).8392,(float)
	    .4314,(float)0.,(float).8431,(float).4196,(float)0.,(float).8471,(
	    float).4039,(float)0.,(float).8549,(float).3882,(float)0.,(float)
	    .8588,(float).3725,(float)0.,(float).8667,(float).3569,(float)0.,(
	    float).8745,(float).3451,(float)0.,(float).8784,(float).3294,(
	    float)0.,(float).8824,(float).3137,(float)0.,(float).8863,(float)
	    .302,(float)0.,(float).8941,(float).2863,(float)0.,(float).898,(
	    float).2706,(float)0.,(float).902,(float).2549,(float)0.,(float)
	    .9059,(float).2392,(float)0.,(float).9137,(float).2275,(float)0.,(
	    float).9176,(float).2118,(float)0.,(float).9255,(float).2,(float)
	    0.,(float).9333,(float).1843,(float)0.,(float).9373,(float).1686,(
	    float)0.,(float).9412,(float).1529,(float)0.,(float).9451,(float)
	    .1373,(float)0.,(float).9529,(float).1255,(float)0.,(float).9569,(
	    float).1098,(float)0.,(float).9647,(float).098,(float)0.,(float)
	    .9686,(float).0824,(float)0.,(float).9725,(float).0667,(float)0.,(
	    float).9765,(float).051,(float)0.,(float).9843,(float).0353,(
	    float)0.,(float).9922,(float).0196,(float)0.,(float).9961,(float)
	    .0078,(float)0.,(float)1.,(float)0.,(float).0039,(float)1.,(float)
	    0.,(float).0275,(float)1.,(float)0.,(float).051,(float)1.,(float)
	    0.,(float).0745,(float)1.,(float)0.,(float).098,(float)1.,(float)
	    0.,(float).1216,(float)1.,(float)0.,(float).149,(float)1.,(float)
	    0.,(float).1765,(float)1.,(float)0.,(float).2,(float)1.,(float)0.,
	    (float).2235,(float)1.,(float)0.,(float).2471,(float)1.,(float)0.,
	    (float).2745,(float)1.,(float)0.,(float).302,(float)1.,(float)0.,(
	    float).3255,(float)1.,(float)0.,(float).349,(float)1.,(float)0.,(
	    float).3725,(float)1.,(float)0.,(float).3961,(float)1.,(float)0.,(
	    float).4235,(float)1.,(float)0.,(float).451,(float)1.,(float)0.,(
	    float).4745,(float)1.,(float)0.,(float).498,(float)1.,(float)0.,(
	    float).5216,(float)1.,(float)0.,(float).549,(float)1.,(float)0.,(
	    float).5765,(float)1.,(float)0.,(float).6,(float)1.,(float)0.,(
	    float).6235,(float)1.,(float)0.,(float).6471,(float)1.,(float)0.,(
	    float).6706,(float)1.,(float)0.,(float).698,(float)1.,(float)0.,(
	    float).7216,(float)1.,(float)0.,(float).749,(float)1.,(float)0.,(
	    float).7725,(float)1.,(float)0.,(float).7961,(float)1.,(float)0.,(
	    float).8235,(float)1.,(float)0.,(float).8471,(float)1.,(float)0.,(
	    float).8745,(float)1.,(float)0.,(float).898,(float)1.,(float)0.,(
	    float).9216,(float)1.,(float)0.,(float).9451,(float)1.,(float)0.,(
	    float).9725,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(
	    float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)
	    1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(
	    float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)
	    1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(
	    float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)
	    0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(
	    float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)
	    1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(
	    float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)
	    1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(
	    float)0.,(float)1.,(float).9843,(float)0.,(float)1.,(float).9725,(
	    float)0.,(float)1.,(float).9608,(float)0.,(float)1.,(float).949,(
	    float)0.,(float)1.,(float).9373,(float)0.,(float)1.,(float).9255,(
	    float)0.,(float)1.,(float).9098,(float)0.,(float)1.,(float).898,(
	    float)0.,(float)1.,(float).8863,(float)0.,(float)1.,(float).8745,(
	    float)0.,(float)1.,(float).8627,(float)0.,(float)1.,(float).851,(
	    float)0.,(float)1.,(float).8392,(float)0.,(float)1.,(float).8275,(
	    float)0.,(float)1.,(float).8157,(float)0.,(float)1.,(float).8039,(
	    float)0.,(float)1.,(float).7922,(float)0.,(float)1.,(float).7765,(
	    float)0.,(float)1.,(float).7647,(float)0.,(float)1.,(float).7529,(
	    float)0.,(float)1.,(float).7412,(float)0.,(float)1.,(float).7294,(
	    float)0.,(float)1.,(float).7176,(float)0.,(float)1.,(float).7059,(
	    float)0.,(float)1.,(float).6941,(float)0.,(float)1.,(float).6824,(
	    float)0.,(float)1.,(float).6706,(float)0.,(float)1.,(float).6549,(
	    float)0.,(float)1.,(float).6392,(float)0.,(float)1.,(float).6275,(
	    float)0.,(float)1.,(float).6157,(float)0.,(float)1.,(float).6039,(
	    float)0.,(float)1.,(float).5922,(float)0.,(float)1.,(float).5804,(
	    float)0.,(float)1.,(float).5686,(float)0.,(float)1.,(float).5569,(
	    float)0.,(float)1.,(float).5451,(float)0.,(float)1.,(float).5333,(
	    float)0.,(float)1.,(float).5216,(float)0.,(float)1.,(float).5098,(
	    float)0.,(float)1.,(float).4941,(float)0.,(float)1.,(float).4824,(
	    float)0.,(float)1.,(float).4706,(float)0.,(float)1.,(float).4588,(
	    float)0.,(float)1.,(float).4471,(float)0.,(float)1.,(float).4353,(
	    float)0.,(float)1.,(float).4235,(float)0.,(float)1.,(float).4118,(
	    float)0.,(float)1.,(float).4,(float)0.,(float)1.,(float).3882,(
	    float)0.,(float)1.,(float).3765,(float)0.,(float)1.,(float).3608,(
	    float)0.,(float)1.,(float).349,(float)0.,(float)1.,(float).3373,(
	    float)0.,(float)1.,(float).3255,(float)0.,(float)1.,(float).3098,(
	    float)0.,(float)1.,(float).298,(float)0.,(float)1.,(float).2863,(
	    float)0.,(float)1.,(float).2745,(float)0.,(float)1.,(float).2627,(
	    float)0.,(float)1.,(float).251,(float)0.,(float)1.,(float).2392,(
	    float)0.,(float)1.,(float).2235,(float)0.,(float)1.,(float).2118,(
	    float)0.,(float)1.,(float).2,(float)0.,(float)1.,(float).1882,(
	    float)0.,(float)1.,(float).1765,(float)0.,(float)1.,(float).1647,(
	    float)0.,(float)1.,(float).1529,(float)0.,(float)1.,(float).1412,(
	    float)0.,(float)1.,(float).1294,(float)0.,(float)1.,(float).1176,(
	    float)0.,(float)1.,(float).1059,(float)0.,(float)1.,(float).0902,(
	    float)0.,(float)1.,(float).0784,(float)0.,(float)1.,(float).0667,(
	    float)0.,(float)1.,(float).0549,(float)0.,(float)1.,(float).0431,(
	    float)0.,(float)1.,(float).0314,(float)0.,(float)1.,(float).0196,(
	    float)0.,(float)1.,(float).0078,(float)0.,(float)1.,(float)0.,(
	    float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)
	    0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(
	    float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)
	    1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(
	    float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)
	    0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(
	    float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)
	    0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(
	    float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0. };
    static real serp[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float)0.,(float)1.,(float)1.,(float).0204,(float)1.,(
	    float).9796,(float).0408,(float)1.,(float).9592,(float).0612,(
	    float)1.,(float).9388,(float).0816,(float)1.,(float).9184,(float)
	    .102,(float)1.,(float).898,(float).1224,(float)1.,(float).8776,(
	    float).1429,(float)1.,(float).8571,(float).1633,(float)1.,(float)
	    .8367,(float).1837,(float)1.,(float).8163,(float).2041,(float)1.,(
	    float).7959,(float).2245,(float)1.,(float).7755,(float).2449,(
	    float)1.,(float).7551,(float).2653,(float)1.,(float).7347,(float)
	    .2857,(float)1.,(float).7143,(float).3061,(float)1.,(float).6939,(
	    float).3265,(float)1.,(float).6735,(float).3469,(float)1.,(float)
	    .6531,(float).3673,(float)1.,(float).6327,(float).3878,(float)1.,(
	    float).6122,(float).4082,(float)1.,(float).5918,(float).4286,(
	    float)1.,(float).5714,(float).449,(float)1.,(float).551,(float)
	    .4694,(float)1.,(float).5306,(float).4898,(float)1.,(float).5102,(
	    float).5102,(float)1.,(float).4898,(float).5306,(float)1.,(float)
	    .4694,(float).551,(float)1.,(float).449,(float).5714,(float)1.,(
	    float).4286,(float).5918,(float)1.,(float).4082,(float).6122,(
	    float)1.,(float).3878,(float).6327,(float)1.,(float).3673,(float)
	    .6531,(float)1.,(float).3469,(float).6735,(float)1.,(float).3265,(
	    float).6939,(float)1.,(float).3061,(float).7143,(float)1.,(float)
	    .2857,(float).7347,(float)1.,(float).2653,(float).7551,(float)1.,(
	    float).2449,(float).7755,(float)1.,(float).2245,(float).7959,(
	    float)1.,(float).2041,(float).8163,(float)1.,(float).1837,(float)
	    .8367,(float)1.,(float).1633,(float).8571,(float)1.,(float).1429,(
	    float).8776,(float)1.,(float).1224,(float).898,(float)1.,(float)
	    .102,(float).9184,(float)1.,(float).0816,(float).9388,(float)1.,(
	    float).0612,(float).9592,(float)1.,(float).0408,(float).9796,(
	    float)1.,(float).0204,(float)1.,(float)1.,(float)0.,(float)1.,(
	    float).98,(float).02,(float)1.,(float).96,(float).04,(float)1.,(
	    float).94,(float).06,(float)1.,(float).92,(float).08,(float)1.,(
	    float).9,(float).1,(float)1.,(float).88,(float).12,(float)1.,(
	    float).86,(float).14,(float)1.,(float).84,(float).16,(float)1.,(
	    float).82,(float).18,(float)1.,(float).8,(float).2,(float)1.,(
	    float).78,(float).22,(float)1.,(float).76,(float).24,(float)1.,(
	    float).74,(float).26,(float)1.,(float).72,(float).28,(float)1.,(
	    float).7,(float).3,(float)1.,(float).68,(float).32,(float)1.,(
	    float).66,(float).34,(float)1.,(float).64,(float).36,(float)1.,(
	    float).62,(float).38,(float)1.,(float).6,(float).4,(float)1.,(
	    float).58,(float).42,(float)1.,(float).56,(float).44,(float)1.,(
	    float).54,(float).46,(float)1.,(float).52,(float).48,(float)1.,(
	    float).5,(float).5,(float)1.,(float).48,(float).52,(float)1.,(
	    float).46,(float).54,(float)1.,(float).44,(float).56,(float)1.,(
	    float).42,(float).58,(float)1.,(float).4,(float).6,(float)1.,(
	    float).38,(float).62,(float)1.,(float).36,(float).64,(float)1.,(
	    float).34,(float).66,(float)1.,(float).32,(float).68,(float)1.,(
	    float).3,(float).7,(float)1.,(float).28,(float).72,(float)1.,(
	    float).26,(float).74,(float)1.,(float).24,(float).76,(float)1.,(
	    float).22,(float).78,(float)1.,(float).2,(float).8,(float)1.,(
	    float).18,(float).82,(float)1.,(float).16,(float).84,(float)1.,(
	    float).14,(float).86,(float)1.,(float).12,(float).88,(float)1.,(
	    float).1,(float).9,(float)1.,(float).08,(float).92,(float)1.,(
	    float).06,(float).94,(float)1.,(float).04,(float).96,(float)1.,(
	    float).02,(float).98,(float)1.,(float)0.,(float)1.,(float)1.,(
	    float)0.,(float).99,(float)1.,(float)0.,(float).98,(float)1.,(
	    float)0.,(float).97,(float)1.,(float)0.,(float).96,(float)1.,(
	    float)0.,(float).95,(float)1.,(float)0.,(float).94,(float)1.,(
	    float)0.,(float).93,(float)1.,(float)0.,(float).92,(float)1.,(
	    float)0.,(float).91,(float)1.,(float)0.,(float).9,(float)1.,(
	    float)0.,(float).89,(float)1.,(float)0.,(float).88,(float)1.,(
	    float)0.,(float).87,(float)1.,(float)0.,(float).86,(float)1.,(
	    float)0.,(float).85,(float)1.,(float)0.,(float).84,(float)1.,(
	    float)0.,(float).83,(float)1.,(float)0.,(float).82,(float)1.,(
	    float)0.,(float).81,(float)1.,(float)0.,(float).8,(float)1.,(
	    float)0.,(float).79,(float)1.,(float)0.,(float).78,(float)1.,(
	    float)0.,(float).77,(float)1.,(float)0.,(float).76,(float)1.,(
	    float)0.,(float).75,(float)1.,(float)0.,(float).74,(float)1.,(
	    float)0.,(float).73,(float)1.,(float)0.,(float).72,(float)1.,(
	    float)0.,(float).71,(float)1.,(float)0.,(float).7,(float)1.,(
	    float)0.,(float).69,(float)1.,(float)0.,(float).68,(float)1.,(
	    float)0.,(float).67,(float)1.,(float)0.,(float).66,(float)1.,(
	    float)0.,(float).65,(float)1.,(float)0.,(float).64,(float)1.,(
	    float)0.,(float).63,(float)1.,(float)0.,(float).62,(float)1.,(
	    float)0.,(float).61,(float)1.,(float)0.,(float).6,(float)1.,(
	    float)0.,(float).59,(float)1.,(float)0.,(float).58,(float)1.,(
	    float)0.,(float).57,(float)1.,(float)0.,(float).56,(float)1.,(
	    float)0.,(float).55,(float)1.,(float)0.,(float).54,(float)1.,(
	    float)0.,(float).53,(float)1.,(float)0.,(float).52,(float)1.,(
	    float)0.,(float).51,(float)1.,(float)0.,(float).5,(float)1.,(
	    float)0.,(float).49,(float)1.,(float)0.,(float).48,(float)1.,(
	    float)0.,(float).47,(float)1.,(float)0.,(float).46,(float)1.,(
	    float)0.,(float).45,(float)1.,(float)0.,(float).44,(float)1.,(
	    float)0.,(float).43,(float)1.,(float)0.,(float).42,(float)1.,(
	    float)0.,(float).41,(float)1.,(float)0.,(float).4,(float)1.,(
	    float)0.,(float).39,(float)1.,(float)0.,(float).38,(float)1.,(
	    float)0.,(float).37,(float)1.,(float)0.,(float).36,(float)1.,(
	    float)0.,(float).35,(float)1.,(float)0.,(float).34,(float)1.,(
	    float)0.,(float).33,(float)1.,(float)0.,(float).32,(float)1.,(
	    float)0.,(float).31,(float)1.,(float)0.,(float).3,(float)1.,(
	    float)0.,(float).29,(float)1.,(float)0.,(float).28,(float)1.,(
	    float)0.,(float).27,(float)1.,(float)0.,(float).26,(float)1.,(
	    float)0.,(float).25,(float)1.,(float)0.,(float).24,(float)1.,(
	    float)0.,(float).23,(float)1.,(float)0.,(float).22,(float)1.,(
	    float)0.,(float).21,(float)1.,(float)0.,(float).2,(float)1.,(
	    float)0.,(float).19,(float)1.,(float)0.,(float).18,(float)1.,(
	    float)0.,(float).17,(float)1.,(float)0.,(float).16,(float)1.,(
	    float)0.,(float).15,(float)1.,(float)0.,(float).14,(float)1.,(
	    float)0.,(float).13,(float)1.,(float)0.,(float).12,(float)1.,(
	    float)0.,(float).11,(float)1.,(float)0.,(float).1,(float)1.,(
	    float)0.,(float).09,(float)1.,(float)0.,(float).08,(float)1.,(
	    float)0.,(float).07,(float)1.,(float)0.,(float).06,(float)1.,(
	    float)0.,(float).05,(float)1.,(float)0.,(float).04,(float)1.,(
	    float)0.,(float).03,(float)1.,(float)0.,(float).02,(float)1.,(
	    float)0.,(float).01,(float)1.,(float)0.,(float)0.,(float).95,(
	    float)0.,(float).05,(float).9,(float)0.,(float).1,(float).85,(
	    float)0.,(float).15,(float).8,(float)0.,(float).2,(float).75,(
	    float)0.,(float).25,(float).7,(float)0.,(float).3,(float).65,(
	    float)0.,(float).35,(float).6,(float)0.,(float).4,(float).55,(
	    float)0.,(float).45,(float).5,(float)0.,(float).5,(float).45,(
	    float)0.,(float).55,(float).4,(float)0.,(float).6,(float).35,(
	    float)0.,(float).65,(float).3,(float)0.,(float).7,(float).25,(
	    float)0.,(float).75,(float).2,(float)0.,(float).8,(float).15,(
	    float)0.,(float).85,(float).1,(float)0.,(float).9,(float).05,(
	    float)0.,(float).95,(float)0.,(float)0.,(float)1.,(float)0.,(
	    float).0467,(float)1.,(float)0.,(float).0933,(float)1.,(float)0.,(
	    float).14,(float)1.,(float)0.,(float).1867,(float)1.,(float)0.,(
	    float).2333,(float)1.,(float)0.,(float).28,(float)1.,(float)0.,(
	    float).3267,(float)1.,(float)0.,(float).3733,(float)1.,(float)0.,(
	    float).42,(float)1.,(float)0.,(float).4667,(float)1.,(float)0.,(
	    float).5133,(float)1.,(float)0.,(float).56,(float)1.,(float)0.,(
	    float).6067,(float)1.,(float)0.,(float).6533,(float)1.,(float)0.,(
	    float).7,(float)1.,(float)0.,(float).7429,(float)1.,(float)0.,(
	    float).7857,(float)1.,(float)0.,(float).8286,(float)1.,(float)0.,(
	    float).8714,(float)1.,(float)0.,(float).9143,(float)1.,(float)0.,(
	    float).9571,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(
	    float)1.,(float)1.,(float).1429,(float).9286,(float).8571,(float)
	    .2857,(float).8571,(float).7143,(float).4286,(float).7857,(float)
	    .5714,(float).5714,(float).7143,(float).4286,(float).7143,(float)
	    .6429,(float).2857,(float).8571,(float).5714,(float).1429,(float)
	    1.,(float).5,(float)0.,(float)1.,(float).5,(float)0.,(float)1.,(
	    float).625,(float).25,(float)1.,(float).75,(float).5,(float)1.,(
	    float).875,(float).75,(float)1.,(float)1.,(float)1. };
    static real eye[3] = { (float)0.,(float)0.,(float)1e3 };
    static real light[3] = { (float)-1.,(float)-1.,(float)-1. };
    static real rgb[9]	/* was [3][3] */ = { (float)0.,(float)0.,(float)1.,(
	    float).35,(float).35,(float).35,(float)1.,(float)1.,(float)1. };

    /* System generated locals */
    integer z_dim1, z_offset, i__1, i__2;

    /* Builtin functions */
    integer s_cmp(), s_wsle(), e_wsle();

    /* Local variables */
    static integer lchr;
    static real shin, dlow;
    static char type__[16];
    extern /* Subroutine */ int pgwindow_();
    static integer ictab;
    static real dhigh;
    static integer iflag, icmin, icmax;
    extern /* Subroutine */ int pgsch_(), pgsci_();
    static real difus;
    extern /* Subroutine */ int euler_();
    static logical lshin;
    extern /* Subroutine */ int pgbox_(), fmxmn_();
    static integer iplot;
    extern /* Subroutine */ int lutin_(), pgslw_(), axes3d_(), sb2srf_();
    static integer ncband;
    static real latice[9]	/* was [3][3] */;
    extern /* Subroutine */ int sbfcls_();
    static integer maxbuf, nclmax;
    static real dofset;
    extern /* Subroutine */ int pgqcol_(), pgqinf_(), sbfint_(), srfcol_(), 
	    colint_(), colsrf_();
    static real polish;
    extern /* Subroutine */ int contor_();
    static integer nclusr;
    extern /* Subroutine */ int logqyn_();
    static logical ovrlay;
    static real lutusr[768]	/* was [3][256] */;
    static integer ncb;
    extern /* Subroutine */ int pglabel_(), pgpaper_(), pgmtext_(), pgvport_()
	    ;

    /* Fortran I/O blocks */
    static cilist io___29 = { 0, 6, 0, 0, 0 };


/*     --------------------------------------------------------------- */


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


/* Purpose */
/*      This subroutine plots "data" defined on a regularly-spaced */
/*   rectangular grid of points Z(I,J). With the default choice for the */
/*   PGCELL routine that is linked, the output is a linearly-interpolated */
/*   map (rather than coarse rectangular boxes). */

/* Parameters */
/*   ARGUMENT  TYPE  I/O  DIMENSION  DESCRIPTION */
/*    Z        R*4    I    N1 x N2   The recatangular "data"-array. */
/*    Z        R*4    O    N1 x N2   A scaled, and clipped, version of */
/*                                   the input array(!). */
/*    N1       I*4    I       -      The first dimension of array Z. */
/*    N2       I*4    I       -      The second dimension of array Z. */
/*    X        R*4    I      NX      Array of X-coordinates. */
/*    NX       I*4    I       -      Number of X-pixels to be plotted */
/*                                  (usually = N1, but must be <= N1). */
/*    Y        R*4    I      NY      Array of Y-coordinates. */
/*    NY       I*4    I       -      Number of Y-pixels to be plotted */
/*                                  (usually = N2, but must be <= N2). */
/*    W        R*4    I       -      Dummy parameter (back-compatibility). */
/*    SIZE     R*4    I       -      Character-size for plot (try 1.5). */
/*    IWIDTH   I*4    I       -      Line-width for plot (try 2). */
/*    XLBL     A*1    I     *(*)     Label for X-axis. */
/*    YLBL     A*1    I     *(*)     Label for Y-axis. */
/*    TITL     A*1    I     *(*)     Title for plot. */

/* Globals */
/*    COLTABS.INC */

/* History */
/*   Initial release.                                    DSS:  3 Jul 1992 */
/*   Minor changes to conform with new PGCELL.           DSS:  6 Feb 1995 */
/*   Put in option to over-lay contours.                 DSS: 21 Feb 1995 */
/*   Now has proper 3-d surface surface rendering.       DSS: 27 Aug 1997 */
/*   Fortran made LINUX-friendly!                        DSS: 15 Sep 1997 */
/*   Choose white background colour for postscript.      DSS:  5 Feb 1999 */
/* ----------------------------------------------------------------------- */

/* ----------------------------------------------------------------------- */
/*  Four colour tables extracted from STARLINK's KAPPA program. */
/* ----------------------------------------------------------------------- */


    /* Parameter adjustments */
    --x;
    --y;
    z_dim1 = *n1;
    z_offset = 1 + z_dim1 * 1;
    z__ -= z_offset;

    /* Function Body */



/* ----------------------------------------------------------------------- */

/*      WRITE(*,*) */
/*      WRITE(*,*)'                (0) Contour' */
/*      WRITE(*,*)'                (1) Surface' */
/*      WRITE(*,*)'                (2) Colour: Grey-Scale' */
/*      WRITE(*,*)'                (3) Colour: Heat' */
/*      WRITE(*,*)'                (4) Colour: Rainbow Spectrum' */
/*      WRITE(*,*)'                (5) Colour: BGYRW' */
/*      WRITE(*,*)'                (6) Colour: Serpent' */
/*      WRITE(*,*)'                (7) Colour: Read in from file' */
/*      WRITE(*,*) */
/* L1: */
    iplot = 1;
/*      WRITE(*,100) */
/* 100  FORMAT(' PLOT>  Type ?  : ',$) */
/*      CALL FORMTQ(STRING,32,NNN) */
/*      IF (NNN.NE.0) READ(STRING,*,ERR=1) IPLOT */
/*      IF (IPLOT.EQ.0) THEN */
/*        CALL CONTOR(Z,NX,NY,X,Y,N1,N2,SIZE,IWIDTH,.FALSE.) */
/*      ELSEIF (IPLOT.EQ.1) THEN */
/*        IF (N1.NE.NX .OR. N2.NE.NY) THEN */
/*          WRITE(*,*)' Sorry folks, SURFACE option needs N1=NX & N2=NY!' */
/*          WRITE(*,*) */
/*          GOTO 1 */
/*        ENDIF */
    srfcol_(rgb, &ncb, &ictab, &difus, &shin, &polish, &lshin, &c__3, &c__7);
    euler_(latice);
    i__1 = *n1 * *n2;
    fmxmn_(&z__[z_offset], &i__1, &dhigh, &dlow, &dofset);
/*        CALL PGBEGIN(0,'?',1,1) */
    pgpaper_(&c_b5, &c_b6);
    pgqcol_(&icmin, &icmax);
/* Computing MIN */
    i__1 = ncband, i__2 = icmax - 16;
    ncband = min(i__1,i__2);
    pgsch_(size);
    pgslw_(iwidth);
    pgvport_(&c_b5, &c_b6, &c_b5, &c_b6);
    pgwindow_(&c_b11, &c_b12, &c_b11, &c_b14);
    pgsci_(&c__0);
    pgbox_("BC", &c_b5, &c__0, "BC", &c_b5, &c__0, (ftnlen)2, (ftnlen)2);
    pgsci_(&c__1);
    pgqinf_("TYPE", type__, &lchr, (ftnlen)4, (ftnlen)16);
    if (s_cmp(type__, "PS", (ftnlen)16, (ftnlen)2) == 0 || s_cmp(type__, 
	    "VPS", (ftnlen)16, (ftnlen)3) == 0 || s_cmp(type__, "CPS", (
	    ftnlen)16, (ftnlen)3) == 0 || s_cmp(type__, "VCPS", (ftnlen)16, (
	    ftnlen)4) == 0) {
	sbfint_(&rgb[6], &c__16, &c__1, &c__1, &maxbuf);
    } else {
	sbfint_(&rgb[3], &c__16, &c__1, &c__1, &maxbuf);
    }
    if (ictab <= 2) {
	colint_(rgb, &c__17, &icmax, &difus, &shin, &polish);
    } else if (ictab == 3) {
	colsrf_(heat, &c__256, &c_b6, &c__17, &icmax, &ncb, &difus, &shin, &
		polish);
    } else if (ictab == 4) {
	colsrf_(&spectrum[3], &c__255, &c_b6, &c__17, &icmax, &ncb, &difus, &
		shin, &polish);
    } else if (ictab == 5) {
	colsrf_(bgyrw, &c__256, &c_b6, &c__17, &icmax, &ncb, &difus, &shin, &
		polish);
    } else if (ictab == 6) {
	colsrf_(serp, &c__256, &c_b6, &c__17, &icmax, &ncb, &difus, &shin, &
		polish);
    } else {
	nclmax = 256;
	lutin_(lutusr, &nclmax, &nclusr, &iflag);
	colsrf_(lutusr, &nclusr, &c_b6, &c__17, &icmax, &ncb, &difus, &shin, &
		polish);
    }
    i__1 = *n1 - 1;
    i__2 = *n2 - 1;
    sb2srf_(eye, latice, &z__[z_offset], &i__1, &i__2, &dlow, &dhigh, &c_b6, &
	    c__17, &icmax, &ncb, light, &lshin);
    axes3d_(eye, latice, &x[1], &x[*n1], &y[1], &y[*n2], xlbl, ylbl, size, &
	    dlow, &dhigh, &dofset, &z__[z_dim1 + 1], &z__[*n1 + z_dim1], &z__[
	    *n1 + *n2 * z_dim1], &z__[*n2 * z_dim1 + 1], xlbl_len, ylbl_len);
    sbfcls_(&c__1);
    pgmtext_("T", &c_b53, &c_b54, &c_b54, titl, (ftnlen)1, titl_len);
/*      ELSEIF (IPLOT.LE.7) THEN */
/*        CALL GREY(Z,NX,NY,X,Y,N1,N2,IPLOT,SIZE,IWIDTH) */
/*      ELSE */
/*        GOTO 1 */
/*      ENDIF */
    if (iplot != 1) {
	pglabel_(xlbl, ylbl, titl, xlbl_len, ylbl_len, titl_len);
    }
    if (iplot > 1) {
	s_wsle(&io___29);
	e_wsle();
	logqyn_(" PLOT> Over-lay contours ?", "N", &ovrlay, (ftnlen)26, (
		ftnlen)1);
	if (ovrlay) {
	    contor_(&z__[z_offset], nx, ny, &x[1], &y[1], n1, n2, size, 
		    iwidth, &ovrlay);
	}
    }
/*      CALL PGEND */
} /* xtal_ */


/* ***<3d-surface>******************************************************** */

/* Subroutine */ int fmxmn_(y, n, ymax, ymin, yofset)
real *y;
integer *n;
real *ymax, *ymin, *yofset;
{
    /* System generated locals */
    integer i__1;
    real r__1, r__2;

    /* Local variables */
    static real ymin1, ymax1;
    static integer i__, nnn;

/*     -------------------------------------- */


    /* Parameter adjustments */
    --y;

    /* Function Body */
    nnn = 0;
    ymin1 = (float)1e25;
    ymax1 = (float)-1e25;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* Computing MIN */
	r__1 = ymin1, r__2 = y[i__];
	ymin1 = dmin(r__1,r__2);
/* Computing MAX */
	r__1 = ymax1, r__2 = y[i__];
	ymax1 = dmax(r__1,r__2);
/* L10: */
    }
L1:
    *ymin = ymin1;
    *ymax = ymax1;
/*      WRITE(*,100) YMIN,YMAX */
/* L100: */
/*      CALL FORMTQ(STRING,32,NNN) */
/*      IF (NNN.NE.0) READ(STRING,*,ERR=1) YMIN,YMAX */
    if (*ymax <= *ymin) {
	goto L1;
    }
/* Computing MAX */
    r__1 = -(*ymin);
    *yofset = dmax(r__1,(float)0.);
    if (*yofset > (float)0.) {
	*ymin += *yofset;
	*ymax += *yofset;
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
/* L20: */
	    y[i__] += *yofset;
	}
    }
} /* fmxmn_ */


/* Subroutine */ int euler_(latice)
real *latice;
{
    /* Initialized data */

    static real pirad = (float).01745329252;

    /* Builtin functions */
    double sin(), cos();

    /* Local variables */
    static real cosa, cosb, sina, sinb;
    extern /* Subroutine */ int rotx_(), roty_();
    static real u, v, w, z1, z2, z3;
    static integer ia, ib, nnn;

/*     ------------------------ */

    /* Parameter adjustments */
    latice -= 4;

    /* Function Body */

    nnn = 0;
/*   1  WRITE(*,100) */
/* L100: */
/*      CALL FORMTQ(STRING,32,NNN) */
    if (nnn == 0) {
	sina = (float)-.7071067814;
	cosa = (float).7071067814;
	sinb = (float)-.5;
	cosb = (float).866025404;
    } else {
/*        READ(STRING,*,ERR=1) IA,IB */
	sina = -sin((real) ia * pirad);
	cosa = cos((real) ia * pirad);
	sinb = -sin((real) ib * pirad);
	cosb = cos((real) ib * pirad);
    }
    roty_(&c_b67, &c_b67, &c_b54, &u, &v, &w, &sina, &cosa);
    rotx_(&u, &v, &w, &latice[4], &latice[5], &z1, &sinb, &cosb);
    latice[6] = z1 - (float)1.;
    roty_(&c_b54, &c_b67, &c_b54, &u, &v, &w, &sina, &cosa);
    rotx_(&u, &v, &w, &latice[7], &latice[8], &z2, &sinb, &cosb);
    latice[9] = z2 - (float)1.;
    roty_(&c_b67, &c_b67, &c_b67, &u, &v, &w, &sina, &cosa);
    rotx_(&u, &v, &w, &latice[10], &latice[11], &z3, &sinb, &cosb);
    latice[12] = z3 - (float)1.;
} /* euler_ */


/* Subroutine */ int rotx_(x, y, z__, u, v, w, s, c__)
real *x, *y, *z__, *u, *v, *w, *s, *c__;
{
/*     -------------------------------- */

    *u = *x;
    *v = *y * *c__ + *z__ * *s;
    *w = -(*y) * *s + *z__ * *c__;
} /* rotx_ */


/* Subroutine */ int roty_(x, y, z__, u, v, w, s, c__)
real *x, *y, *z__, *u, *v, *w, *s, *c__;
{
/*     -------------------------------- */

    *u = *x * *c__ - *z__ * *s;
    *v = *y;
    *w = *x * *s + *z__ * *c__;
} /* roty_ */


/* Subroutine */ int srfcol_(rgb, ncband, ictab, dif, shin, polish, lshin, 
	ic1, ic2)
real *rgb;
integer *ncband, *ictab;
real *dif, *shin, *polish;
logical *lshin;
integer *ic1, *ic2;
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    integer s_rsli(), do_lio(), e_rsli();

    /* Local variables */
    static integer i__;
    static real difuse;
    static char string[32];
    static integer nnn;

    /* Fortran I/O blocks */
    static icilist io___51 = { 1, string, 0, 0, 32, 1 };
    static icilist io___52 = { 1, string, 0, 0, 32, 1 };
    static icilist io___54 = { 1, string, 0, 0, 32, 1 };


/*     ----------------------------------------------------------------- */


    /* Parameter adjustments */
    --rgb;

    /* Function Body */
    *polish = (float)1.;
    nnn = 0;
L1:
    *ictab = 0;
/*      WRITE(*,100) IC1,IC2,ICTAB */
/* L100: */
/*      CALL FORMTQ(STRING,32,NNN) */
    if (nnn != 0) {
	i__1 = s_rsli(&io___51);
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = do_lio(&c__3, &c__1, (char *)&(*ictab), (ftnlen)sizeof(integer)
		);
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = e_rsli();
	if (i__1 != 0) {
	    goto L1;
	}
    }
    if (*ictab > *ic2) {
	goto L1;
    } else if (*ictab < *ic1) {
	*ncband = 1;
L2:
	rgb[1] = (float)0.;
	rgb[2] = (float)1.;
	rgb[3] = (float)0.;
/*        WRITE(*,110) (RGB(I),I=1,3) */
/* L110: */
/*        CALL FORMTQ(STRING,32,NNN) */
	if (nnn != 0) {
	    i__1 = s_rsli(&io___52);
	    if (i__1 != 0) {
		goto L2;
	    }
	    for (i__ = 1; i__ <= 3; ++i__) {
		i__1 = do_lio(&c__4, &c__1, (char *)&rgb[i__], (ftnlen)sizeof(
			real));
		if (i__1 != 0) {
		    goto L2;
		}
	    }
	    i__1 = e_rsli();
	    if (i__1 != 0) {
		goto L2;
	    }
	}
	if (rgb[1] + rgb[2] + rgb[3] <= (float).05) {
	    goto L2;
	}
    } else {
L3:
	*ncband = 8;
/*        WRITE(*,120) NCBAND */
/* L120: */
/*        CALL FORMTQ(STRING,32,NNN) */
	if (nnn != 0) {
	    i__1 = s_rsli(&io___54);
	    if (i__1 != 0) {
		goto L3;
	    }
	    i__1 = do_lio(&c__3, &c__1, (char *)&(*ncband), (ftnlen)sizeof(
		    integer));
	    if (i__1 != 0) {
		goto L3;
	    }
	    i__1 = e_rsli();
	    if (i__1 != 0) {
		goto L3;
	    }
	}
/* Computing MAX */
	i__1 = min(*ncband,64);
	*ncband = max(i__1,1);
    }
    *lshin = FALSE_;
/*      CALL LOGQYN(' Surface> A shiny gloss ?','N',LSHIN) */
    if (*lshin) {
	*shin = (float)1.;
	*dif = (float)0.;
    } else {
	*shin = (float)0.;
/* L4: */
	*dif = (float).7;
/*        WRITE(*,130) DIF */
/* 130    FORMAT(' Surface> Diffusiveness ?  (def=',F3.1,') : ',$) */
/*        CALL FORMTQ(STRING,32,NNN) */
/*        IF (NNN.NE.0) READ(STRING,*,ERR=4) DIF */
/* Computing MAX */
	r__1 = dmin(*dif,(float)1.);
	difuse = dmax(r__1,(float).1);
    }
} /* srfcol_ */


/* Subroutine */ int axes3d_(eye, latice, xmin, xmax, ymin, ymax, xlbl, ylbl, 
	size, dlow, dhigh, dofset, d00, dx0, dxy, d0y, xlbl_len, ylbl_len)
real *eye, *latice, *xmin, *xmax, *ymin, *ymax;
char *xlbl, *ylbl;
real *size, *dlow, *dhigh, *dofset, *d00, *dx0, *dxy, *d0y;
ftnlen xlbl_len;
ftnlen ylbl_len;
{
    /* System generated locals */
    real r__1, r__2;

    /* Local variables */
    static real scla, xscl, yscl, pivx[3], pivy[3], fracz, xsign, ysign;
    extern /* Subroutine */ int vcopy_();
    static real zxmin, latcab[12]	/* was [3][4] */, ax, ay, az, bx, by, 
	    bz, cx, cy, cz;
    extern /* Subroutine */ int sbline_();
    static real zscale;
    extern /* Subroutine */ int sbtext_(), axnums_(), aznums_();
    static real orx[6]	/* was [3][2] */, ory[6]	/* was [3][2] */;

/*     ---------------------------------------------------------------- */


    /* Parameter adjustments */
    latice -= 4;
    --eye;

    /* Function Body */
    if (*xmax <= *xmin || *ymax <= *ymin) {
	return 0;
    }
    scla = *size * (float).15;
    ax = latice[7] - latice[4];
    ay = latice[8] - latice[5];
    az = latice[9] - latice[6];
    bx = latice[10] - latice[4];
    by = latice[11] - latice[5];
    bz = latice[12] - latice[6];
    cx = ay * bz - by * az;
    cy = az * bx - bz * ax;
    cz = ax * by - bx * ay;
    xsign = (float)1.;
    xscl = -scla;
    if (cy * bz > (float)0.) {
	xsign = (float)-1.;
	xscl = scla + (float)1.;
    }
    orx[0] = xsign * ax;
    orx[1] = xsign * ay;
    orx[2] = xsign * az;
    orx[3] = xsign * bx;
    orx[4] = xsign * by;
    orx[5] = xsign * bz;
    pivx[0] = (latice[4] + latice[7]) * (float).5 + xscl * bx;
    pivx[1] = (latice[5] + latice[8]) * (float).5 + xscl * by;
    pivx[2] = (latice[6] + latice[9]) * (float).5 + xscl * bz;
    r__1 = scla * (float).2;
    sbtext_(&eye[1], xlbl, &c__1, pivx, &c_b54, orx, &r__1, xlbl_len);
    axnums_(&eye[1], xmin, xmax, pivx, orx, &scla, &xsign);
    ysign = (float)-1.;
    yscl = -scla;
    if (cy * az > (float)0.) {
	ysign = (float)1.;
	yscl = scla + (float)1.;
    }
    ory[0] = ysign * bx;
    ory[1] = ysign * by;
    ory[2] = ysign * bz;
    ory[3] = -ysign * ax;
    ory[4] = -ysign * ay;
    ory[5] = -ysign * az;
    pivy[0] = (latice[4] + latice[10]) * (float).5 + yscl * ax;
    pivy[1] = (latice[5] + latice[11]) * (float).5 + yscl * ay;
    pivy[2] = (latice[6] + latice[12]) * (float).5 + yscl * az;
    r__1 = scla * (float).2;
    sbtext_(&eye[1], ylbl, &c__1, pivy, &c_b54, ory, &r__1, ylbl_len);
    axnums_(&eye[1], ymin, ymax, pivy, ory, &scla, &ysign);
    latcab[0] = latice[7] + bx;
    latcab[1] = latice[8] + by;
    latcab[2] = latice[9] + bz;
    sbline_(&eye[1], &latice[4], &latice[7], &c__1, &c_false);
    sbline_(&eye[1], &latice[7], latcab, &c__1, &c_false);
    sbline_(&eye[1], latcab, &latice[10], &c__1, &c_false);
    sbline_(&eye[1], &latice[10], &latice[4], &c__1, &c_false);
/* Computing MAX */
    r__1 = *dhigh - *dlow;
    zscale = (float)1. / dmax(r__1,(float)1e-20);
/* Computing MAX */
    r__1 = (*d00 - *dlow) * zscale;
    fracz = dmax(r__1,(float)0.);
    latcab[3] = latice[4] + fracz * cx;
    latcab[4] = latice[5] + fracz * cy;
    latcab[5] = latice[6] + fracz * cz;
    sbline_(&eye[1], &latice[4], &latcab[3], &c__1, &c_false);
    vcopy_(&latice[4], &latcab[6], &c__3);
    zxmin = latcab[6];
/* Computing MAX */
    r__1 = (*dx0 - *dlow) * zscale;
    fracz = dmax(r__1,(float)0.);
    latcab[3] = latice[7] + fracz * cx;
    latcab[4] = latice[8] + fracz * cy;
    latcab[5] = latice[9] + fracz * cz;
    sbline_(&eye[1], &latice[7], &latcab[3], &c__1, &c_false);
    if (latice[7] > zxmin) {
	vcopy_(&latice[7], &latcab[6], &c__3);
	zxmin = latcab[6];
    }
/* Computing MAX */
    r__1 = (*dxy - *dlow) * zscale;
    fracz = dmax(r__1,(float)0.);
    latcab[3] = latcab[0] + fracz * cx;
    latcab[4] = latcab[1] + fracz * cy;
    latcab[5] = latcab[2] + fracz * cz;
    if (latcab[0] > zxmin) {
	vcopy_(latcab, &latcab[6], &c__3);
	zxmin = latcab[6];
    }
    sbline_(&eye[1], latcab, &latcab[3], &c__1, &c_false);
/* Computing MAX */
    r__1 = (*d0y - *dlow) * zscale;
    fracz = dmax(r__1,(float)0.);
    latcab[3] = latice[10] + fracz * cx;
    latcab[4] = latice[11] + fracz * cy;
    latcab[5] = latice[12] + fracz * cz;
    sbline_(&eye[1], &latice[10], &latcab[3], &c__1, &c_false);
    if (latice[10] > zxmin) {
	vcopy_(&latice[10], &latcab[6], &c__3);
	zxmin = latcab[6];
    }
    latcab[9] = latcab[6] + cx;
    latcab[10] = latcab[7] + cy;
    latcab[11] = latcab[8] + cz;
    sbline_(&eye[1], &latcab[6], &latcab[9], &c__1, &c_false);
    r__1 = *dlow - *dofset;
    r__2 = *dhigh - *dofset;
    aznums_(&eye[1], &r__1, &r__2, &latcab[6], &scla);
} /* axes3d_ */


/* Subroutine */ int axnums_(eye, xmin, xmax, pivx, orx, scla, xsign)
real *eye, *xmin, *xmax, *pivx, *orx, *scla, *xsign;
{
    /* Initialized data */

    static real frtick = (float).02;
    static real frnum = (float).1;

    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    double r_lg10(), pow_ri();
    integer i_nint();

    /* Local variables */
    static char nlbl[20];
    static integer nsub, i__, j;
    static real x;
    extern doublereal pgrnd_();
    static real pivot[3];
    static integer nc;
    static real dx, xf, xh, xj, xn, xr;
    extern /* Subroutine */ int sbline_(), pgnumb_();
    static integer imants, ipower;
    extern /* Subroutine */ int sbtext_();
    static real end1[3], end2[3];

/*     ---------------------------------------------------- */

    /* Parameter adjustments */
    orx -= 4;
    --pivx;
    --eye;

    /* Function Body */

    r__1 = *xmax - *xmin;
    xr = pgrnd_(&r__1, &nsub);
    dx = xr / (real) nsub;
    if (dx <= (float)1e-20) {
	return 0;
    }
L1:
    xj = dx * (real) ((integer) (*xmin / dx) + 1);
    if (xj + dx >= *xmax) {
	dx /= (float)2.;
	nsub <<= 1;
	goto L1;
    }
    if (*xmin < (float)0.) {
	xj -= dx;
    }
    xn = *xsign / (*xmax - *xmin);
    xh = (*xmin + *xmax) * (float).5;
    i__1 = nsub;
    for (j = 1; j <= i__1; ++j) {
	if (xj > *xmax) {
	    return 0;
	}
	xf = xn * (xj - xh);
	for (i__ = 1; i__ <= 3; ++i__) {
	    end1[i__ - 1] = pivx[i__] + xf * orx[i__ + 3] + *scla * orx[i__ + 
		    6];
	    end2[i__ - 1] = end1[i__ - 1] - frtick * orx[i__ + 6];
	    pivot[i__ - 1] = end1[i__ - 1] - frnum * orx[i__ + 6];
/* L10: */
	}
	r__1 = dabs(xj) + (float)1e-10;
	ipower = (integer) r_lg10(&r__1) - 5;
	if (xj < (float)1.) {
	    --ipower;
	}
	x = xj / pow_ri(&c_b127, &ipower);
	imants = i_nint(&x);
	pgnumb_(&imants, &ipower, &c__0, nlbl, &nc, (ftnlen)20);
	sbline_(&eye[1], end1, end2, &c__1, &c_false);
	r__1 = *scla * (float).15;
	sbtext_(&eye[1], nlbl, &c__1, pivot, &c_b54, &orx[4], &r__1, (ftnlen)
		20);
	xj += dx;
/* L20: */
    }
} /* axnums_ */


/* Subroutine */ int aznums_(eye, zmin, zmax, latz, scla)
real *eye, *zmin, *zmax, *latz, *scla;
{
    /* Initialized data */

    static real frtick = (float).05;
    static real frnum = (float).1;

    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    double r_lg10(), pow_ri();
    integer i_nint();

    /* Local variables */
    static char nlbl[20];
    static integer nsub, i__, j;
    static real z__;
    extern doublereal pgrnd_();
    static real zsign, pivot[3];
    static integer nc;
    static real dz, zf, zj, zn, zr;
    extern /* Subroutine */ int sbline_(), pgnumb_();
    static integer imants;
    static real orient[9]	/* was [3][3] */;
    static integer ipower;
    extern /* Subroutine */ int sbtext_();
    static real end1[3], end2[3];

/*     ------------------------------------------ */

    /* Parameter adjustments */
    latz -= 4;
    --eye;

    /* Function Body */

    orient[0] = *scla;
    orient[1] = (float)0.;
    orient[2] = (float)0.;
    zsign = (float)1.;
    if (latz[8] - latz[5] < (float)0.) {
	zsign = (float)-1.;
    }
    for (i__ = 1; i__ <= 3; ++i__) {
	orient[i__ + 5] = latz[i__ + 6] - latz[i__ + 3];
	orient[i__ + 2] = zsign * *scla * orient[i__ + 5];
/* L10: */
    }
    r__1 = *zmax - *zmin;
    zr = pgrnd_(&r__1, &nsub);
    dz = zr / (real) nsub;
    if (dz <= (float)1e-20) {
	return 0;
    }
L1:
    zj = dz * (real) ((integer) (*zmin / dz) + 1);
    if (zj + dz >= *zmax) {
	dz /= (float)2.;
	nsub <<= 1;
	goto L1;
    }
    if (*zmin <= (float)0.) {
	zj -= dz;
    }
    zn = (float)1. / (*zmax - *zmin);
    i__1 = nsub;
    for (j = 1; j <= i__1; ++j) {
	if (zj > *zmax) {
	    return 0;
	}
	zf = zn * (zj - *zmin);
	for (i__ = 1; i__ <= 3; ++i__) {
	    end1[i__ - 1] = latz[i__ + 3] + zf * orient[i__ + 5];
	    end2[i__ - 1] = end1[i__ - 1] + frtick * orient[i__ - 1];
	    pivot[i__ - 1] = end1[i__ - 1] + frnum * orient[i__ - 1] - frtick 
		    * orient[i__ + 2];
/* L20: */
	}
	r__1 = dabs(zj) + (float)1e-10;
	ipower = (integer) r_lg10(&r__1) - 5;
	if (zj < (float)1.) {
	    --ipower;
	}
	z__ = zj / pow_ri(&c_b127, &ipower);
	imants = i_nint(&z__);
	pgnumb_(&imants, &ipower, &c__0, nlbl, &nc, (ftnlen)20);
	sbline_(&eye[1], end1, end2, &c__1, &c_false);
	r__1 = *scla * (float).12;
	sbtext_(&eye[1], nlbl, &c__1, pivot, &c_b5, orient, &r__1, (ftnlen)20)
		;
	zj += dz;
/* L30: */
    }
} /* aznums_ */


/* ***<contour>*********************************************************** */

/* Subroutine */ int contor_(map, nx, ny, x, y, n1, n2, size, iwidth, ovrlay)
real *map;
integer *nx, *ny;
real *x, *y;
integer *n1, *n2;
real *size;
integer *iwidth;
logical *ovrlay;
{
    /* Initialized data */

    static integer lcolor[2] = { 1,3 };
    static integer lstyle[2] = { 1,1 };
    static integer lwidth[2] = { 1,2 };

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static real dmin__, dmax__, cont[50]	/* was [25][2] */;
    extern /* Subroutine */ int init_();
    static integer i__;
    extern /* Subroutine */ int pgsch_(), pgsci_(), pgenv_();
    static integer ncont[2], lwdth;
    extern /* Subroutine */ int pgsls_(), pgslw_();
    static real tr[6];
    extern /* Subroutine */ int askcnt_(), pgcont_(), autcnt_(), pgpaper_();

/*     --------------------------------------------------------- */

    /* Parameter adjustments */
    --y;
    --x;
    --map;

    /* Function Body */

    i__1 = *n1 * *n2;
    init_(&x[1], &y[1], nx, ny, tr, &map[1], &dmin__, &dmax__, &i__1);
    if (*ovrlay) {
	autcnt_(ncont, cont, &dmin__, &dmax__, ovrlay);
	pgslw_(&c__1);
	pgcont_(&map[1], n1, n2, &c__1, nx, &c__1, ny, cont, ncont, tr);
	pgslw_(iwidth);
	return 0;
    }
    askcnt_(ncont, cont, &dmin__, &dmax__);
/*      CALL PGBEGIN(0,'?',1,1) */
    pgpaper_(&c_b5, &c_b6);
    pgsch_(size);
    pgslw_(iwidth);
    pgenv_(&x[1], &x[*nx], &y[1], &y[*ny], &c__0, &c__0);
    for (i__ = 1; i__ <= 2; ++i__) {
	pgsci_(&lcolor[i__ - 1]);
	pgsls_(&lstyle[i__ - 1]);
	lwdth = lwidth[i__ - 1] << 1;
	if (lwdth > 7) {
	    lwdth = 7;
	}
	pgslw_(&lwdth);
	pgcont_(&map[1], n1, n2, &c__1, nx, &c__1, ny, &cont[i__ * 25 - 25], &
		ncont[i__ - 1], tr);
/* L20: */
    }
    pgsci_(&c__1);
    pgslw_(iwidth);
} /* contor_ */


/* Subroutine */ int init_(x, y, nx, ny, tr, map, dmin__, dmax__, nmap)
real *x, *y;
integer *nx, *ny;
real *tr, *map, *dmin__, *dmax__;
integer *nmap;
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(), e_wsle(), do_lio();

    /* Local variables */
    static real dtot, f;
    static integer i__;

    /* Fortran I/O blocks */
    static cilist io___132 = { 0, 6, 0, 0, 0 };
    static cilist io___133 = { 0, 6, 0, 0, 0 };
    static cilist io___134 = { 0, 6, 0, 0, 0 };
    static cilist io___135 = { 0, 6, 0, 0, 0 };
    static cilist io___136 = { 0, 6, 0, 0, 0 };


/*     ------------------------------------------------ */


    /* Parameter adjustments */
    --map;
    --tr;
    --y;
    --x;

    /* Function Body */
    tr[1] = x[1] - (x[*nx] - x[1]) / (real) (*nx - 1);
    tr[2] = (x[*nx] - x[1]) / (real) (*nx - 1);
    tr[3] = (float)0.;
    tr[4] = y[1] - (y[*ny] - y[1]) / (real) (*ny - 1);
    tr[5] = (float)0.;
    tr[6] = (y[*ny] - y[1]) / (real) (*ny - 1);
    *dmin__ = (float)1e20;
    *dmax__ = (float)-1e20;
    dtot = (float)0.;
    i__1 = *nmap;
    for (i__ = 1; i__ <= i__1; ++i__) {
	f = map[i__];
	dtot += f;
	if (f > *dmax__) {
	    *dmax__ = f;
	}
	if (f < *dmin__) {
	    *dmin__ = f;
	}
/* L10: */
    }
    s_wsle(&io___132);
    e_wsle();
    s_wsle(&io___133);
    do_lio(&c__9, &c__1, " Minimum value = ", (ftnlen)17);
    do_lio(&c__4, &c__1, (char *)&(*dmin__), (ftnlen)sizeof(real));
    e_wsle();
    s_wsle(&io___134);
    do_lio(&c__9, &c__1, " Maximum value = ", (ftnlen)17);
    do_lio(&c__4, &c__1, (char *)&(*dmax__), (ftnlen)sizeof(real));
    e_wsle();
    s_wsle(&io___135);
    do_lio(&c__9, &c__1, " Total flux    = ", (ftnlen)17);
    do_lio(&c__4, &c__1, (char *)&dtot, (ftnlen)sizeof(real));
    e_wsle();
    s_wsle(&io___136);
    e_wsle();
} /* init_ */


/* Subroutine */ int askcnt_(ncont, cont, dmin__, dmax__)
integer *ncont;
real *cont, *dmin__, *dmax__;
{
    /* Format strings */
    static char fmt_100[] = "(\002 Thin>  \002,$)";
    static char fmt_200[] = "(a)";
    static char fmt_110[] = "(\002 Thick>  \002,$)";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsle(), e_wsle(), do_lio(), s_wsfe(), e_wsfe(), s_rsfe(), 
	    do_fio(), e_rsfe(), s_rsli(), e_rsli();

    /* Local variables */
    static logical auto__;
    static integer i__;
    extern /* Subroutine */ int findnc_(), autcnt_();
    static char string[132];
    extern /* Subroutine */ int logqyn_();

    /* Fortran I/O blocks */
    static cilist io___137 = { 0, 6, 0, 0, 0 };
    static cilist io___139 = { 0, 6, 0, 0, 0 };
    static cilist io___140 = { 0, 6, 0, 0, 0 };
    static cilist io___141 = { 0, 6, 0, 0, 0 };
    static cilist io___142 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___143 = { 1, 5, 0, fmt_200, 0 };
    static icilist io___145 = { 1, string, 0, 0, 132, 1 };
    static cilist io___147 = { 0, 6, 0, 0, 0 };
    static cilist io___148 = { 0, 6, 0, fmt_110, 0 };
    static cilist io___149 = { 1, 5, 0, fmt_200, 0 };
    static icilist io___150 = { 1, string, 0, 0, 132, 1 };


/*     --------------------------------------- */


    /* Parameter adjustments */
    cont -= 26;
    --ncont;

    /* Function Body */
    s_wsle(&io___137);
    e_wsle();
    logqyn_(" Contours>  Autoscale (linear) ?", "Y", &auto__, (ftnlen)32, (
	    ftnlen)1);
    if (auto__) {
	autcnt_(&ncont[1], &cont[26], dmin__, dmax__, &c_false);
	return 0;
    }
    s_wsle(&io___139);
    e_wsle();
    s_wsle(&io___140);
    do_lio(&c__9, &c__1, "          ***** Contour Values  *****", (ftnlen)37);
    e_wsle();
    s_wsle(&io___141);
    e_wsle();
L1:
    s_wsfe(&io___142);
    e_wsfe();
    i__1 = s_rsfe(&io___143);
    if (i__1 != 0) {
	goto L1;
    }
    i__1 = do_fio(&c__1, string, (ftnlen)132);
    if (i__1 != 0) {
	goto L1;
    }
    i__1 = e_rsfe();
    if (i__1 != 0) {
	goto L1;
    }
    findnc_(string, &c__132, &ncont[1], (ftnlen)132);
    i__1 = s_rsli(&io___145);
    if (i__1 != 0) {
	goto L1;
    }
    i__2 = ncont[1];
    for (i__ = 1; i__ <= i__2; ++i__) {
	i__1 = do_lio(&c__4, &c__1, (char *)&cont[i__ + 25], (ftnlen)sizeof(
		real));
	if (i__1 != 0) {
	    goto L1;
	}
    }
    i__1 = e_rsli();
    if (i__1 != 0) {
	goto L1;
    }
    s_wsle(&io___147);
    e_wsle();
L2:
    s_wsfe(&io___148);
    e_wsfe();
    i__1 = s_rsfe(&io___149);
    if (i__1 != 0) {
	goto L2;
    }
    i__1 = do_fio(&c__1, string, (ftnlen)132);
    if (i__1 != 0) {
	goto L2;
    }
    i__1 = e_rsfe();
    if (i__1 != 0) {
	goto L2;
    }
    findnc_(string, &c__132, &ncont[2], (ftnlen)132);
    i__1 = s_rsli(&io___150);
    if (i__1 != 0) {
	goto L2;
    }
    i__2 = ncont[2];
    for (i__ = 1; i__ <= i__2; ++i__) {
	i__1 = do_lio(&c__4, &c__1, (char *)&cont[i__ + 50], (ftnlen)sizeof(
		real));
	if (i__1 != 0) {
	    goto L2;
	}
    }
    i__1 = e_rsli();
    if (i__1 != 0) {
	goto L2;
    }
} /* askcnt_ */


/* Subroutine */ int findnc_(string, nchars, nc, string_len)
char *string;
integer *nchars, *nc;
ftnlen string_len;
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer imin, imax, i__, j;

/*     ----------------------------------- */


    /* Parameter adjustments */
    --string;

    /* Function Body */
    i__1 = *nchars;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* L10: */
	if (*(unsigned char *)&string[i__] != ' ') {
	    goto L1;
	}
    }
L1:
    imin = i__;
    for (i__ = *nchars; i__ >= 1; --i__) {
/* L20: */
	if (*(unsigned char *)&string[i__] != ' ') {
	    goto L2;
	}
    }
L2:
    imax = i__;
    if (imin <= imax) {
	*nc = 1;
	j = imin;
	i__1 = imax;
	for (i__ = imin; i__ <= i__1; ++i__) {
	    if (j > imax) {
		return 0;
	    }
	    if (*(unsigned char *)&string[j] == ' ' || *(unsigned char *)&
		    string[j] == ',') {
		++(*nc);
L3:
		if (*(unsigned char *)&string[j + 1] == ' ' || *(unsigned 
			char *)&string[j + 1] == ',') {
		    ++j;
		    goto L3;
		}
	    }
	    ++j;
/* L30: */
	}
    } else {
	*nc = 0;
    }
} /* findnc_ */


/* Subroutine */ int autcnt_(ncont, cont, dmin__, dmax__, ovrlay)
integer *ncont;
real *cont, *dmin__, *dmax__;
logical *ovrlay;
{
    /* Format strings */
    static char fmt_100[] = "(\002 Autoscale> Range ? (def=\002,1pe10.3,\002\
 to \002,e10.3,\002)  : \002,$)";
    static char fmt_110[] = "(\002 Autoscale>  No. of contours ?  (def=\002,\
i2,\002) : \002,$)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(), e_wsle(), s_wsfe(), do_fio(), e_wsfe(), s_rsli(), 
	    do_lio(), e_rsli();

    /* Local variables */
    static real xinc, dmin1, dmax1;
    static integer i__, n;
    static real x;
    static char string[32];
    extern /* Subroutine */ int formtq_();
    static integer nnn;

    /* Fortran I/O blocks */
    static cilist io___155 = { 0, 6, 0, 0, 0 };
    static cilist io___156 = { 0, 6, 0, fmt_100, 0 };
    static icilist io___159 = { 1, string, 0, 0, 32, 1 };
    static cilist io___163 = { 0, 6, 0, fmt_110, 0 };
    static icilist io___164 = { 1, string, 0, 0, 32, 1 };


/*     ---------------------------------------------- */


    /* Parameter adjustments */
    cont -= 26;
    --ncont;

    /* Function Body */
    s_wsle(&io___155);
    e_wsle();
L1:
    s_wsfe(&io___156);
    do_fio(&c__1, (char *)&(*dmin__), (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&(*dmax__), (ftnlen)sizeof(real));
    e_wsfe();
    formtq_(string, &c__32, &nnn, (ftnlen)32);
    if (nnn != 0) {
	i__1 = s_rsli(&io___159);
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = do_lio(&c__4, &c__1, (char *)&dmin1, (ftnlen)sizeof(real));
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = do_lio(&c__4, &c__1, (char *)&dmax1, (ftnlen)sizeof(real));
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = e_rsli();
	if (i__1 != 0) {
	    goto L1;
	}
    }
    if (nnn != 0) {
	*dmin__ = dmin1;
	*dmax__ = dmax1;
    }
L2:
    n = 10;
    s_wsfe(&io___163);
    do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
    e_wsfe();
    formtq_(string, &c__32, &nnn, (ftnlen)32);
    if (nnn != 0) {
	i__1 = s_rsli(&io___164);
	if (i__1 != 0) {
	    goto L2;
	}
	i__1 = do_lio(&c__3, &c__1, (char *)&n, (ftnlen)sizeof(integer));
	if (i__1 != 0) {
	    goto L2;
	}
	i__1 = e_rsli();
	if (i__1 != 0) {
	    goto L2;
	}
    }
/* Computing MAX */
    i__1 = min(n,50);
    n = max(i__1,2);
    ncont[1] = (n + 1) / 2;
    ncont[2] = n - ncont[1];
    if (*ovrlay) {
	if (n > 25) {
	    n = 25;
	}
	ncont[1] = n;
	ncont[2] = 0;
    }
    xinc = (*dmax__ - *dmin__) / (real) n;
    x = *dmin__ + xinc * (float).5;
    i__1 = ncont[1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	cont[i__ + 25] = x;
	x += xinc;
/* L10: */
    }
    i__1 = ncont[2];
    for (i__ = 1; i__ <= i__1; ++i__) {
	cont[i__ + 50] = x;
	x += xinc;
/* L20: */
    }
} /* autcnt_ */


/* ***<grey scale>******************************************************** */

/* Subroutine */ int grey_(map, nx, ny, x, y, n1, n2, iplot, size, iwidth)
real *map;
integer *nx, *ny;
real *x, *y;
integer *n1, *n2, *iplot;
real *size;
integer *iwidth;
{
    /* Initialized data */

    static real trcol[6] = { (float)0.,(float)1.,(float)0.,(float)0.,(float)
	    0.,(float)1. };

    /* System generated locals */
    integer i__1;
    real r__1;

    /* Local variables */
    static real dmin__, dmax__;
    extern /* Subroutine */ int init_();
    static real xmin, ymin, xmax, ymax;
    extern /* Subroutine */ int pgwindow_(), pgsch_();
    static integer ncols;
    extern /* Subroutine */ int pgbox_(), lutin_(), pgslw_();
    static real bl[256], gr[256], colbar[512]	/* was [2][256] */, tr[6];
    extern /* Subroutine */ int pgcell_(), setcol_();
    static integer nclusr;
    extern /* Subroutine */ int stgrey_();
    static real lutusr[768]	/* was [3][256] */, red[256];
    extern /* Subroutine */ int pgpaper_(), pgvport_();

/*     ------------------------------------------------------ */

    /* Parameter adjustments */
    --y;
    --x;
    --map;

    /* Function Body */

    ncols = 256;
    i__1 = *n1 * *n2;
    init_(&x[1], &y[1], nx, ny, tr, &map[1], &dmin__, &dmax__, &i__1);
    i__1 = *n1 * *n2;
    stgrey_(&map[1], &i__1, &dmin__, &dmax__, colbar, &ncols);
    trcol[5] = (dmax__ - dmin__) / (float)255.;
    trcol[3] = dmin__ - trcol[5];
    if (*iplot == 7) {
	lutin_(lutusr, &ncols, &nclusr, iplot);
    }
/*      CALL PGBEGIN(0,'?',1,1) */
    pgpaper_(&c_b5, &c_b6);
    pgsch_(size);
    pgslw_(iwidth);
    pgvport_(&c_b234, &c_b235, &c_b236, &c_b235);
    pgwindow_(&c_b6, &c_b239, &dmin__, &dmax__);
    setcol_(iplot, &ncols, lutusr, red, gr, bl);
    r__1 = *size * (float).5;
    pgsch_(&r__1);
    pgbox_("B", &c_b5, &c__0, "B", &c_b5, &c__0, (ftnlen)1, (ftnlen)1);
    pgbox_("C", &c_b5, &c__0, "CMTSVI", &c_b5, &c__0, (ftnlen)1, (ftnlen)6);
    pgcell_(colbar, &c__2, &c__256, &c__1, &c__2, &c__1, &c__256, &c_b6, &
	    c_b5, trcol, &ncols, red, gr, bl);
    pgbox_("B", &c_b5, &c__0, "B", &c_b5, &c__0, (ftnlen)1, (ftnlen)1);
    pgbox_("C", &c_b5, &c__0, "CMTSVI", &c_b5, &c__0, (ftnlen)1, (ftnlen)6);
    pgsch_(size);
    pgvport_(&c_b272, &c_b273, &c_b236, &c_b235);
    xmin = tr[0] + tr[1];
    xmax = tr[0] + (real) (*nx) * tr[1];
    ymin = tr[3] + tr[5];
    ymax = tr[3] + (real) (*ny) * tr[5];
    pgwindow_(&xmin, &xmax, &ymin, &ymax);
    pgbox_("C", &c_b5, &c__0, "C", &c_b5, &c__0, (ftnlen)1, (ftnlen)1);
    pgbox_("BNSTI", &c_b5, &c__0, "BNSTI", &c_b5, &c__0, (ftnlen)5, (ftnlen)5)
	    ;
    pgcell_(&map[1], n1, n2, &c__1, nx, &c__1, ny, &c_b6, &c_b5, tr, &ncols, 
	    red, gr, bl);
    pgbox_("C", &c_b5, &c__0, "C", &c_b5, &c__0, (ftnlen)1, (ftnlen)1);
    pgbox_("BNSTI", &c_b5, &c__0, "BNSTI", &c_b5, &c__0, (ftnlen)5, (ftnlen)5)
	    ;
} /* grey_ */


/* Subroutine */ int stgrey_(map, nmap, fmin, fmax, colbar, ncols)
real *map;
integer *nmap;
real *fmin, *fmax, *colbar;
integer *ncols;
{
    /* Format strings */
    static char fmt_100[] = "(\002 >>  Zmin & Zmax for plot ? (def=\002,1pe1\
0.3,\002,\002,e10.3,\002)  : \002,$)";
    static char fmt_110[] = "(\002 >>  Contrast factor ?  (def=\002,f3.1,\
\002)  : \002,$)";

    /* System generated locals */
    integer i__1;
    real r__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe(), s_rsli(), do_lio(), e_rsli();
    double pow_dd();

    /* Local variables */
    static real dcol, fmin2, fmax2, c__, f;
    static integer i__;
    static real fnorm;
    static char string[32];
    extern /* Subroutine */ int formtq_();
    static real col;
    static integer nnn;

    /* Fortran I/O blocks */
    static cilist io___183 = { 0, 6, 0, fmt_100, 0 };
    static icilist io___186 = { 1, string, 0, 0, 32, 1 };
    static cilist io___191 = { 0, 6, 0, fmt_110, 0 };
    static icilist io___192 = { 1, string, 0, 0, 32, 1 };


/*     -------------------------------------------------- */


    /* Parameter adjustments */
    colbar -= 3;
    --map;

    /* Function Body */
L1:
    s_wsfe(&io___183);
    do_fio(&c__1, (char *)&(*fmin), (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&(*fmax), (ftnlen)sizeof(real));
    e_wsfe();
    formtq_(string, &c__32, &nnn, (ftnlen)32);
    if (nnn != 0) {
	i__1 = s_rsli(&io___186);
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = do_lio(&c__4, &c__1, (char *)&fmin2, (ftnlen)sizeof(real));
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = do_lio(&c__4, &c__1, (char *)&fmax2, (ftnlen)sizeof(real));
	if (i__1 != 0) {
	    goto L1;
	}
	i__1 = e_rsli();
	if (i__1 != 0) {
	    goto L1;
	}
    }
    if (nnn != 0) {
	*fmin = fmin2;
	*fmax = fmax2;
    }
    if ((r__1 = *fmax - *fmin, dabs(r__1)) < (float)1e-20) {
	goto L1;
    }
    fnorm = (float)1. / (*fmax - *fmin);
L2:
    c__ = (float)1.;
    s_wsfe(&io___191);
    do_fio(&c__1, (char *)&c__, (ftnlen)sizeof(real));
    e_wsfe();
    formtq_(string, &c__32, &nnn, (ftnlen)32);
    if (nnn != 0) {
	i__1 = s_rsli(&io___192);
	if (i__1 != 0) {
	    goto L2;
	}
	i__1 = do_lio(&c__4, &c__1, (char *)&c__, (ftnlen)sizeof(real));
	if (i__1 != 0) {
	    goto L2;
	}
	i__1 = e_rsli();
	if (i__1 != 0) {
	    goto L2;
	}
    }
/* Computing MAX */
    r__1 = dmin(c__,(float)10.);
    c__ = dmax(r__1,(float).01);
    i__1 = *nmap;
    for (i__ = 1; i__ <= i__1; ++i__) {
	f = (map[i__] - *fmin) * fnorm;
	if (f <= (float)0.) {
	    map[i__] = (float)0.;
	} else if (f >= (float)1.) {
	    map[i__] = (float)1.;
	} else {
	    d__1 = (doublereal) f;
	    d__2 = (doublereal) c__;
	    map[i__] = pow_dd(&d__1, &d__2);
	}
/* L10: */
    }
    dcol = (float).999 / (real) (*ncols - 1);
    col = (float)0.;
    if (*fmax < *fmin) {
	col = (float)1.;
	dcol = -dcol;
    }
    i__1 = *ncols;
    for (i__ = 1; i__ <= i__1; ++i__) {
	d__1 = (doublereal) col;
	d__2 = (doublereal) c__;
	colbar[(i__ << 1) + 1] = pow_dd(&d__1, &d__2);
	d__1 = (doublereal) col;
	d__2 = (doublereal) c__;
	colbar[(i__ << 1) + 2] = pow_dd(&d__1, &d__2);
	col += dcol;
/* L20: */
    }
} /* stgrey_ */


/* Subroutine */ int lutin_(lutusr, ncols, nclusr, iplot)
real *lutusr;
integer *ncols, *nclusr, *iplot;
{
    /* Format strings */
    static char fmt_100[] = "(\002 INPUT> Filename for user colour-table ?  \
: \002,$)";
    static char fmt_200[] = "(a)";

    /* System generated locals */
    integer i__1, i__2;
    olist o__1;
    cllist cl__1;

    /* Builtin functions */
    integer s_wsfe(), e_wsfe(), s_rsfe(), do_fio(), e_rsfe(), f_open(), 
	    s_rsle(), do_lio(), e_rsle(), f_clos(), s_wsle(), e_wsle();

    /* Local variables */
    static integer i__, j;
    static char filnam[72];

    /* Fortran I/O blocks */
    static cilist io___197 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___198 = { 1, 5, 0, fmt_200, 0 };
    static cilist io___201 = { 1, 17, 1, 0, 0 };
    static cilist io___203 = { 0, 6, 0, 0, 0 };


/*     ------------------------------------------- */


    /* Parameter adjustments */
    lutusr -= 4;

    /* Function Body */
L1:
    s_wsfe(&io___197);
    e_wsfe();
    i__1 = s_rsfe(&io___198);
    if (i__1 != 0) {
	goto L1;
    }
    i__1 = do_fio(&c__1, filnam, (ftnlen)72);
    if (i__1 != 0) {
	goto L1;
    }
    i__1 = e_rsfe();
    if (i__1 != 0) {
	goto L1;
    }
    o__1.oerr = 1;
    o__1.ounit = 17;
    o__1.ofnmlen = 72;
    o__1.ofnm = filnam;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    i__1 = f_open(&o__1);
    if (i__1 != 0) {
	goto L1;
    }
    i__1 = *ncols;
    for (j = 1; j <= i__1; ++j) {
/* L10: */
	i__2 = s_rsle(&io___201);
	if (i__2 != 0) {
	    goto L2;
	}
	for (i__ = 1; i__ <= 3; ++i__) {
	    i__2 = do_lio(&c__4, &c__1, (char *)&lutusr[i__ + j * 3], (ftnlen)
		    sizeof(real));
	    if (i__2 != 0) {
		goto L2;
	    }
	}
	i__2 = e_rsle();
	if (i__2 != 0) {
	    goto L2;
	}
    }
L2:
    cl__1.cerr = 0;
    cl__1.cunit = 17;
    cl__1.csta = 0;
    f_clos(&cl__1);
    *nclusr = j - 1;
    s_wsle(&io___203);
    do_lio(&c__9, &c__1, " No. of colour indicies read in = ", (ftnlen)34);
    do_lio(&c__3, &c__1, (char *)&(*nclusr), (ftnlen)sizeof(integer));
    e_wsle();
    if (*nclusr <= 1) {
	*iplot = 2;
    } else {
	*ncols = *nclusr;
    }
} /* lutin_ */


/* Subroutine */ int setcol_(iplot, ncols, lutusr, r__, g, b)
integer *iplot, *ncols;
real *lutusr, *r__, *g, *b;
{
    /* Initialized data */

    static real bgyrw[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float)0.,(float)0.,(float).0195,(float)0.,(float)0.,(
	    float).0391,(float)0.,(float)0.,(float).0586,(float)0.,(float)0.,(
	    float).0781,(float)0.,(float)0.,(float).0977,(float)0.,(float)0.,(
	    float).1172,(float)0.,(float)0.,(float).1367,(float)0.,(float)0.,(
	    float).1563,(float)0.,(float)0.,(float).1758,(float)0.,(float)0.,(
	    float).1992,(float)0.,(float)0.,(float).2188,(float)0.,(float)0.,(
	    float).2383,(float)0.,(float)0.,(float).2578,(float)0.,(float)0.,(
	    float).2773,(float)0.,(float)0.,(float).2969,(float)0.,(float)0.,(
	    float).3164,(float)0.,(float)0.,(float).3359,(float)0.,(float)0.,(
	    float).3555,(float)0.,(float)0.,(float).375,(float)0.,(float)0.,(
	    float).3984,(float)0.,(float)0.,(float).418,(float)0.,(float)0.,(
	    float).4375,(float)0.,(float)0.,(float).457,(float)0.,(float)0.,(
	    float).4766,(float)0.,(float)0.,(float).4961,(float)0.,(float)0.,(
	    float).5156,(float)0.,(float)0.,(float).5352,(float)0.,(float)0.,(
	    float).5547,(float)0.,(float)0.,(float).5742,(float)0.,(float)0.,(
	    float).5977,(float)0.,(float)0.,(float).6172,(float)0.,(float)0.,(
	    float).6367,(float)0.,(float)0.,(float).6563,(float)0.,(float)0.,(
	    float).6758,(float)0.,(float)0.,(float).6953,(float)0.,(float)0.,(
	    float).7148,(float)0.,(float)0.,(float).7344,(float)0.,(float)0.,(
	    float).7539,(float)0.,(float)0.,(float).7734,(float)0.,(float)0.,(
	    float).7969,(float)0.,(float)0.,(float).8164,(float)0.,(float)0.,(
	    float).8359,(float)0.,(float)0.,(float).8555,(float)0.,(float)0.,(
	    float).875,(float)0.,(float)0.,(float).8945,(float)0.,(float)0.,(
	    float).9141,(float)0.,(float)0.,(float).9336,(float)0.,(float)0.,(
	    float).9531,(float)0.,(float)0.,(float).9727,(float)0.,(float)0.,(
	    float).9961,(float)0.,(float).0156,(float).9961,(float)0.,(float)
	    .0469,(float).9961,(float)0.,(float).0781,(float).9961,(float)0.,(
	    float).0938,(float).9961,(float)0.,(float).125,(float).9961,(
	    float)0.,(float).1563,(float).9961,(float)0.,(float).1719,(float)
	    .9961,(float)0.,(float).2031,(float).9961,(float)0.,(float).2344,(
	    float).9961,(float)0.,(float).2656,(float).9961,(float)0.,(float)
	    .2813,(float).9961,(float)0.,(float).3125,(float).9961,(float)0.,(
	    float).3438,(float).9961,(float)0.,(float).3594,(float).9961,(
	    float)0.,(float).3906,(float).9961,(float)0.,(float).4219,(float)
	    .9961,(float)0.,(float).4375,(float).9961,(float)0.,(float).4688,(
	    float).9961,(float)0.,(float).5,(float).9961,(float)0.,(float)
	    .5313,(float).9961,(float)0.,(float).5469,(float).9961,(float)0.,(
	    float).5781,(float).9961,(float)0.,(float).6094,(float).9961,(
	    float)0.,(float).625,(float).9961,(float)0.,(float).6563,(float)
	    .9961,(float)0.,(float).6875,(float).9961,(float)0.,(float).7031,(
	    float).9961,(float)0.,(float).7344,(float).9961,(float)0.,(float)
	    .7656,(float).9961,(float)0.,(float).7969,(float).9961,(float)0.,(
	    float).8125,(float).9961,(float)0.,(float).8438,(float).9961,(
	    float)0.,(float).875,(float).9961,(float)0.,(float).8906,(float)
	    .9961,(float)0.,(float).9219,(float).9961,(float)0.,(float).9531,(
	    float).9961,(float)0.,(float).9688,(float).9961,(float)0.,(float)
	    .9961,(float).9844,(float)0.,(float).9961,(float).9531,(float)0.,(
	    float).9961,(float).9219,(float)0.,(float).9961,(float).9063,(
	    float)0.,(float).9961,(float).875,(float)0.,(float).9961,(float)
	    .8438,(float)0.,(float).9961,(float).8281,(float)0.,(float).9961,(
	    float).7969,(float)0.,(float).9961,(float).7656,(float)0.,(float)
	    .9961,(float).75,(float)0.,(float).9961,(float).7188,(float)0.,(
	    float).9961,(float).6875,(float)0.,(float).9961,(float).6563,(
	    float)0.,(float).9961,(float).6406,(float)0.,(float).9961,(float)
	    .6094,(float)0.,(float).9961,(float).5781,(float)0.,(float).9961,(
	    float).5625,(float)0.,(float).9961,(float).5313,(float)0.,(float)
	    .9961,(float).5,(float)0.,(float).9961,(float).4844,(float)0.,(
	    float).9961,(float).4531,(float)0.,(float).9961,(float).4219,(
	    float)0.,(float).9961,(float).3906,(float)0.,(float).9961,(float)
	    .375,(float)0.,(float).9961,(float).3438,(float)0.,(float).9961,(
	    float).3125,(float)0.,(float).9961,(float).2969,(float)0.,(float)
	    .9961,(float).2656,(float)0.,(float).9961,(float).2344,(float)0.,(
	    float).9961,(float).2188,(float)0.,(float).9961,(float).1875,(
	    float)0.,(float).9961,(float).1563,(float)0.,(float).9961,(float)
	    .125,(float)0.,(float).9961,(float).1094,(float)0.,(float).9961,(
	    float).0781,(float)0.,(float).9961,(float).0469,(float)0.,(float)
	    .9961,(float).0313,(float)0.,(float).9961,(float)0.,(float).0156,(
	    float).9961,(float)0.,(float).0313,(float).9961,(float)0.,(float)
	    .0625,(float).9961,(float)0.,(float).0938,(float).9961,(float)0.,(
	    float).125,(float).9961,(float)0.,(float).1406,(float).9961,(
	    float)0.,(float).1719,(float).9961,(float)0.,(float).2031,(float)
	    .9961,(float)0.,(float).2188,(float).9961,(float)0.,(float).25,(
	    float).9961,(float)0.,(float).2813,(float).9961,(float)0.,(float)
	    .2969,(float).9961,(float)0.,(float).3281,(float).9961,(float)0.,(
	    float).3594,(float).9961,(float)0.,(float).3906,(float).9961,(
	    float)0.,(float).4063,(float).9961,(float)0.,(float).4375,(float)
	    .9961,(float)0.,(float).4688,(float).9961,(float)0.,(float).4844,(
	    float).9961,(float)0.,(float).5156,(float).9961,(float)0.,(float)
	    .5469,(float).9961,(float)0.,(float).5625,(float).9961,(float)0.,(
	    float).5938,(float).9961,(float)0.,(float).625,(float).9961,(
	    float)0.,(float).6563,(float).9961,(float)0.,(float).6719,(float)
	    .9961,(float)0.,(float).7031,(float).9961,(float)0.,(float).7344,(
	    float).9961,(float)0.,(float).75,(float).9961,(float)0.,(float)
	    .7813,(float).9961,(float)0.,(float).8125,(float).9961,(float)0.,(
	    float).8281,(float).9961,(float)0.,(float).8594,(float).9961,(
	    float)0.,(float).8906,(float).9961,(float)0.,(float).9219,(float)
	    .9961,(float)0.,(float).9375,(float).9961,(float)0.,(float).9688,(
	    float).9961,(float)0.,(float).9961,(float).9844,(float)0.,(float)
	    .9961,(float).9688,(float)0.,(float).9961,(float).9375,(float)0.,(
	    float).9961,(float).9063,(float)0.,(float).9961,(float).8906,(
	    float)0.,(float).9961,(float).8594,(float)0.,(float).9961,(float)
	    .8281,(float)0.,(float).9961,(float).7969,(float)0.,(float).9961,(
	    float).7813,(float)0.,(float).9961,(float).75,(float)0.,(float)
	    .9961,(float).7188,(float)0.,(float).9961,(float).7031,(float)0.,(
	    float).9961,(float).6719,(float)0.,(float).9961,(float).6406,(
	    float)0.,(float).9961,(float).625,(float)0.,(float).9961,(float)
	    .5938,(float)0.,(float).9961,(float).5625,(float)0.,(float).9961,(
	    float).5313,(float)0.,(float).9961,(float).5156,(float)0.,(float)
	    .9961,(float).4844,(float)0.,(float).9961,(float).4531,(float)0.,(
	    float).9961,(float).4375,(float)0.,(float).9961,(float).4063,(
	    float)0.,(float).9961,(float).375,(float)0.,(float).9961,(float)
	    .3594,(float)0.,(float).9961,(float).3281,(float)0.,(float).9961,(
	    float).2969,(float)0.,(float).9961,(float).2656,(float)0.,(float)
	    .9961,(float).25,(float)0.,(float).9961,(float).2188,(float)0.,(
	    float).9961,(float).1875,(float)0.,(float).9961,(float).1719,(
	    float)0.,(float).9961,(float).1406,(float)0.,(float).9961,(float)
	    .1094,(float)0.,(float).9961,(float).0938,(float)0.,(float).9961,(
	    float).0625,(float)0.,(float).9961,(float).0313,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float)0.,(float)0.,(float)
	    .9961,(float)0.,(float)0.,(float).9961,(float).0273,(float).0273,(
	    float).9961,(float).0547,(float).0547,(float).9961,(float).082,(
	    float).082,(float).9961,(float).1133,(float).1133,(float).9961,(
	    float).1406,(float).1406,(float).9961,(float).168,(float).168,(
	    float).9961,(float).1992,(float).1992,(float).9961,(float).2266,(
	    float).2266,(float).9961,(float).2539,(float).2539,(float).9961,(
	    float).2813,(float).2813,(float).9961,(float).3125,(float).3125,(
	    float).9961,(float).3398,(float).3398,(float).9961,(float).3672,(
	    float).3672,(float).9961,(float).3984,(float).3984,(float).9961,(
	    float).4258,(float).4258,(float).9961,(float).4531,(float).4531,(
	    float).9961,(float).4805,(float).4805,(float).9961,(float).5117,(
	    float).5117,(float).9961,(float).5391,(float).5391,(float).9961,(
	    float).5664,(float).5664,(float).9961,(float).5977,(float).5977,(
	    float).9961,(float).625,(float).625,(float).9961,(float).6523,(
	    float).6523,(float).9961,(float).6797,(float).6797,(float).9961,(
	    float).7109,(float).7109,(float).9961,(float).7383,(float).7383,(
	    float).9961,(float).7656,(float).7656,(float).9961,(float).7969,(
	    float).7969,(float).9961,(float).8242,(float).8242,(float).9961,(
	    float).8516,(float).8516,(float).9961,(float).8789,(float).8789,(
	    float).9961,(float).9102,(float).9102,(float).9961,(float).9375,(
	    float).9375,(float).9961,(float).9648,(float).9648,(float).9961,(
	    float).9961,(float).9961 };
    static real heat[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)
	    0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(
	    float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)
	    0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(float)0.,(
	    float)0.,(float)0.,(float).0195,(float)0.,(float).0195,(float)
	    .0391,(float)0.,(float).0391,(float).0586,(float)0.,(float).0586,(
	    float).0781,(float)0.,(float).0781,(float).1016,(float)0.,(float)
	    .1016,(float).1211,(float)0.,(float).1211,(float).1406,(float)0.,(
	    float).1406,(float).1602,(float)0.,(float).1602,(float).1797,(
	    float)0.,(float).1797,(float).1992,(float)0.,(float).1992,(float)
	    .2188,(float)0.,(float).2188,(float).2383,(float)0.,(float).2383,(
	    float).2578,(float)0.,(float).2578,(float).2773,(float)0.,(float)
	    .2773,(float).3008,(float)0.,(float).3008,(float).3203,(float)0.,(
	    float).3203,(float).3398,(float)0.,(float).3398,(float).3594,(
	    float)0.,(float).3594,(float).3789,(float)0.,(float).3789,(float)
	    .3984,(float)0.,(float).3984,(float).418,(float)0.,(float).418,(
	    float).4375,(float)0.,(float).4375,(float).457,(float)0.,(float)
	    .457,(float).4766,(float)0.,(float).4766,(float).5,(float)0.,(
	    float).5,(float).5195,(float)0.,(float).5195,(float).5391,(float)
	    0.,(float).5391,(float).5586,(float)0.,(float).5586,(float).5781,(
	    float)0.,(float).5781,(float).5977,(float)0.,(float).5977,(float)
	    .6172,(float)0.,(float).6172,(float).6367,(float)0.,(float).6367,(
	    float).6563,(float)0.,(float).6563,(float).6758,(float)0.,(float)
	    .6758,(float).6992,(float)0.,(float).6992,(float).7188,(float)0.,(
	    float).7188,(float).7383,(float)0.,(float).7383,(float).7578,(
	    float)0.,(float).7578,(float).7773,(float)0.,(float).7773,(float)
	    .7969,(float)0.,(float).7969,(float).8164,(float)0.,(float).8164,(
	    float).8359,(float)0.,(float).8359,(float).8555,(float)0.,(float)
	    .8555,(float).875,(float)0.,(float).875,(float).8984,(float)0.,(
	    float).8984,(float).9375,(float)0.,(float).918,(float).9766,(
	    float)0.,(float).9375,(float).9961,(float)0.,(float).957,(float)
	    .9961,(float)0.,(float).9766,(float).9961,(float)0.,(float).9961,(
	    float).9961,(float)0.,(float).9766,(float).9961,(float)0.,(float)
	    .957,(float).9961,(float)0.,(float).9375,(float).9961,(float)0.,(
	    float).918,(float).9961,(float)0.,(float).8984,(float).9961,(
	    float)0.,(float).875,(float).9961,(float)0.,(float).8555,(float)
	    .9961,(float)0.,(float).8359,(float).9961,(float)0.,(float).8164,(
	    float).9961,(float)0.,(float).7969,(float).9961,(float)0.,(float)
	    .7773,(float).9961,(float)0.,(float).7578,(float).9961,(float)0.,(
	    float).7383,(float).9961,(float)0.,(float).7188,(float).9961,(
	    float)0.,(float).6992,(float).9961,(float)0.,(float).6758,(float)
	    .9961,(float)0.,(float).6563,(float).9961,(float)0.,(float).6367,(
	    float).9961,(float)0.,(float).6172,(float).9961,(float)0.,(float)
	    .5977,(float).9961,(float)0.,(float).5781,(float).9961,(float)0.,(
	    float).5586,(float).9961,(float)0.,(float).5391,(float).9961,(
	    float)0.,(float).5195,(float).9961,(float)0.,(float).5,(float)
	    .9961,(float)0.,(float).4766,(float).9961,(float)0.,(float).457,(
	    float).9961,(float)0.,(float).4375,(float).9961,(float)0.,(float)
	    .418,(float).9961,(float)0.,(float).3984,(float).9961,(float)0.,(
	    float).3789,(float).9961,(float)0.,(float).3594,(float).9961,(
	    float)0.,(float).3398,(float).9961,(float)0.,(float).3203,(float)
	    .9961,(float)0.,(float).3008,(float).9961,(float)0.,(float).2773,(
	    float).9961,(float)0.,(float).2578,(float).9961,(float)0.,(float)
	    .2383,(float).9961,(float)0.,(float).2188,(float).9961,(float)0.,(
	    float).1992,(float).9961,(float)0.,(float).1797,(float).9961,(
	    float).0117,(float).1602,(float).9961,(float).0195,(float).1406,(
	    float).9961,(float).0313,(float).1211,(float).9961,(float).0391,(
	    float)0.,(float).9961,(float).0508,(float).0781,(float).9961,(
	    float).0586,(float).0586,(float).9961,(float).0703,(float).0391,(
	    float).9961,(float).0781,(float).0195,(float).9961,(float).0898,(
	    float)0.,(float).9961,(float).1016,(float)0.,(float).9961,(float)
	    .1094,(float)0.,(float).9961,(float).1211,(float)0.,(float).9961,(
	    float).1289,(float)0.,(float).9961,(float).1406,(float)0.,(float)
	    .9961,(float).1484,(float)0.,(float).9961,(float).1602,(float)0.,(
	    float).9961,(float).1719,(float)0.,(float).9961,(float).1797,(
	    float)0.,(float).9961,(float).1914,(float)0.,(float).9961,(float)
	    .1992,(float)0.,(float).9961,(float).2109,(float)0.,(float).9961,(
	    float).2188,(float)0.,(float).9961,(float).2305,(float)0.,(float)
	    .9961,(float).2383,(float)0.,(float).9961,(float).25,(float)0.,(
	    float).9961,(float).2617,(float)0.,(float).9961,(float).2695,(
	    float)0.,(float).9961,(float).2813,(float)0.,(float).9961,(float)
	    .2891,(float)0.,(float).9961,(float).3008,(float)0.,(float).9961,(
	    float).3086,(float)0.,(float).9961,(float).3203,(float)0.,(float)
	    .9961,(float).3281,(float)0.,(float).9961,(float).3398,(float)0.,(
	    float).9961,(float).3516,(float)0.,(float).9961,(float).3594,(
	    float)0.,(float).9961,(float).3711,(float)0.,(float).9961,(float)
	    .3789,(float)0.,(float).9961,(float).3906,(float)0.,(float).9961,(
	    float).3984,(float)0.,(float).9961,(float).4102,(float)0.,(float)
	    .9961,(float).4219,(float)0.,(float).9961,(float).4297,(float)0.,(
	    float).9961,(float).4414,(float)0.,(float).9961,(float).4492,(
	    float)0.,(float).9961,(float).4609,(float)0.,(float).9961,(float)
	    .4688,(float)0.,(float).9961,(float).4805,(float)0.,(float).9961,(
	    float).4883,(float)0.,(float).9961,(float).5195,(float)0.,(float)
	    .9961,(float).5273,(float)0.,(float).9961,(float).5391,(float)0.,(
	    float).9961,(float).5469,(float)0.,(float).9961,(float).5625,(
	    float)0.,(float).9961,(float).5703,(float)0.,(float).9961,(float)
	    .582,(float)0.,(float).9961,(float).5898,(float)0.,(float).9961,(
	    float).6016,(float)0.,(float).9961,(float).6094,(float)0.,(float)
	    .9961,(float).6172,(float)0.,(float).9961,(float).6289,(float)0.,(
	    float).9961,(float).6367,(float)0.,(float).9961,(float).6484,(
	    float)0.,(float).9961,(float).6602,(float)0.,(float).9961,(float)
	    .6719,(float)0.,(float).9961,(float).6797,(float)0.,(float).9961,(
	    float).6875,(float)0.,(float).9961,(float).6992,(float)0.,(float)
	    .9961,(float).707,(float)0.,(float).9961,(float).7188,(float)0.,(
	    float).9961,(float).7266,(float)0.,(float).9961,(float).7383,(
	    float)0.,(float).9961,(float).7461,(float)0.,(float).9961,(float)
	    .7617,(float)0.,(float).9961,(float).7695,(float)0.,(float).9961,(
	    float).7773,(float)0.,(float).9961,(float).7891,(float)0.,(float)
	    .9961,(float).7969,(float)0.,(float).9961,(float).8086,(float)0.,(
	    float).9961,(float).8164,(float)0.,(float).9961,(float).8281,(
	    float)0.,(float).9961,(float).8359,(float)0.,(float).9961,(float)
	    .8477,(float)0.,(float).9961,(float).8594,(float)0.,(float).9961,(
	    float).8672,(float)0.,(float).9961,(float).8789,(float)0.,(float)
	    .9961,(float).8867,(float)0.,(float).9961,(float).8984,(float)0.,(
	    float).9961,(float).9063,(float)0.,(float).9961,(float).918,(
	    float)0.,(float).9961,(float).9258,(float)0.,(float).9961,(float)
	    .9688,(float).0156,(float).9961,(float).9961,(float).0352,(float)
	    .9961,(float).9961,(float).0508,(float).9961,(float).9961,(float)
	    .0664,(float).9961,(float).9961,(float).082,(float).9961,(float)
	    .9961,(float).1016,(float).9961,(float).9961,(float).1172,(float)
	    .9961,(float).9961,(float).1328,(float).9961,(float).9961,(float)
	    .1484,(float).9961,(float).9961,(float).168,(float).9961,(float)
	    .9961,(float).1836,(float).9961,(float).9961,(float).1992,(float)
	    .9961,(float).9961,(float).2148,(float).9961,(float).9961,(float)
	    .2344,(float).9961,(float).9961,(float).25,(float).9961,(float)
	    .9961,(float).2656,(float).9961,(float).9961,(float).2852,(float)
	    .9961,(float).9961,(float).3008,(float).9961,(float).9961,(float)
	    .3164,(float).9961,(float).9961,(float).332,(float).9961,(float)
	    .9961,(float).3516,(float).9961,(float).9961,(float).3672,(float)
	    .9961,(float).9961,(float).3828,(float).9961,(float).9961,(float)
	    .3984,(float).9961,(float).9961,(float).418,(float).9961,(float)
	    .9961,(float).4336,(float).9961,(float).9961,(float).4492,(float)
	    .9961,(float).9961,(float).4648,(float).9961,(float).9961,(float)
	    .4844,(float).9961,(float).9961,(float).5,(float).9961,(float)
	    .9961,(float).4844,(float).9961,(float).9961,(float).4648,(float)
	    .9961,(float).9961,(float).4492,(float).9961,(float).9961,(float)
	    .4336,(float).9961,(float).9961,(float).4531,(float).9961,(float)
	    .9961,(float).4648,(float).9961,(float).9961,(float).4844,(float)
	    .9961,(float).9961,(float).5,(float).9961,(float).9961,(float)
	    .5195,(float).9961,(float).9961,(float).5313,(float).9961,(float)
	    .9961,(float).5508,(float).9961,(float).9961,(float).5664,(float)
	    .9961,(float).9961,(float).5859,(float).9961,(float).9961,(float)
	    .5977,(float).9961,(float).9961,(float).6172,(float).9961,(float)
	    .9961,(float).6328,(float).9961,(float).9961,(float).6484,(float)
	    .9961,(float).9961,(float).6641,(float).9961,(float).9961,(float)
	    .6836,(float).9961,(float).9961,(float).6992,(float).9961,(float)
	    .9961,(float).7148,(float).9844,(float).9844,(float).7305,(float)
	    .9688,(float).9688,(float).75,(float).9414,(float).9414,(float)
	    .7656,(float).9258,(float).9258,(float).7813,(float).9023,(float)
	    .9023,(float).7969,(float).8867,(float).8867,(float).8164,(float)
	    .8633,(float).8633,(float).832,(float).8672,(float).8672,(float)
	    .8477,(float).8633,(float).8633,(float).8633,(float).8984,(float)
	    .8984,(float).8984,(float).9297,(float).9297,(float).9297,(float)
	    .9648,(float).9648,(float).9648,(float).9961,(float).9961,(float)
	    .9961 };
    static real spectrum[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float).016,(float).169,(float).91,(float).012,(float)
	    .184,(float).922,(float).0118,(float).1961,(float).9373,(float)
	    .0078,(float).2078,(float).949,(float).0039,(float).2235,(float)
	    .9608,(float)0.,(float).2392,(float).9765,(float)0.,(float).251,(
	    float).9882,(float)0.,(float).2627,(float)1.,(float)0.,(float)
	    .2627,(float)1.,(float)0.,(float).2745,(float).9882,(float)0.,(
	    float).2863,(float).9804,(float)0.,(float).298,(float).9686,(
	    float)0.,(float).3098,(float).9569,(float)0.,(float).3255,(float)
	    .949,(float)0.,(float).3373,(float).9373,(float)0.,(float).349,(
	    float).9294,(float)0.,(float).3647,(float).9176,(float)0.,(float)
	    .3804,(float).9059,(float)0.,(float).3922,(float).8941,(float)0.,(
	    float).4039,(float).8863,(float)0.,(float).4157,(float).8745,(
	    float)0.,(float).4275,(float).8667,(float)0.,(float).4431,(float)
	    .8549,(float)0.,(float).4549,(float).8431,(float)0.,(float).4667,(
	    float).8353,(float)0.,(float).4784,(float).8235,(float)0.,(float)
	    .4941,(float).8118,(float)0.,(float).5098,(float).8039,(float)0.,(
	    float).5216,(float).7922,(float)0.,(float).5333,(float).7804,(
	    float)0.,(float).5451,(float).7725,(float)0.,(float).5608,(float)
	    .7647,(float)0.,(float).5725,(float).7529,(float)0.,(float).5843,(
	    float).7412,(float)0.,(float).5961,(float).7333,(float)0.,(float)
	    .6118,(float).7216,(float)0.,(float).6235,(float).7098,(float)0.,(
	    float).6353,(float).702,(float)0.,(float).651,(float).6902,(float)
	    0.,(float).6627,(float).6784,(float)0.,(float).6745,(float).6706,(
	    float)0.,(float).6902,(float).6588,(float)0.,(float).702,(float)
	    .6471,(float)0.,(float).7137,(float).6392,(float)0.,(float).7294,(
	    float).6314,(float)0.,(float).7412,(float).6196,(float)0.,(float)
	    .7529,(float).6078,(float)0.,(float).7647,(float).6,(float)0.,(
	    float).7804,(float).5882,(float)0.,(float).7843,(float).5882,(
	    float)0.,(float).7843,(float).5804,(float)0.,(float).7882,(float)
	    .5647,(float)0.,(float).7961,(float).549,(float)0.,(float).8,(
	    float).5333,(float)0.,(float).8078,(float).5216,(float)0.,(float)
	    .8118,(float).5059,(float)0.,(float).8157,(float).4902,(float)0.,(
	    float).8235,(float).4745,(float)0.,(float).8275,(float).4627,(
	    float)0.,(float).8353,(float).4471,(float)0.,(float).8392,(float)
	    .4314,(float)0.,(float).8431,(float).4196,(float)0.,(float).8471,(
	    float).4039,(float)0.,(float).8549,(float).3882,(float)0.,(float)
	    .8588,(float).3725,(float)0.,(float).8667,(float).3569,(float)0.,(
	    float).8745,(float).3451,(float)0.,(float).8784,(float).3294,(
	    float)0.,(float).8824,(float).3137,(float)0.,(float).8863,(float)
	    .302,(float)0.,(float).8941,(float).2863,(float)0.,(float).898,(
	    float).2706,(float)0.,(float).902,(float).2549,(float)0.,(float)
	    .9059,(float).2392,(float)0.,(float).9137,(float).2275,(float)0.,(
	    float).9176,(float).2118,(float)0.,(float).9255,(float).2,(float)
	    0.,(float).9333,(float).1843,(float)0.,(float).9373,(float).1686,(
	    float)0.,(float).9412,(float).1529,(float)0.,(float).9451,(float)
	    .1373,(float)0.,(float).9529,(float).1255,(float)0.,(float).9569,(
	    float).1098,(float)0.,(float).9647,(float).098,(float)0.,(float)
	    .9686,(float).0824,(float)0.,(float).9725,(float).0667,(float)0.,(
	    float).9765,(float).051,(float)0.,(float).9843,(float).0353,(
	    float)0.,(float).9922,(float).0196,(float)0.,(float).9961,(float)
	    .0078,(float)0.,(float)1.,(float)0.,(float).0039,(float)1.,(float)
	    0.,(float).0275,(float)1.,(float)0.,(float).051,(float)1.,(float)
	    0.,(float).0745,(float)1.,(float)0.,(float).098,(float)1.,(float)
	    0.,(float).1216,(float)1.,(float)0.,(float).149,(float)1.,(float)
	    0.,(float).1765,(float)1.,(float)0.,(float).2,(float)1.,(float)0.,
	    (float).2235,(float)1.,(float)0.,(float).2471,(float)1.,(float)0.,
	    (float).2745,(float)1.,(float)0.,(float).302,(float)1.,(float)0.,(
	    float).3255,(float)1.,(float)0.,(float).349,(float)1.,(float)0.,(
	    float).3725,(float)1.,(float)0.,(float).3961,(float)1.,(float)0.,(
	    float).4235,(float)1.,(float)0.,(float).451,(float)1.,(float)0.,(
	    float).4745,(float)1.,(float)0.,(float).498,(float)1.,(float)0.,(
	    float).5216,(float)1.,(float)0.,(float).549,(float)1.,(float)0.,(
	    float).5765,(float)1.,(float)0.,(float).6,(float)1.,(float)0.,(
	    float).6235,(float)1.,(float)0.,(float).6471,(float)1.,(float)0.,(
	    float).6706,(float)1.,(float)0.,(float).698,(float)1.,(float)0.,(
	    float).7216,(float)1.,(float)0.,(float).749,(float)1.,(float)0.,(
	    float).7725,(float)1.,(float)0.,(float).7961,(float)1.,(float)0.,(
	    float).8235,(float)1.,(float)0.,(float).8471,(float)1.,(float)0.,(
	    float).8745,(float)1.,(float)0.,(float).898,(float)1.,(float)0.,(
	    float).9216,(float)1.,(float)0.,(float).9451,(float)1.,(float)0.,(
	    float).9725,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(
	    float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)
	    1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(
	    float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)
	    1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(
	    float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)
	    0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(
	    float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)
	    1.,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(
	    float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)
	    1.,(float)0.,(float)1.,(float)1.,(float)0.,(float)1.,(float)1.,(
	    float)0.,(float)1.,(float).9843,(float)0.,(float)1.,(float).9725,(
	    float)0.,(float)1.,(float).9608,(float)0.,(float)1.,(float).949,(
	    float)0.,(float)1.,(float).9373,(float)0.,(float)1.,(float).9255,(
	    float)0.,(float)1.,(float).9098,(float)0.,(float)1.,(float).898,(
	    float)0.,(float)1.,(float).8863,(float)0.,(float)1.,(float).8745,(
	    float)0.,(float)1.,(float).8627,(float)0.,(float)1.,(float).851,(
	    float)0.,(float)1.,(float).8392,(float)0.,(float)1.,(float).8275,(
	    float)0.,(float)1.,(float).8157,(float)0.,(float)1.,(float).8039,(
	    float)0.,(float)1.,(float).7922,(float)0.,(float)1.,(float).7765,(
	    float)0.,(float)1.,(float).7647,(float)0.,(float)1.,(float).7529,(
	    float)0.,(float)1.,(float).7412,(float)0.,(float)1.,(float).7294,(
	    float)0.,(float)1.,(float).7176,(float)0.,(float)1.,(float).7059,(
	    float)0.,(float)1.,(float).6941,(float)0.,(float)1.,(float).6824,(
	    float)0.,(float)1.,(float).6706,(float)0.,(float)1.,(float).6549,(
	    float)0.,(float)1.,(float).6392,(float)0.,(float)1.,(float).6275,(
	    float)0.,(float)1.,(float).6157,(float)0.,(float)1.,(float).6039,(
	    float)0.,(float)1.,(float).5922,(float)0.,(float)1.,(float).5804,(
	    float)0.,(float)1.,(float).5686,(float)0.,(float)1.,(float).5569,(
	    float)0.,(float)1.,(float).5451,(float)0.,(float)1.,(float).5333,(
	    float)0.,(float)1.,(float).5216,(float)0.,(float)1.,(float).5098,(
	    float)0.,(float)1.,(float).4941,(float)0.,(float)1.,(float).4824,(
	    float)0.,(float)1.,(float).4706,(float)0.,(float)1.,(float).4588,(
	    float)0.,(float)1.,(float).4471,(float)0.,(float)1.,(float).4353,(
	    float)0.,(float)1.,(float).4235,(float)0.,(float)1.,(float).4118,(
	    float)0.,(float)1.,(float).4,(float)0.,(float)1.,(float).3882,(
	    float)0.,(float)1.,(float).3765,(float)0.,(float)1.,(float).3608,(
	    float)0.,(float)1.,(float).349,(float)0.,(float)1.,(float).3373,(
	    float)0.,(float)1.,(float).3255,(float)0.,(float)1.,(float).3098,(
	    float)0.,(float)1.,(float).298,(float)0.,(float)1.,(float).2863,(
	    float)0.,(float)1.,(float).2745,(float)0.,(float)1.,(float).2627,(
	    float)0.,(float)1.,(float).251,(float)0.,(float)1.,(float).2392,(
	    float)0.,(float)1.,(float).2235,(float)0.,(float)1.,(float).2118,(
	    float)0.,(float)1.,(float).2,(float)0.,(float)1.,(float).1882,(
	    float)0.,(float)1.,(float).1765,(float)0.,(float)1.,(float).1647,(
	    float)0.,(float)1.,(float).1529,(float)0.,(float)1.,(float).1412,(
	    float)0.,(float)1.,(float).1294,(float)0.,(float)1.,(float).1176,(
	    float)0.,(float)1.,(float).1059,(float)0.,(float)1.,(float).0902,(
	    float)0.,(float)1.,(float).0784,(float)0.,(float)1.,(float).0667,(
	    float)0.,(float)1.,(float).0549,(float)0.,(float)1.,(float).0431,(
	    float)0.,(float)1.,(float).0314,(float)0.,(float)1.,(float).0196,(
	    float)0.,(float)1.,(float).0078,(float)0.,(float)1.,(float)0.,(
	    float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)
	    0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(
	    float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)
	    1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(
	    float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)
	    0.,(float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(
	    float)0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)
	    0.,(float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0.,(
	    float)1.,(float)0.,(float)0.,(float)1.,(float)0.,(float)0. };
    static real serp[768]	/* was [3][256] */ = { (float)0.,(float)0.,(
	    float)0.,(float)0.,(float)1.,(float)1.,(float).0204,(float)1.,(
	    float).9796,(float).0408,(float)1.,(float).9592,(float).0612,(
	    float)1.,(float).9388,(float).0816,(float)1.,(float).9184,(float)
	    .102,(float)1.,(float).898,(float).1224,(float)1.,(float).8776,(
	    float).1429,(float)1.,(float).8571,(float).1633,(float)1.,(float)
	    .8367,(float).1837,(float)1.,(float).8163,(float).2041,(float)1.,(
	    float).7959,(float).2245,(float)1.,(float).7755,(float).2449,(
	    float)1.,(float).7551,(float).2653,(float)1.,(float).7347,(float)
	    .2857,(float)1.,(float).7143,(float).3061,(float)1.,(float).6939,(
	    float).3265,(float)1.,(float).6735,(float).3469,(float)1.,(float)
	    .6531,(float).3673,(float)1.,(float).6327,(float).3878,(float)1.,(
	    float).6122,(float).4082,(float)1.,(float).5918,(float).4286,(
	    float)1.,(float).5714,(float).449,(float)1.,(float).551,(float)
	    .4694,(float)1.,(float).5306,(float).4898,(float)1.,(float).5102,(
	    float).5102,(float)1.,(float).4898,(float).5306,(float)1.,(float)
	    .4694,(float).551,(float)1.,(float).449,(float).5714,(float)1.,(
	    float).4286,(float).5918,(float)1.,(float).4082,(float).6122,(
	    float)1.,(float).3878,(float).6327,(float)1.,(float).3673,(float)
	    .6531,(float)1.,(float).3469,(float).6735,(float)1.,(float).3265,(
	    float).6939,(float)1.,(float).3061,(float).7143,(float)1.,(float)
	    .2857,(float).7347,(float)1.,(float).2653,(float).7551,(float)1.,(
	    float).2449,(float).7755,(float)1.,(float).2245,(float).7959,(
	    float)1.,(float).2041,(float).8163,(float)1.,(float).1837,(float)
	    .8367,(float)1.,(float).1633,(float).8571,(float)1.,(float).1429,(
	    float).8776,(float)1.,(float).1224,(float).898,(float)1.,(float)
	    .102,(float).9184,(float)1.,(float).0816,(float).9388,(float)1.,(
	    float).0612,(float).9592,(float)1.,(float).0408,(float).9796,(
	    float)1.,(float).0204,(float)1.,(float)1.,(float)0.,(float)1.,(
	    float).98,(float).02,(float)1.,(float).96,(float).04,(float)1.,(
	    float).94,(float).06,(float)1.,(float).92,(float).08,(float)1.,(
	    float).9,(float).1,(float)1.,(float).88,(float).12,(float)1.,(
	    float).86,(float).14,(float)1.,(float).84,(float).16,(float)1.,(
	    float).82,(float).18,(float)1.,(float).8,(float).2,(float)1.,(
	    float).78,(float).22,(float)1.,(float).76,(float).24,(float)1.,(
	    float).74,(float).26,(float)1.,(float).72,(float).28,(float)1.,(
	    float).7,(float).3,(float)1.,(float).68,(float).32,(float)1.,(
	    float).66,(float).34,(float)1.,(float).64,(float).36,(float)1.,(
	    float).62,(float).38,(float)1.,(float).6,(float).4,(float)1.,(
	    float).58,(float).42,(float)1.,(float).56,(float).44,(float)1.,(
	    float).54,(float).46,(float)1.,(float).52,(float).48,(float)1.,(
	    float).5,(float).5,(float)1.,(float).48,(float).52,(float)1.,(
	    float).46,(float).54,(float)1.,(float).44,(float).56,(float)1.,(
	    float).42,(float).58,(float)1.,(float).4,(float).6,(float)1.,(
	    float).38,(float).62,(float)1.,(float).36,(float).64,(float)1.,(
	    float).34,(float).66,(float)1.,(float).32,(float).68,(float)1.,(
	    float).3,(float).7,(float)1.,(float).28,(float).72,(float)1.,(
	    float).26,(float).74,(float)1.,(float).24,(float).76,(float)1.,(
	    float).22,(float).78,(float)1.,(float).2,(float).8,(float)1.,(
	    float).18,(float).82,(float)1.,(float).16,(float).84,(float)1.,(
	    float).14,(float).86,(float)1.,(float).12,(float).88,(float)1.,(
	    float).1,(float).9,(float)1.,(float).08,(float).92,(float)1.,(
	    float).06,(float).94,(float)1.,(float).04,(float).96,(float)1.,(
	    float).02,(float).98,(float)1.,(float)0.,(float)1.,(float)1.,(
	    float)0.,(float).99,(float)1.,(float)0.,(float).98,(float)1.,(
	    float)0.,(float).97,(float)1.,(float)0.,(float).96,(float)1.,(
	    float)0.,(float).95,(float)1.,(float)0.,(float).94,(float)1.,(
	    float)0.,(float).93,(float)1.,(float)0.,(float).92,(float)1.,(
	    float)0.,(float).91,(float)1.,(float)0.,(float).9,(float)1.,(
	    float)0.,(float).89,(float)1.,(float)0.,(float).88,(float)1.,(
	    float)0.,(float).87,(float)1.,(float)0.,(float).86,(float)1.,(
	    float)0.,(float).85,(float)1.,(float)0.,(float).84,(float)1.,(
	    float)0.,(float).83,(float)1.,(float)0.,(float).82,(float)1.,(
	    float)0.,(float).81,(float)1.,(float)0.,(float).8,(float)1.,(
	    float)0.,(float).79,(float)1.,(float)0.,(float).78,(float)1.,(
	    float)0.,(float).77,(float)1.,(float)0.,(float).76,(float)1.,(
	    float)0.,(float).75,(float)1.,(float)0.,(float).74,(float)1.,(
	    float)0.,(float).73,(float)1.,(float)0.,(float).72,(float)1.,(
	    float)0.,(float).71,(float)1.,(float)0.,(float).7,(float)1.,(
	    float)0.,(float).69,(float)1.,(float)0.,(float).68,(float)1.,(
	    float)0.,(float).67,(float)1.,(float)0.,(float).66,(float)1.,(
	    float)0.,(float).65,(float)1.,(float)0.,(float).64,(float)1.,(
	    float)0.,(float).63,(float)1.,(float)0.,(float).62,(float)1.,(
	    float)0.,(float).61,(float)1.,(float)0.,(float).6,(float)1.,(
	    float)0.,(float).59,(float)1.,(float)0.,(float).58,(float)1.,(
	    float)0.,(float).57,(float)1.,(float)0.,(float).56,(float)1.,(
	    float)0.,(float).55,(float)1.,(float)0.,(float).54,(float)1.,(
	    float)0.,(float).53,(float)1.,(float)0.,(float).52,(float)1.,(
	    float)0.,(float).51,(float)1.,(float)0.,(float).5,(float)1.,(
	    float)0.,(float).49,(float)1.,(float)0.,(float).48,(float)1.,(
	    float)0.,(float).47,(float)1.,(float)0.,(float).46,(float)1.,(
	    float)0.,(float).45,(float)1.,(float)0.,(float).44,(float)1.,(
	    float)0.,(float).43,(float)1.,(float)0.,(float).42,(float)1.,(
	    float)0.,(float).41,(float)1.,(float)0.,(float).4,(float)1.,(
	    float)0.,(float).39,(float)1.,(float)0.,(float).38,(float)1.,(
	    float)0.,(float).37,(float)1.,(float)0.,(float).36,(float)1.,(
	    float)0.,(float).35,(float)1.,(float)0.,(float).34,(float)1.,(
	    float)0.,(float).33,(float)1.,(float)0.,(float).32,(float)1.,(
	    float)0.,(float).31,(float)1.,(float)0.,(float).3,(float)1.,(
	    float)0.,(float).29,(float)1.,(float)0.,(float).28,(float)1.,(
	    float)0.,(float).27,(float)1.,(float)0.,(float).26,(float)1.,(
	    float)0.,(float).25,(float)1.,(float)0.,(float).24,(float)1.,(
	    float)0.,(float).23,(float)1.,(float)0.,(float).22,(float)1.,(
	    float)0.,(float).21,(float)1.,(float)0.,(float).2,(float)1.,(
	    float)0.,(float).19,(float)1.,(float)0.,(float).18,(float)1.,(
	    float)0.,(float).17,(float)1.,(float)0.,(float).16,(float)1.,(
	    float)0.,(float).15,(float)1.,(float)0.,(float).14,(float)1.,(
	    float)0.,(float).13,(float)1.,(float)0.,(float).12,(float)1.,(
	    float)0.,(float).11,(float)1.,(float)0.,(float).1,(float)1.,(
	    float)0.,(float).09,(float)1.,(float)0.,(float).08,(float)1.,(
	    float)0.,(float).07,(float)1.,(float)0.,(float).06,(float)1.,(
	    float)0.,(float).05,(float)1.,(float)0.,(float).04,(float)1.,(
	    float)0.,(float).03,(float)1.,(float)0.,(float).02,(float)1.,(
	    float)0.,(float).01,(float)1.,(float)0.,(float)0.,(float).95,(
	    float)0.,(float).05,(float).9,(float)0.,(float).1,(float).85,(
	    float)0.,(float).15,(float).8,(float)0.,(float).2,(float).75,(
	    float)0.,(float).25,(float).7,(float)0.,(float).3,(float).65,(
	    float)0.,(float).35,(float).6,(float)0.,(float).4,(float).55,(
	    float)0.,(float).45,(float).5,(float)0.,(float).5,(float).45,(
	    float)0.,(float).55,(float).4,(float)0.,(float).6,(float).35,(
	    float)0.,(float).65,(float).3,(float)0.,(float).7,(float).25,(
	    float)0.,(float).75,(float).2,(float)0.,(float).8,(float).15,(
	    float)0.,(float).85,(float).1,(float)0.,(float).9,(float).05,(
	    float)0.,(float).95,(float)0.,(float)0.,(float)1.,(float)0.,(
	    float).0467,(float)1.,(float)0.,(float).0933,(float)1.,(float)0.,(
	    float).14,(float)1.,(float)0.,(float).1867,(float)1.,(float)0.,(
	    float).2333,(float)1.,(float)0.,(float).28,(float)1.,(float)0.,(
	    float).3267,(float)1.,(float)0.,(float).3733,(float)1.,(float)0.,(
	    float).42,(float)1.,(float)0.,(float).4667,(float)1.,(float)0.,(
	    float).5133,(float)1.,(float)0.,(float).56,(float)1.,(float)0.,(
	    float).6067,(float)1.,(float)0.,(float).6533,(float)1.,(float)0.,(
	    float).7,(float)1.,(float)0.,(float).7429,(float)1.,(float)0.,(
	    float).7857,(float)1.,(float)0.,(float).8286,(float)1.,(float)0.,(
	    float).8714,(float)1.,(float)0.,(float).9143,(float)1.,(float)0.,(
	    float).9571,(float)1.,(float)0.,(float)1.,(float)1.,(float)0.,(
	    float)1.,(float)1.,(float).1429,(float).9286,(float).8571,(float)
	    .2857,(float).8571,(float).7143,(float).4286,(float).7857,(float)
	    .5714,(float).5714,(float).7143,(float).4286,(float).7143,(float)
	    .6429,(float).2857,(float).8571,(float).5714,(float).1429,(float)
	    1.,(float).5,(float)0.,(float)1.,(float).5,(float)0.,(float)1.,(
	    float).625,(float).25,(float)1.,(float).75,(float).5,(float)1.,(
	    float).875,(float).75,(float)1.,(float)1.,(float)1. };

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static real z__;
    extern /* Subroutine */ int stcol1_();
    static real dz;

/*     ------------------------------------------- */


/* ----------------------------------------------------------------------- */
/*  Four colour tables extracted from STARLINK's KAPPA program. */
/* ----------------------------------------------------------------------- */


    /* Parameter adjustments */
    --b;
    --g;
    --r__;
    lutusr -= 4;

    /* Function Body */



/* ----------------------------------------------------------------------- */
    if (*iplot == 2) {
	z__ = (float)0.;
	dz = (float).999 / (real) (*ncols - 1);
	i__1 = *ncols;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    r__[i__] = z__;
	    g[i__] = z__;
	    b[i__] = z__;
	    z__ += dz;
/* L10: */
	}
    } else if (*iplot == 3) {
	stcol1_(heat, &r__[1], &g[1], &b[1], ncols);
    } else if (*iplot == 4) {
	stcol1_(spectrum, &r__[1], &g[1], &b[1], ncols);
    } else if (*iplot == 5) {
	stcol1_(bgyrw, &r__[1], &g[1], &b[1], ncols);
    } else if (*iplot == 6) {
	stcol1_(serp, &r__[1], &g[1], &b[1], ncols);
    } else {
	stcol1_(&lutusr[4], &r__[1], &g[1], &b[1], ncols);
    }
} /* setcol_ */


/* Subroutine */ int stcol1_(lut, r__, g, b, n)
real *lut, *r__, *g, *b;
integer *n;
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;

/*     ------------------------------ */


    /* Parameter adjustments */
    --b;
    --g;
    --r__;
    lut -= 4;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	r__[i__] = lut[i__ * 3 + 1];
	g[i__] = lut[i__ * 3 + 2];
	b[i__] = lut[i__ * 3 + 3];
/* L10: */
    }
} /* stcol1_ */


/* ***<some untility routines>******************************************** */

/* Subroutine */ int vcopy_(x, y, n)
real *x, *y;
integer *n;
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;

/*     ----------------------- */


    /* Parameter adjustments */
    --y;
    --x;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* L10: */
	y[i__] = x[i__];
    }
} /* vcopy_ */


/* Subroutine */ int vrfill_(x, a, n)
real *x, *a;
integer *n;
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;

/*     ------------------------ */


    /* Parameter adjustments */
    --x;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* L10: */
	x[i__] = *a;
    }
} /* vrfill_ */


/* Subroutine */ int logqyn_(s, d__, l, s_len, d_len)
char *s, *d__;
logical *l;
ftnlen s_len;
ftnlen d_len;
{
    /* Format strings */
    static char fmt_100[] = "(a45,\002 Y/N (def=\002,a1,\002)  : \002,$)";

    /* Builtin functions */
    integer s_wsle(), do_lio(), e_wsle(), s_wsfe(), do_fio(), e_wsfe();

    /* Local variables */
    static char a[1], d2[1];
    static logical l2;
    extern /* Subroutine */ int stpack_();
    static char string[45];
    extern /* Subroutine */ int formtq_();
    static integer nnn;

    /* Fortran I/O blocks */
    static cilist io___216 = { 0, 6, 0, 0, 0 };
    static cilist io___218 = { 0, 6, 0, fmt_100, 0 };


/*     ------------------------ */


    /* Parameter adjustments */
    --s;

    /* Function Body */
    if (*(unsigned char *)d__ == 'Y') {
	*l = TRUE_;
	*(unsigned char *)d2 = 'N';
	l2 = FALSE_;
    } else if (*(unsigned char *)d__ == 'N') {
	*l = FALSE_;
	*(unsigned char *)d2 = 'Y';
	l2 = TRUE_;
    } else {
	s_wsle(&io___216);
	do_lio(&c__9, &c__1, " Default should be Y or N !", (ftnlen)27);
	e_wsle();
	return 0;
    }
    stpack_(string, s + 1, &c__45, (ftnlen)45, (ftnlen)1);
L1:
    s_wsfe(&io___218);
    do_fio(&c__1, string, (ftnlen)45);
    do_fio(&c__1, d__, (ftnlen)1);
    e_wsfe();
    formtq_(a, &c__1, &nnn, (ftnlen)1);
    if (nnn == 0) {
	return 0;
    }
    if (*(unsigned char *)a == 'y' || *(unsigned char *)a == 'T' || *(
	    unsigned char *)a == 't') {
	*(unsigned char *)a = 'Y';
    }
    if (*(unsigned char *)a == 'n' || *(unsigned char *)a == 'F' || *(
	    unsigned char *)a == 'f') {
	*(unsigned char *)a = 'N';
    }
    if (*(unsigned char *)a == *(unsigned char *)d__) {
	return 0;
    } else if (*(unsigned char *)a == *(unsigned char *)d2) {
	*l = l2;
	return 0;
    }
    goto L1;
} /* logqyn_ */


/* Subroutine */ int stpack_(string, s, n, string_len, s_len)
char *string, *s;
integer *n;
ftnlen string_len;
ftnlen s_len;
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__, j;

/*     ----------------------------- */


    /* Parameter adjustments */
    --s;
    --string;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)&string[i__] = *(unsigned char *)&s[i__];
	if (*(unsigned char *)&s[i__] == '?') {
	    goto L20;
	}
/* L10: */
    }
L20:
    i__1 = *n;
    for (j = i__ + 1; j <= i__1; ++j) {
/* L30: */
	*(unsigned char *)&string[j] = ' ';
    }
} /* stpack_ */


/* Subroutine */ int formtq_(string, nchar, nnn, string_len)
char *string;
integer *nchar, *nnn;
ftnlen string_len;
{
    /* Format strings */
    static char fmt_200[] = "(a)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_rsfe(), do_fio(), e_rsfe();

    /* Local variables */
    static integer i__;

    /* Fortran I/O blocks */
    static cilist io___223 = { 0, 5, 0, fmt_200, 0 };


/*     ----------------------------------- */


    s_rsfe(&io___223);
    do_fio(&c__1, string, string_len);
    e_rsfe();
    *nnn = 0;
    i__1 = *nchar;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* L10: */
	if (*(unsigned char *)&string[i__ - 1] != ' ') {
	    ++(*nnn);
	}
    }
} /* formtq_ */

