/* .\dsqinf.f -- translated by f2c (version 20000704).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#ifdef WIN
#pragma warning (disable: 4244) // disable warning for conversion from double to float, possible loss of data
#pragma warning (disable: 4716) // disable warning for returning a value
#endif /* WIN */

#include "f2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;

/* CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC */

/* Subroutine */ int dsqinf_(xoff, xlen, xorg, xscale, xperin, xblc, xtrc, 
	yoff, ylen, yorg, yscale, yperin, yblc, ytrc)
real *xoff, *xlen, *xorg, *xscale, *xperin, *xblc, *xtrc, *yoff, *ylen, *yorg,
	 *yscale, *yperin, *yblc, *ytrc;
{
    /* Initialized data */

    static real big = (float)1e20;
    static real small = (float)1e-20;

    /* System generated locals */
    real r__1;

    /* Local variables */
    static real xwdif, ywdif;
    extern /* Subroutine */ int pgqvp_();
    static real axwdif, aywdif;
    extern /* Subroutine */ int pgqwin_();
    static real xi1, xi2, yi1, yi2, xp2, yp2;

/*     --------------------------------------------------------- */


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/* Purpose */
/*      Obtain some viewport and window information about the current */
/*    PGPLOT device, without directly accessing the common blocks in */
/*    pgplot.inc. */

/* Parameters */
/*   ARGUMENT  TYPE  I/O  DIMENSION  DESCRIPTION */
/*    XPERIN   R*4    O       -      Plot X scale in dots/inch. */
/*    YPERIN   R*4    O       -      Plot Y scale in dots/inch. */
/*    XOFF     R*4    O       -      Absolute coord of blc of viewport. */
/*    YOFF     R*4    O       -      Absolute coord of blc of viewport. */
/*    XLEN     R*4    O       -      Width of viewport in absolute coord. */
/*    YLEN     R*4    O       -      Height of viewport in absolute coord. */
/*    XORG     R*4    O       -      Absolute coord of world X=0. */
/*    YORG     R*4    O       -      Absolute coord of world Y=0. */
/*    XSCALE   R*4    O       -      Absolute units per world coord in X. */
/*    YSCALE   R*4    O       -      Absolute units per world coord in Y. */
/*    XBLC     R*4    O       -      World X coord at blc of window. */
/*    XTRC     R*4    O       -      World X coord at trc of window. */
/*    YBLC     R*4    O       -      World Y coord at blc of window. */
/*    YTRC     R*4    O       -      World Y coord at trc of window. */

/* Globals */
/*     None. */

/* External Calls */
/*   SUBROUTINE   DESCRIPTION */
/*     PGQVP      Inquires about viewport dimensions. */
/*     PGQWIN     Inquires about world coords of window. */

/* History */
/*   D. S. Sivia       1 Aug 1996  Initial release. */
/* ----------------------------------------------------------------------- */

    pgqwin_(xblc, xtrc, yblc, ytrc);
    pgqvp_(&c__1, &xi1, &xi2, &yi1, &yi2);
    pgqvp_(&c__3, xoff, &xp2, yoff, &yp2);
    *xlen = (r__1 = xp2 - *xoff, dabs(r__1));
    *ylen = (r__1 = yp2 - *yoff, dabs(r__1));
    *xperin = *xlen / ((r__1 = xi2 - xi1, dabs(r__1)) + small);
    *yperin = *ylen / ((r__1 = yi2 - yi1, dabs(r__1)) + small);
    xwdif = *xtrc - *xblc;
    ywdif = *ytrc - *yblc;
    axwdif = big;
    aywdif = big;
    if (dabs(xwdif) > small) {
	axwdif = (float)1. / xwdif;
    }
    if (dabs(ywdif) > small) {
	aywdif = (float)1. / ywdif;
    }
    *xscale = *xlen * axwdif;
    *yscale = *ylen * aywdif;
    *xorg = (*xoff * *xtrc - xp2 * *xblc) * axwdif;
    *yorg = (*yoff * *ytrc - yp2 * *yblc) * aywdif;
} /* dsqinf_ */

