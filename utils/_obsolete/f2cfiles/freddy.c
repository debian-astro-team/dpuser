/* .\freddy.f -- translated by f2c (version 20000704).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#ifdef WIN
#pragma warning (disable: 4244) // disable warning for conversion from double to float, possible loss of data
#pragma warning (disable: 4554) // disable warning for check operator precedence for possible error
#pragma warning (disable: 4715) // disable warning for not all control paths return a value
#pragma warning (disable: 4716) // disable warning for must return a value
#endif /* WIN */

#include "f2c.h"

/* Common Block Declarations */

struct {
    real deltax, x;
    integer step, left, right, it, nx;
    logical visble;
} fredcm_;

#define fredcm_1 fredcm_

/* Table of constant values */

static real c_b10 = (float)0.;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int freddy_(array, kx, ny, size, angle)
real *array;
integer *kx, *ny;
real *size, *angle;
{
    /* System generated locals */
    integer array_dim1, array_offset, i__1, i__2;
    real r__1, r__2;

    /* Builtin functions */
    double sin();

    /* Local variables */
    static real peak, fmin, fmax, sine;
    static integer incx, i__, j, ki, kj, mn;
    static real dx;
    static integer mx;
    static real height;
    static integer my;
    extern /* Subroutine */ int pgbbuf_(), fredgo_(), pgebuf_();
    static real deltav, deltay;
    extern /* Subroutine */ int pgdraw_(), pgmove_();


/* Draws isometric plot of array */


    /* Parameter adjustments */
    array_dim1 = *kx;
    array_offset = 1 + array_dim1 * 1;
    array -= array_offset;

    /* Function Body */
    mn = *kx * *ny;
    fredcm_1.nx = *kx;
/*     Check array size: */
    if (fredcm_1.nx < 2 || *ny < 2) {
	return 0;
    }
    fmax = array[array_dim1 + 1];
    fmin = fmax;
    i__1 = *ny;
    for (j = 1; j <= i__1; ++j) {
	i__2 = fredcm_1.nx;
	for (i__ = 1; i__ <= i__2; ++i__) {
/* Computing MIN */
	    r__1 = array[i__ + j * array_dim1];
	    fmin = dmin(r__1,fmin);
/* Computing MAX */
	    r__1 = array[i__ + j * array_dim1];
	    fmax = dmax(r__1,fmax);
/* L10: */
	}
/* L20: */
    }
    fredcm_1.deltax = *size / (fredcm_1.nx + *ny);
    sine = sin(*angle / (float)58.);
    deltay = fredcm_1.deltax * sine;
    height = *size * ((float)1. - dabs(sine));
    deltav = height;
    fmax -= fmin;
    if (fmax < (float)1e-4) {
	fmax = deltav;
    }
    deltav /= fmax;
    mx = fredcm_1.nx + 1;
    my = *ny + 1;
    fredcm_1.step = mx;

/* Start PGPLOT buffering. */

    pgbbuf_();

/* Work our way down the Y axis, then up the X axis, */
/* calculating the Y plotter coordinates for each */
/* column of the plot, doing the hidden-line suppression */
/* at the same time. */

    i__1 = *ny;
    for (j = 1; j <= i__1; ++j) {
	kj = my - j;
	ki = 1;
/*               ( KI,KJ are coordinates of bottom of column) */
	array[ki + kj * array_dim1] = deltay * (ki + kj) + deltav * (array[ki 
		+ kj * array_dim1] - fmin);
L30:
	peak = array[ki + kj * array_dim1];
L40:
	++ki;
	++kj;
	if (ki > fredcm_1.nx || kj > *ny) {
	    goto L50;
	}
	array[ki + kj * array_dim1] = deltay * (ki + kj) + deltav * (array[ki 
		+ kj * array_dim1] - fmin);
	if (array[ki + kj * array_dim1] > peak) {
	    goto L30;
	}
	if (array[ki + kj * array_dim1] <= peak) {
	    array[ki + kj * array_dim1] = -(r__1 = array[ki + kj * array_dim1]
		    , dabs(r__1));
	}
	goto L40;
L50:
	;
    }

/* Now to work our way up the X axis */

    i__1 = fredcm_1.nx;
    for (i__ = 2; i__ <= i__1; ++i__) {
	ki = i__;
	kj = 1;
	array[ki + kj * array_dim1] = deltay * (ki + kj) + deltav * (array[ki 
		+ kj * array_dim1] - fmin);
L60:
	peak = array[ki + kj * array_dim1];
L70:
	++ki;
	++kj;
	if (ki > fredcm_1.nx || kj > *ny) {
	    goto L80;
	}
	array[ki + kj * array_dim1] = deltay * (ki + kj) + deltav * (array[ki 
		+ kj * array_dim1] - fmin);
	if (array[ki + kj * array_dim1] > peak) {
	    goto L60;
	}
	if (array[ki + kj * array_dim1] <= peak) {
	    array[ki + kj * array_dim1] = -(r__1 = array[ki + kj * array_dim1]
		    , dabs(r__1));
	}
	goto L70;
L80:
	;
    }

/* Draw a line along the bottom of the vertical faces */

    r__1 = fredcm_1.deltax * (fredcm_1.nx + *ny - 2);
    r__2 = deltay * mx;
    pgmove_(&r__1, &r__2);
    r__1 = fredcm_1.deltax * (*ny - 1);
    r__2 = deltay * 2;
    pgdraw_(&r__1, &r__2);
    r__1 = deltay * my;
    pgdraw_(&c_b10, &r__1);

/* Array is now ready for plotting.  If a point is */
/* positive, then it is to be plotted at that Y */
/* coordinate; if it is negative, then it is */
/* invisible, but at minus that Y coordinate (the point */
/* where the line heading towards it disappears has to */
/* be determined by finding the intersection of it and */
/* the cresting line). */

/* Plot rows: */

    i__1 = *ny;
    for (j = 1; j <= i__1; j += 2) {
	kj = my - j;
	dx = fredcm_1.deltax * (j - 2);
	fredcm_1.x = dx + fredcm_1.deltax;
	r__1 = deltay * (kj + 1);
	pgmove_(&fredcm_1.x, &r__1);
	pgdraw_(&fredcm_1.x, &array[kj * array_dim1 + 1]);
	fredcm_1.visble = TRUE_;
	i__2 = fredcm_1.nx;
	for (i__ = 2; i__ <= i__2; ++i__) {
	    fredcm_1.right = i__ + fredcm_1.nx * (kj - 1);
	    fredcm_1.left = fredcm_1.right - 1;
	    fredcm_1.it = fredcm_1.right;
	    fredcm_1.x = dx + fredcm_1.deltax * i__;
	    fredgo_(&array[array_offset], &mn);
/* L90: */
	}

/* Now at far end of row so come back */

	--kj;
	if (kj <= 0) {
	    goto L170;
	}
	fredcm_1.visble = array[fredcm_1.nx + kj * array_dim1] >= (float)0.;
	dx = fredcm_1.deltax * (fredcm_1.nx + j);
	if (fredcm_1.visble) {
	    r__1 = dx - fredcm_1.deltax;
	    pgmove_(&r__1, &array[fredcm_1.nx + kj * array_dim1]);
	}
	fredcm_1.deltax = -fredcm_1.deltax;
	i__2 = fredcm_1.nx;
	for (i__ = 2; i__ <= i__2; ++i__) {
	    ki = mx - i__;
	    fredcm_1.left = ki + fredcm_1.nx * (kj - 1);
	    fredcm_1.right = fredcm_1.left + 1;
	    fredcm_1.it = fredcm_1.left;
	    fredcm_1.x = dx + fredcm_1.deltax * i__;
	    fredgo_(&array[array_offset], &mn);
/* L100: */
	}

	fredcm_1.x = dx + fredcm_1.deltax * fredcm_1.nx;
	if (! fredcm_1.visble) {
	    pgmove_(&fredcm_1.x, &array[kj * array_dim1 + 1]);
	}
	r__1 = deltay * (kj + 1);
	pgdraw_(&fredcm_1.x, &r__1);
/*               (set DELTAX positive for return trip) */
	fredcm_1.deltax = -fredcm_1.deltax;
/* L110: */
    }

/* Now do the columns: */
/* as we fell out of the last DO-loop we do the */
/* columns in ascending-X order */

    incx = 1;
    ki = 1;
/*               (set DELTAX -ve since scanning R to L) */
L120:
    dx = fredcm_1.deltax * (ki + *ny - 1);
    fredcm_1.deltax = -fredcm_1.deltax;
    fredcm_1.x = dx + fredcm_1.deltax;
    pgmove_(&fredcm_1.x, &array[array_dim1 + 1]);
L130:
    fredcm_1.visble = TRUE_;
    i__1 = *ny;
    for (j = 2; j <= i__1; ++j) {
	fredcm_1.left = ki + fredcm_1.nx * (j - 1);
	fredcm_1.right = fredcm_1.left - fredcm_1.nx;
	fredcm_1.it = fredcm_1.left;
	fredcm_1.x = dx + fredcm_1.deltax * j;
	fredgo_(&array[array_offset], &mn);
/* L140: */
    }

/* At far end, increment X and check still inside array */

    ki += incx;
    if (ki <= 0 || ki > fredcm_1.nx) {
	goto L180;
    }
    fredcm_1.visble = array[ki + *ny * array_dim1] >= (float)0.;
    fredcm_1.deltax = -fredcm_1.deltax;
    dx = fredcm_1.deltax * (ki - 2);
    fredcm_1.x = dx + fredcm_1.deltax;
    if (fredcm_1.visble) {
	pgmove_(&fredcm_1.x, &array[ki + *ny * array_dim1]);
    }
    i__1 = *ny;
    for (j = 2; j <= i__1; ++j) {
	kj = my - j;
	fredcm_1.right = ki + fredcm_1.nx * (kj - 1);
	fredcm_1.left = fredcm_1.right + fredcm_1.nx;
	fredcm_1.it = fredcm_1.right;
	fredcm_1.x = dx + fredcm_1.deltax * j;
	fredgo_(&array[array_offset], &mn);
/* L150: */
    }

    fredcm_1.x = dx + fredcm_1.deltax * *ny;
    if (! fredcm_1.visble) {
	pgmove_(&fredcm_1.x, &array[ki + array_dim1]);
    }
    if (ki == 1) {
	goto L180;
    }
    r__1 = deltay * (ki + 1);
    pgdraw_(&fredcm_1.x, &r__1);
    ki += incx;
    if (ki > fredcm_1.nx) {
	goto L180;
    }
    if (ki == 1) {
	goto L120;
    }
L160:
    fredcm_1.deltax = -fredcm_1.deltax;
    dx = fredcm_1.deltax * (1 - ki - *ny);
    fredcm_1.x = dx + fredcm_1.deltax;
    r__1 = deltay * (ki + 1);
    pgmove_(&fredcm_1.x, &r__1);
    pgdraw_(&fredcm_1.x, &array[ki + array_dim1]);
    goto L130;

/* Do columns backwards because ended rows at far end of X */

L170:
    ki = fredcm_1.nx;
    incx = -1;
    dx = fredcm_1.deltax * (ki + *ny);
    goto L160;


L180:
    pgebuf_();
} /* freddy_ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int fredgo_(array, mn)
real *array;
integer *mn;
{
    /* System generated locals */
    real r__1;

    /* Local variables */
    static real y, al, bl, em, ar, xx;
    extern /* Subroutine */ int pgdraw_(), pgmove_();



/* Test visibility */

    /* Parameter adjustments */
    --array;

    /* Function Body */
    if (array[fredcm_1.it] < (float)0.) {
	goto L80;
    }

/* This point is visible - was last? */

    if (fredcm_1.visble) {
	goto L50;
    }

/* No: calculate point where this line vanishes */

L10:
    if (fredcm_1.left <= fredcm_1.nx || (fredcm_1.left - 1) % fredcm_1.nx == 
	    0 || fredcm_1.right <= fredcm_1.nx || (fredcm_1.right - 1) % 
	    fredcm_1.nx == 0) {
	goto L100;
    }
    al = (r__1 = array[fredcm_1.left], dabs(r__1));
    ar = (r__1 = array[fredcm_1.right], dabs(r__1));
    if (array[fredcm_1.left] < (float)0.) {
	goto L70;
    }
/*               Right-hand point is crested */
L20:
    fredcm_1.right -= fredcm_1.step;
    if (array[fredcm_1.right] < (float)0.) {
	goto L20;
    }
/*               Left-hand end of cresting line is either */
/*               RIGHT+NX or RIGHT-1 */
    fredcm_1.left = fredcm_1.right + fredcm_1.nx;
    if (array[fredcm_1.left] < (float)0.) {
	fredcm_1.left = fredcm_1.right - 1;
    }

/*               RIGHT and LEFT index into the endpoints of the */
/*               cresting line */
L30:
    bl = (r__1 = array[fredcm_1.left], dabs(r__1));
    em = (r__1 = array[fredcm_1.right], dabs(r__1)) - bl;
    xx = em - ar + al;
    if (dabs(xx) < (float)1e-4) {
	goto L60;
    }
    xx = (al - bl) / xx;
L40:
    y = em * xx + bl;
    if (fredcm_1.deltax > (float)0.) {
	xx = (float)1. - xx;
    }
    xx = fredcm_1.x - xx * fredcm_1.deltax;
    if (fredcm_1.visble) {
	goto L90;
    }
/*               Drawing a line from an invisible point */
/*               to a visible one */
    pgmove_(&xx, &y);
    fredcm_1.visble = TRUE_;
L50:
    pgdraw_(&fredcm_1.x, &array[fredcm_1.it]);
    return 0;

L60:
    xx = (float).5;
    goto L40;

/* Left-hand point crested */

L70:
    fredcm_1.left -= fredcm_1.step;
    if (array[fredcm_1.left] < (float)0.) {
	goto L70;
    }

/* Right-hand end of cresting line is either LEFT+1 or LEFT-NX */

    fredcm_1.right = fredcm_1.left + 1;
    if (array[fredcm_1.right] < (float)0.) {
	fredcm_1.right = fredcm_1.left - fredcm_1.nx;
    }
    goto L30;

/* This point is invisible; if last one was too, then forget it; */
/* else draw a line towards it */

L80:
    if (! fredcm_1.visble) {
	return 0;
    }
    goto L10;

L90:
    pgdraw_(&xx, &y);
L100:
    fredcm_1.visble = FALSE_;
    return 0;
} /* fredgo_ */

