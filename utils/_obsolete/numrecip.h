/***************************************************************************
                          numrecip.h  -  description
                             -------------------
    begin                : Wed Apr 19 2000
    copyright            : (C) 2000 by Thomas Ott
    email                : ott@mpe.mpg.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 * file: utils/numrecip.h
 * Purpose: Selection of routines of numerical recipes in C - header file
 * Author: Thomas Ott
 *
 * History: 06.10.1999: file created
 */

#ifndef NUMRECIP_H
#define NUMRECIP_H

#include "defines.h"

#ifdef __cplusplus
extern "C" {
#endif

float *vector(long nl, long nh);
float **matrix(long nrl, long nrh, long ncl, long nch);
void free_vector(float *v, long nl, long nh);
void free_matrix(float **m, long nrl, long nrh, long ncl, long nch);
void nr_polint(float xa[], float ya[], int n, float x, float *y, float *dy);
void polin2(float x1a[], float x2a[], float **ya, int m, int n, float x1, float x2, float *y, float *dy);
float nr_select(unsigned long k, unsigned long n, float arr[]);
void sort1(int n, float ra[]);
void four1(float data[], unsigned long nn, int isign);
void realft(float data[], unsigned long n, int isign);
void twofft(float data1[], float data2[], float fft1[], float fft2[],
        unsigned long n);
void correl(float data1[], float data2[], unsigned long n, float ans[]);
double to_atan(double x, double y);
void hpsort(unsigned long n, float ra[]);
void gser(float *gamser, float a, float x, float *gln);
void gcf(float *gammcf, float a, float x, float *gln);
float gammp(float a, float x);
void fpoly(float x, float p[], int np);
void svdfit(float x[], float y[], float sig[], int ndata, float a[], int ma,
	float **u, float **v, float w[], float *chisq,
	void (*funcs)(float, float [], int));
void svdvar(float **v, int ma, float w[], float **cvm);
void indexx(unsigned long n, float arr[], unsigned long indx[]);
float ran1(long *idum);
void gaussj(float **a, int n, float **b, int m);
/*
int gaussfit(float *x, float *y, int NPT, float *guess, float *error, int ngauss, int maxiter);
int gauss2dfit(float *x, float *y, int NPT, float *guess, float *error, int xx, int maxiter);
*/
#ifdef __cplusplus
}
#endif

#endif /* NUMRECIP_H */
