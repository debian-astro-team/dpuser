#ifndef DPSTRINGLIST_H
#define DPSTRINGLIST_H

#ifdef WIN
#pragma warning(disable:4786)
#endif

#include <vector>

#include "dpstring.h"

class dpStringList : public std::vector<dpString> {
public:
//@    dpStringList();
//@    dpStringList(const dpStringList &);
//@    ~dpStringList();

//@    dpStringList &operator=(const dpStringList &);
	dpint64 count() const;
	void append(const dpString &);
	void prepend(const dpString &);
    void remove(dpStringList::iterator);
	void operator +=(const dpStringList &);
    void operator +=(const char *);
    dpString first();
    bool readFile(const dpString &);
    static dpStringList split(const char, dpString &);
    static dpStringList splitcrlf(dpString &);

    const char *getFileName() const;
    void setFileName(const char *fname);
    const char *getColumnName() const;
    void setColumnName(const char *cname);

private:
    std::string columnName,
                fileName;
//@    char *columnName,
//@         *fileName;
};

class dpDir {
public:
	static dpString currentDirPath();
	static dpStringList findfile(const dpString &);
	static dpStringList dir(const dpString &);

private:
	static void walk(const dpString &, const dpString &, const dpString &, dpStringList &);
};

#endif /* DPSTRINGLIST_H */
